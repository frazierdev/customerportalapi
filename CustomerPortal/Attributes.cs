﻿using System;

namespace CustomerPortal.Shared
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DateOnly : System.Attribute { }
}
