﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using System.Globalization;

namespace CustomerPortal
{
    public static class DB
    {

        public static void SaveField(object DatabaseObject, string FieldName, string Value)
        {
            Type t = DatabaseObject.GetType();
            PropertyInfo info = t.GetProperty(FieldName);

            Type u = Nullable.GetUnderlyingType(info.PropertyType);

            if (u != null)
            {
                if (Value == null || Value.Length == 0)
                    info.SetValue(DatabaseObject, null);
                else
                {
                    if (u.ToString().Contains("DateTime"))
                        info.SetValue(DatabaseObject, DateTime.ParseExact(Value.Substring(0, 24), "ddd MMM d yyyy HH:mm:ss", CultureInfo.InvariantCulture));
                    else if (u.ToString().Contains("TimeSpan"))
                        info.SetValue(DatabaseObject, TimeSpan.Parse(Value));
                    else
                        info.SetValue(DatabaseObject, Convert.ChangeType(Value, u));
                }
            }
            else
            {
                info.SetValue(DatabaseObject, Convert.ChangeType(Value, info.PropertyType));
            }

        }
    }
}
