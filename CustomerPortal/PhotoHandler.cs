﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace CustomerPortal.Images
{
    public static class PhotoHandler
    {
        public static byte[] FormatImage(string filename, bool Resize = false)
        {

            using (System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(filename))
            {
                byte[] result = null;
                using (MemoryStream stream = new MemoryStream())
                {
                    ImageCodecInfo jpgEncoder;
                    if (filename.Contains(".png"))
                        jpgEncoder = GetEncoder(ImageFormat.Png);
                    else
                        jpgEncoder = GetEncoder(ImageFormat.Jpeg);

                    // Create an Encoder object based on the GUID
                    // for the Quality parameter category.
                    System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
                    EncoderParameters myEncoderParameters = new EncoderParameters(1);
                    EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 80L);
                    myEncoderParameters.Param[0] = myEncoderParameter;
                    bitmap.Save(stream, jpgEncoder, myEncoderParameters);
                    if (!Resize)
                        result = stream.ToArray();
                    else
                    {
                        Image img = Image.FromStream(stream);
                        Image thumb = ScaleImage(img, 1000, 200);
                        using (MemoryStream stream2 = new MemoryStream())
                        {
                            thumb.Save(stream2, System.Drawing.Imaging.ImageFormat.Jpeg);
                            result = stream2.ToArray();
                        }
                    }
                }

                return result;
            }
        }

        private static Image ScaleImage(Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);
            Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
            return newImage;
        }

        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }


    }
}
