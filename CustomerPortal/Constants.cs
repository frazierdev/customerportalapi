﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal
{
    public class Constants
    {


#if DEBUG
        public const string FrobotUrl = "http://localhost/frobot/";
        // TODO: May need to update this to use specific Frobot instance with IP
#if DEV
        public const string BaseUrl = "https://customersdev.frazier.com/";
#else //#if TEST || TEST2
        public const string BaseUrl = "http://localhost:8080/";
#endif
        public const bool DEBUG = true;
#else

#if BETA
        public const string BaseUrl = "https://customers-beta.frazier.com/";
#else
        public const string BaseUrl = "https://customers.frazier.com/";
#endif

        public const string FrobotUrl = "https://frobot.frazier.com/";
        public const bool DEBUG = false;
#endif

        public const string GalleryPassword = "Frazier.2023";

        public static class Connections
        {
#if DEBUG
            public const string Salesforce = "SalesforceDevConnection";
            public const string CustomerPortal = "CustomerPortalDevConnection";
            public const string MES = "MESDevConnection";
            public const string Epicor = "TestConnection";
#else
            public const string Epicor = "EpicorConnection";
            public const string Salesforce = "SalesforceConnection";
            public const string CustomerPortal = "CustomerPortalConnection";
            public const string MES = "MESConnection";
#endif
        }

        public static class Databases
        {
#if DEBUG
            public const string Salesforce = "Salesforce_Restored";
            public const string CustomerPortal = "CustomerPortalDev";

#else
            public const string Salesforce = "Salesforce";
            public const string CustomerPortal = "CustomerPortal";
#endif
        }

        public static class Directories
        {
#if DEBUG
            public const string TempAttachments = @"\\nj-web-03\CSData$\TEST\Attachments\";
            public const string MESBase = @"\\nj-mes-fs-dev\dev\";
            public const string MarketingBase = @"\\nj-mkt\marketing$\FCP\TEST\";
#else
            public const string TempAttachments = @"\\nj-web-03\CSData$\Attachments\";
            public const string MESBase = @"\\nj-mes-fs\prod\";
            public const string MarketingBase = @"\\nj-mkt\marketing$\FCP\";
#endif
            public const string EmpPhotos = @"\\nj-mes-fs\Prod\Employee Photos\";
            public const string EpiPhotos = @"\\nj-epicor-fs\EpicorData\EmpPhoto\";

            public static string Surveys => $@"{MarketingBase}Surveys\";
            public static string GalleryPhotos => $@"{MarketingBase}Gallery\";
            //public const string GalleryPhotos = @"\\nj-gen\publicshare$\share\JAlmonte\TEST\Gallery\";

            public static string PortalAttachments => $@"{MESBase}Customer Portal\";

            public static string Geolocations => $@"{MESBase}Geolocations\";
            public static string GeolocationsStreetMap => $@"{Geolocations}StreetMap\";
            public static string GeolocationsAerialMap => $@"{Geolocations}AerialMap\";
        }

        public static class ClaimType
        {
            public const string EMP_ID = "EmpID";
            public const string USER_ID = "UserID";
            public const string HAS_MASTER_PASSWORD = "HasMasterPassword";
        }

        public static class TierLimit
        {
            public const int Bronze = 0;
            public const int Silver = 1;
            public const int Gold = 2;
            public const int Platinum = 3;
        }

        public static string GetUniqueFileName(string fileDir, string fileName)
        {
            int counter = Directory.GetFiles(fileDir, fileName + DateTime.Now.ToString("yyyyMMdd") + "*").Length + 1;
            fileName = fileName + DateTime.Now.ToString("yyyyMMdd") + counter.ToString() + ".xlsx";
            return fileName;
        }

    }
}
