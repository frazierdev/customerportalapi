﻿using CustomerPortal.Data;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal
{
    public class Utilities
    {
        private readonly IConfiguration _configuration;

        public Utilities(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public static void UpdateField(object DatabaseObject, string FieldName, string Value, CustomerPortalContext CustomerPortal = null)
        {
            try
            {
                Type t = DatabaseObject.GetType();
                System.Reflection.PropertyInfo info = t.GetProperty(FieldName);

                Type u = Nullable.GetUnderlyingType(info.PropertyType);

                if (u != null)
                {
                    if (Value == null || Value.Length == 0)
                    {
                        info.SetValue(DatabaseObject, null);
                    }
                    else
                    {
                        if (u.ToString().Contains("TimeSpan"))
                        {
                            info.SetValue(DatabaseObject, TimeSpan.Parse(Value));
                        }
                        else if (u.ToString().Contains("DateTime"))
                        {
                            try
                            {
                                var x = Value.Substring(0, Math.Min(Value.Length, 24));
                                DateTime d;
                                if (t.GetProperties().Any(property => Attribute.IsDefined(property, typeof(Shared.DateOnly))))
                                    d = DateTime.ParseExact(x, "ddd MMM d yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).Date;
                                else
                                    d = DateTime.ParseExact(x, "ddd MMM d yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToUniversalTime();

                                info.SetValue(DatabaseObject, d);
                            }
                            catch (Exception)
                            {
                                if (t.GetProperties().Any(property => Attribute.IsDefined(property, typeof(Shared.DateOnly))))
                                    info.SetValue(DatabaseObject, Convert.ToDateTime(Value).Date);
                                else
                                    info.SetValue(DatabaseObject, Convert.ToDateTime(Value).ToUniversalTime());
                            }

                        }
                        else
                        {
                            info.SetValue(DatabaseObject, Convert.ChangeType(Value, u));
                        }
                    }
                }
                else
                {
                    if (info.PropertyType.IsGenericType && (info.PropertyType.GetGenericTypeDefinition() == typeof(List<>)))
                    {
                        var items = JArray.Parse(Value);

                        object instance = Activator.CreateInstance(info.PropertyType);
                        IList list = (IList)instance;
                        foreach (var o in items)
                        {
                            list.Add(o.ToObject(instance.GetType().GetGenericArguments()[0]));
                        }

                        info.SetValue(DatabaseObject, list, null);
                    }
                    else if (info.PropertyType.IsEnum)
                    {
                        info.SetValue(DatabaseObject, Enum.Parse(info.PropertyType, Value));
                    }
                    else
                    {
                        info.SetValue(DatabaseObject, Convert.ChangeType(Value, info.PropertyType));
                    }
                }


                if (CustomerPortal != null)
                {
                    //if (Attribute.IsDefined(info, typeof(SerializeToFieldAttribute)))
                    //{
                    //    System.Diagnostics.Debug.WriteLine($"{DatabaseObject.GetType().Name} --> {MES.Entry(DatabaseObject).State}");
                    //    if (MES.Entry(DatabaseObject).State == System.Data.Entity.EntityState.Unchanged)
                    //    {
                    //        Logging.Instance.LogIt("Unmodified DB Entity", new { t.Name, FieldName, Value });
                    //        //MES.Entry(DatabaseObject).State = System.Data.Entity.EntityState.Modified;
                    //    }
                    //}

                    CustomerPortal.SaveChanges();
                }
                else
                    Logger.LogIt("Invalid Data Context", new { CustomerPortal });


            }
            catch (Exception x)
            {
                List<string> info = new List<string>
                {
                    FieldName,
                    Value.ToString()
                };
                //Logger.LogIt(x, info);
                Logger.LogException(x);
                throw;
            }

        }

        public DataTable RetrieveDataTable(string SQLQuery, SqlParameter[] parameters, string connectionName = Constants.Connections.Epicor)
        {
            if (string.IsNullOrEmpty(connectionName))
                connectionName = Constants.Connections.Epicor;

            using (var conn = new SqlConnection(_configuration.GetConnectionString(connectionName)))
            {
                conn.Open();

                using (var cmd = new SqlCommand(SQLQuery, conn))
                {
                    cmd.CommandTimeout = 90;
                    cmd.Parameters.AddRange(parameters);

                    var dt = new DataTable();

                    dt.Load(cmd.ExecuteReader());

                    cmd.Parameters.Clear();
                    return dt;
                }
            }
        }

        public static int PerceivedBrightness(Color c)
        {
            return (int)Math.Sqrt(
            c.R * c.R * .299 +
            c.G * c.G * .587 +
            c.B * c.B * .114);
        }
    }
}
