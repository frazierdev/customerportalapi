﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
//using Infragistics.Documents.Excel;
using System.Data;
using System.Web;
using System.Drawing;
using System.Data.SqlClient;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.Extensions.Configuration;

namespace CustomerPortal
{
    public class DataExport
    {
        /// <summary>
        /// Get excel columns 
        /// </summary>
        /// <param name="columns">total number of columns in table, defaults to 0</param>
        /// <returns>IEnumberable filled with columns letters for Excel</returns>
        static IEnumerable<string> ExcelColumns(int columns = 0)
        {
            StringBuilder chars = new StringBuilder();
            int count = 0;
            while (count < columns)
            {
                int i = chars.Length - 1;
                while (i >= 0 && chars[i] == 'Z')
                {
                    chars[i] = 'A';
                    i--;
                }
                if (i == -1)
                    chars.Insert(0, 'A');
                else
                    chars[i]++;
                count++;
                yield return chars.ToString();
            }
        }

        public string ConnectionString { get; private set; }

        public DataExport(string connectionString)
        {
            ConnectionString = connectionString;
            //_connectionString = !string.IsNullOrEmpty(connectionString) ? connectionString : Constants.Connections.Epicor;
        }

        /// <summary>
        /// Constructor for DataExport class to retrieve query results in different forms
        /// </summary>
        /// <param name="configuration">Configuration to determine the ConnectionString depending on the connectionName parameter</param>
        /// <param name="connectionName">Defaults to EpicorConnection name</param>
        public DataExport(IConfiguration configuration, string connectionName = Constants.Connections.Epicor)
        {
            ConnectionString = configuration.GetConnectionString(!string.IsNullOrEmpty(connectionName) ? connectionName : Constants.Connections.Epicor);
        }

        public byte[] QueryToExcel(string SQLQuery, string TabName, SqlParameter[] SQLParameters = null, bool IsSQLProcedure = false)
        {
            MemoryStream stream = new MemoryStream();

            using (SpreadsheetDocument doc = SpreadsheetDocument.Create(stream, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart bookPart = doc.AddWorkbookPart();
                bookPart.Workbook = new Workbook();

                // Add a WorksheetPart to the WorkbookPart.
                WorksheetPart part = bookPart.AddNewPart<WorksheetPart>();
                SheetData data = new SheetData();
                part.Worksheet = new Worksheet(data);

                // Add Sheets to the Workbook.
                Sheets sheets = doc.WorkbookPart.Workbook.
                    AppendChild<Sheets>(new Sheets());

                Sheet sheet = new Sheet()
                {
                    Id = doc.WorkbookPart.GetIdOfPart(part),
                    SheetId = 1,
                    Name = TabName
                };

                using (var conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (var cmd = new SqlCommand(SQLQuery, conn))
                    {
                        cmd.CommandTimeout = 300;

                        if (IsSQLProcedure)
                            cmd.CommandType = CommandType.StoredProcedure;

                        if (SQLParameters != null && SQLParameters.Length > 0)
                            cmd.Parameters.AddRange(SQLParameters);

                        var reader = cmd.ExecuteReader();
                        Row row = new Row();

                        for (int c = 0; c < reader.FieldCount; c++)
                        {
                            //sht.Rows[0].Cells[c].Value = reader.GetName(c);
                            Cell cell = new Cell();
                            cell.DataType = CellValues.String;
                            cell.CellValue = new CellValue(reader.GetName(c));
                            row.AppendChild(cell);
                        }

                        data.AppendChild(row);
                        //int r = 1;

                        while (reader.Read())
                        {
                            row = new Row();
                            for (int c = 0; c < reader.FieldCount; c++)
                            {
                                //sht.Rows[r].Cells[c].Value = reader[c];
                                Cell cell = new Cell();
                                cell.DataType = CellValues.String;
                                cell.CellValue = new CellValue(reader[c].ToString());
                                row.AppendChild(cell);
                            }
                            data.AppendChild(row);
                            //r++;
                        }
                    }

                    conn.Close();
                }

                sheets.Append(sheet);

                bookPart.Workbook.Save();

                // Close the document.
                doc.Close();


                byte[] rtn = stream.ToArray();

                return rtn;
            }
        }

        public DataTable QueryToTable(string SQLQuery, string TableName, SqlParameter[] SQLParameters = null, bool IsSQLProcedure = false)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                using (var cmd = new SqlCommand(SQLQuery, conn))
                {
                    cmd.CommandTimeout = 300;

                    if (IsSQLProcedure)
                        cmd.CommandType = CommandType.StoredProcedure;

                    if (SQLParameters != null && SQLParameters.Length > 0)
                        cmd.Parameters.AddRange(SQLParameters);

                    var dt = new DataTable(TableName);

                    dt.Load(cmd.ExecuteReader());

                    cmd.Parameters.Clear();
                    return dt;
                }
            }
        }

    }
}
