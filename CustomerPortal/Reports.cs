﻿using CustomerPortal.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomerPortal.Data;
using System.Diagnostics;
using CustomerPortal.Controllers;
using System.IO;
using iText.Html2pdf;

namespace CustomerPortal
{
    public class Reports
    {
        protected readonly CustomerPortalContext CustomerPortal;
        private UserCredentials _credentials;

        public Reports(CustomerPortalContext context, UserCredentials credentials)
        {
            CustomerPortal = context;
            _credentials = credentials;
        }

        public ReportQuery GetReportQuery(ReportParams report)
        {
            ReportQuery result = null;
            string query = string.Empty;
            List<string> paramNames = new List<string>();
            List<SqlParameter> parameters = new List<SqlParameter>();

            if (report.Type == ReportType.UsageReport)
            {
                List<int> users = JsonConvert.DeserializeObject<List<int>>(report.Data);

                if (report.StartDate.HasValue)
                    parameters.Add(new SqlParameter("@startDate", report.StartDate.Value.ToShortDateString()));

                if (report.EndDate.HasValue)
                    parameters.Add(new SqlParameter("@endDate", report.EndDate.Value.ToShortDateString()));

                if (!report.SelectAll)
                {
                    for (int i = 0; i < users.Count; i++)
                    {
                        string paramName = $"@user{i}";
                        paramNames.Add(paramName);
                        parameters.Add(new SqlParameter(paramName, users[i]));
                    }
                }

                //TODO: Handle UTC correctly with date fields in excel export
                query = $@"
SELECT Logs.UserID AS User_ID,
       ISNULL(Users.EmailAddress, EmpBasic.EMailAddress) User_Email,
       LogType AS Log_Type,
       LogDateTime AS Log_Date_Time,
       --FORMAT(DATEADD(HOUR, DATEDIFF(HOUR, GETUTCDATE(), GETDATE()), LogDateTime), 'dd/MM/yyyy hh:mm:ss tt') LogDateTimeString,
       CASE
           WHEN LogText LIKE '%OrderNum%' THEN
               JSON_VALUE(LogText, '$.OrderNum')
           ELSE
               ''
       END AS [Order],
       CASE
           WHEN LogText LIKE '%Count%' THEN
               JSON_VALUE(LogText, '$.Count')
           ELSE
               NULL
       END Record_Count,
       CASE
           WHEN LogText LIKE '%Elapsed%' THEN
               JSON_VALUE(LogText, '$.Elapsed')
           ELSE
               NULL
       END Time_Elapsed,
       CASE
           WHEN LogType = 'Download Attachment' THEN
               CONCAT(
                         ISNULL(JSON_VALUE(LogText, '$.Name'), JSON_VALUE(LogText, '$.Description')),
                         ' [',
                         JSON_VALUE(LogText, '$.Category'),
                         ']'
                     )
           WHEN LogType = 'Retrieve Gallery' THEN
               JSON_VALUE(LogText, '$.gallery')
           WHEN LogText LIKE '%Name%' THEN
               JSON_VALUE(LogText, '$.Name')
           ELSE
               NULL
       END AS [Name],
       CASE
           WHEN LogText LIKE '%searchtext%' THEN
               JSON_VALUE(LogText, '$.searchText')
           ELSE
               NULL
       END AS Search_Text,
       CASE
           WHEN LogText LIKE '%ip%' THEN
               JSON_VALUE(LogText, '$.IPAddress')
           ELSE
               NULL
       END AS IP_Address
--,LogText
FROM customerportal.Logs
    LEFT JOIN customerportal.Users
        ON Users.UserID = Logs.UserID
    LEFT JOIN dbo.EmpBasic
        ON EmpID = CONVERT(NVARCHAR(10), Logs.UserID)
            AND MainCompany_c = 1
WHERE LogText like '{{%}}'
    AND LogType NOT IN ('Get Attachment')
    {(!report.IncludeEmployees ? "AND ISNULL(EmpBasic.EmpID, 0) = 0" : string.Empty)}
    {(!report.SelectAll ? $"AND Logs.UserID IN ({(paramNames.Count > 0 ? string.Join(",", paramNames) : "0")})" : $"AND ISNULL(Users.Admin, 0) = 0  AND ISNULL(Users.ITAdmin, 0) = 0")}
    {(report.StartDate.HasValue ? $"AND CONVERT(DATE, Logs.LogDateTime) >= @startDate" : string.Empty)}
    {(report.EndDate.HasValue ? $"AND CONVERT(DATE, Logs.LogDateTime) <= @endDate" : string.Empty)}
ORDER BY Logs.UserID, Logs.LogDateTime DESC;";
            }
            else if (report.Type == ReportType.OrderItems)
            {
                if (report.StartDate.HasValue)
                    parameters.Add(new SqlParameter("@startDate", report.StartDate.Value.ToShortDateString()));

                if (report.EndDate.HasValue)
                    parameters.Add(new SqlParameter("@endDate", report.EndDate.Value.ToShortDateString()));

                query = $@"
                SELECT vwOrderHeaders.OrderNum,
                       TRY_CONVERT(VARCHAR(20), OrderDate) AS Order_Date,
                       OTSName AS Ship_To,
                       Customer.Name AS Sold_To,
                       ISNULL(SumAttach.Invoices, 0) AS Invoices,
                       ISNULL(SumAttach.Billing_Files, 0) AS Billing_Files,
                       ISNULL(SumAttach.Attachments, 0) AS Attachments,
                       ISNULL(SumTasks.Events, 0) AS Events,
                       ISNULL(SumEvents.Tasks, 0) AS Tasks,
                       ISNULL(SumEvents.Complete_Tasks, 0) AS Complete_Tasks,
                       ISNULL(SumEvents.Incomplete_Tasks, 0) AS Incomplete_Tasks,
                       CONVERT(   BIT,
                                  CASE
                                      WHEN SpecialBilling_c = 1
                                           OR PayApp_c = 1 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ) AS Has_Special_Billing
                FROM dbo.vwOrderHeaders
                    LEFT JOIN
                    (
                        SELECT Attachments.OrderNum,
                               SUM(   CASE
                                          WHEN Category = 'billing'
                                               AND Billing.Company IS NOT NULL THEN
                                              1
                                          ELSE
                                              0
                                      END
                                  ) Invoices,
                               SUM(   CASE
                                          WHEN Category <> 'billing'
                                               AND Category LIKE '%billing%' THEN
                                              1
                                          ELSE
                                              0
                                      END
                                  ) Billing_Files,
                               SUM(   CASE
                                          WHEN Category NOT LIKE '%billing%' THEN
                                              1
                                          ELSE
                                              0
                                      END
                                  ) Attachments
                        FROM customerportal.Attachments
                            LEFT JOIN dbo.vwBillingDetail Billing
                                ON Billing.OrderNum = Attachments.OrderNum
                                   AND Category = 'billing'
                                   AND CONCAT(InvoiceNum, '.pdf') = FileName
                        GROUP BY Attachments.OrderNum
                    ) SumAttach
                        ON SumAttach.OrderNum = vwOrderHeaders.OrderNum
                    LEFT JOIN
                    (
                        SELECT OrderNum,
                               SUM(ScheduledOrderEventID) Events
                        FROM customerportal.ScheduledOrderEvents
                        GROUP BY OrderNum
                    ) SumTasks
                        ON SumTasks.OrderNum = vwOrderHeaders.OrderNum
                    LEFT JOIN
                    (
                        SELECT OrderNum,
                               SUM(TaskItemID) Tasks,
                               SUM(   CASE Complete
                                          WHEN 1 THEN
                                              1
                                          ELSE
                                              0
                                      END
                                  ) Complete_Tasks,
                               SUM(   CASE Complete
                                          WHEN 0 THEN
                                              1
                                          ELSE
                                              0
                                      END
                                  ) Incomplete_Tasks
                        FROM customerportal.TaskItems
                        GROUP BY OrderNum
                    ) SumEvents
                        ON SumEvents.OrderNum = vwOrderHeaders.OrderNum
                    JOIN dbo.Customer
                        ON Customer.Company = vwOrderHeaders.Company
                           AND Customer.CustNum = vwOrderHeaders.CustNum
		                   AND ICCust = 0
                WHERE ICPONum = 0
                    {(report.StartDate.HasValue ? $"AND OrderDate >= @startDate" : string.Empty)}
                    {(report.EndDate.HasValue ? $"AND OrderDate <= @endDate" : string.Empty)}
                ORDER BY OrderDate DESC;";
            }
            else if (report.Type == ReportType.UserList)
            {
                query = $@"
                SELECT Users.UserID,
                       Users.EmailAddress,
                       Users.Verified AS Active,
                       Users.Master,
                       Users.ParentID,
                       Users.City,
                       Users.State,
                       Users.FirstName,
                       Users.LastName,
                       Users.Company,
                       Users.AgreementSigned,
                       Users.PromptNewPassword,
                       Users.Title,
                       Users.LastLogin,
                       Users.LimitedAccess,
                       Users.OrderList,
                       Users.DateCreatedUTC,
                       Users.CreatedByID,
                       CASE
                           WHEN Users.DateCreatedUTC IS NOT NULL THEN
                               ISNULL(Creator.EmailAddress, 'Automation')
                           ELSE
                               ISNULL(Creator.EmailAddress, 'N/A')
                       END AS CreatedBy
                FROM customerportal.Users
                    LEFT JOIN customerportal.Users Creator
                        ON Users.CreatedByID = Creator.UserID
                WHERE Users.Admin = 0
                ORDER BY Users.UserID;";
            }
            else if (report.Type == ReportType.UserSalesCS)
            {
                query = $@"SELECT Users.UserID,
       Users.EmailAddress,
       Users.FirstName,
       Users.LastName,
       Users.Active,
       --Users.Master,
       --Users.ParentID,
       --Users.City,
       --Users.State,
       Users.Company,
       Users.Title,
       --Users.AgreementSigned,
       --Users.PromptNewPassword,
       Sales_Reps,
       Sales_Regions,
       Account_Types,
       CS_Reps,
       Users.Tiers,
       FOCUS_Account,
       Users.DateCreatedUTC,
       Users.LastLogin,
       --Users.LimitedAccess,
       --Users.OrderList,
       --Users.CreatedByID,
       CreatedBy
    FROM dbo.vwFCP_UserSalesCS Users
ORDER BY Users.UserID;";
            }
            else if (report.Type == ReportType.UserSalesCS_Created)
            {
                string criteria = string.Empty;

                if (report.StartDate.HasValue || report.EndDate.HasValue)
                {
                    List<string> list = new List<string>();
                    if (report.StartDate.HasValue)
                    {
                        list.Add("CONVERT(DATE, rpt.DateCreatedUTC) >= @startDate");
                        parameters.Add(new SqlParameter("@startDate", report.StartDate.Value.Date.ToShortDateString()));
                    }

                    if (report.EndDate.HasValue)
                    {
                        list.Add("CONVERT(DATE, rpt.DateCreatedUTC) <= @endDate");
                        parameters.Add(new SqlParameter("@endDate", report.EndDate.Value.Date.ToShortDateString()));
                    }

                    criteria = $"WHERE {string.Join(" AND ", list)}";
                }

                query = $@"
SELECT rpt.UserID,
       rpt.EmailAddress,
       rpt.FirstName,
       rpt.LastName,
       rpt.Active,
       rpt.Company,
       rpt.Title,
       rpt.Sales_Reps,
       rpt.Sales_Regions,
       rpt.Account_Types,
       rpt.CS_Reps,
       rpt.Tiers,
       rpt.FOCUS_Account,
       rpt.DateCreatedUTC,
       rpt.CreatedBy
FROM dbo.vwFCP_UserSalesCS rpt
{criteria}
ORDER BY rpt.UserID;";
            }
            else if (report.Type == ReportType.UserSalesCS_LastLogin)
            {
                string criteria = string.Empty;

                if (report.StartDate.HasValue || report.EndDate.HasValue)
                {
                    List<string> list = new List<string>();
                    if (report.StartDate.HasValue)
                    {
                        list.Add("CONVERT(DATE, rpt.LastLogin) >= @startDate");
                        parameters.Add(new SqlParameter("@startDate", report.StartDate.Value.Date.ToShortDateString()));
                    }

                    if (report.EndDate.HasValue)
                    {
                        list.Add("CONVERT(DATE, rpt.LastLogin) <= @endDate");
                        parameters.Add(new SqlParameter("@endDate", report.EndDate.Value.Date.ToShortDateString()));
                    }

                    criteria = $"WHERE {string.Join(" AND ", list)}";
                }

                query = $@"
SELECT rpt.UserID,
       rpt.EmailAddress,
       rpt.FirstName,
       rpt.LastName,
       rpt.Active,
       rpt.Company,
       rpt.Title,
       rpt.Sales_Reps,
       rpt.Sales_Regions,
       rpt.Account_Types,
       rpt.CS_Reps,
       rpt.Tiers,
       rpt.FOCUS_Account,
       rpt.LastLogin,
       rpt.CreatedBy
FROM dbo.vwFCP_UserSalesCS rpt
{criteria}
ORDER BY rpt.UserID;";
            }
            else if (report.Type == ReportType.MasterUsageReport)
            {

                List<int> users = JsonConvert.DeserializeObject<List<int>>(report.Data);

                if (report.StartDate.HasValue)
                    parameters.Add(new SqlParameter("@startDate", report.StartDate.Value.Date.ToShortDateString()));

                if (report.EndDate.HasValue)
                    parameters.Add(new SqlParameter("@endDate", report.EndDate.Value.Date.ToShortDateString()));

                if (!report.SelectAll)
                {
                    for (int i = 0; i < users.Count; i++)
                    {
                        string paramName = $"@user{i}";
                        paramNames.Add(paramName);
                        parameters.Add(new SqlParameter(paramName, users[i]));
                    }
                }


                query = $@"SELECT Logs.UserID AS User_ID,
       ISNULL(Users.EmailAddress, EmpBasic.EMailAddress) User_Email,
       LogType AS Log_Type,
       LogDateTime AS Log_Date_Time,
       --FORMAT(DATEADD(HOUR, DATEDIFF(HOUR, GETUTCDATE(), GETDATE()), LogDateTime), 'dd/MM/yyyy hh:mm:ss tt') LogDateTimeString,
       CASE
           WHEN LogText LIKE '%OrderNum%' THEN
               JSON_VALUE(LogText, '$.OrderNum')
           ELSE
               ''
       END AS [Order],
       CASE
           WHEN LogText LIKE '%Count%' THEN
               JSON_VALUE(LogText, '$.Count')
           ELSE
               NULL
       END Record_Count,
       CASE
           WHEN LogText LIKE '%Elapsed%' THEN
               JSON_VALUE(LogText, '$.Elapsed')
           ELSE
               NULL
       END Time_Elapsed,
       CASE
           WHEN LogType = 'Download Attachment' THEN
               CONCAT(
                         ISNULL(JSON_VALUE(LogText, '$.Name'), JSON_VALUE(LogText, '$.Description')),
                         ' [',
                         JSON_VALUE(LogText, '$.Category'),
                         ']'
                     )
           WHEN LogType = 'Retrieve Gallery' THEN
               JSON_VALUE(LogText, '$.gallery')
           WHEN LogText LIKE '%Name%' THEN
               JSON_VALUE(LogText, '$.Name')
           ELSE
               NULL
       END AS [Name],
       CASE
           WHEN LogText LIKE '%searchtext%' THEN
               JSON_VALUE(LogText, '$.searchText')
           ELSE
               NULL
       END AS Search_Text,
       CASE
           WHEN LogText LIKE '%ip%' THEN
               JSON_VALUE(LogText, '$.IPAddress')
           ELSE
               NULL
       END AS IP_Address,
       Users.FirstName,
       Users.LastName,
       Users.Active,
       Users.Company,
       Users.Title,
       Users.Sales_Reps,
       Users.Sales_Regions,
       Users.Account_Types,
       Users.CS_Reps,
       Users.FOCUS_Account,
       Users.DateCreatedUTC,
       Users.LastLogin,
       Users.CreatedBy
--,LogText
FROM customerportal.Logs
    LEFT JOIN
    (
        SELECT tmpUsers.UserID,
               tmpUsers.EmailAddress,
               tmpUsers.FirstName,
               tmpUsers.LastName,
               tmpUsers.Verified AS Active,
               --tmpUsers.Master,
               --tmpUsers.ParentID,
               --tmpUsers.City,
               --tmpUsers.State,
               tmpUsers.Company,
               tmpUsers.Title,
               tmpUsers.Admin,
               tmpUsers.ITAdmin,
               --tmpUsers.AgreementSigned,
               --tmpUsers.PromptNewPassword,
               STUFF(
               (
                   SELECT DISTINCT
                          CONCAT(', ', Name)
                   --CONCAT(', ', Name, ' (', ISNULL(NULLIF(tmpSales.SalesTerritory, ''), 'N/A'), ')')
                   FROM customerportal.AccountOrders tmp
                       JOIN dbo.vwOrderHeaders tmpOrders
                           ON tmpOrders.OrderNum = tmp.OrderNum
                       JOIN dbo.vwSalesHierarchy tmpSales
                           ON SalesRep1_c = SalesRepCode
                              AND MainCompany_c = 1
                   WHERE tmp.UserID = ISNULL(tmpUsers.ParentID, tmpUsers.UserID)
                   FOR XML PATH('')
               ),
               1,
               2,
               ''
                    ) AS Sales_Reps,
               STUFF(
               (
                   SELECT DISTINCT
                          CONCAT(', ', tmpSales.SalesTerritory)
                   FROM customerportal.AccountOrders tmp
                       JOIN dbo.vwOrderHeaders tmpOrders
                           ON tmpOrders.OrderNum = tmp.OrderNum
                       JOIN dbo.vwSalesHierarchy tmpSales
                           ON SalesRep1_c = SalesRepCode
                              AND MainCompany_c = 1
                   WHERE tmp.UserID = ISNULL(tmpUsers.ParentID, tmpUsers.UserID)
                         AND tmpSales.SalesTerritoryID IS NOT NULL
                   FOR XML PATH('')
               ),
               1,
               2,
               ''
                    ) AS Sales_Regions,
               STUFF(
               (
                   SELECT DISTINCT
                          CONCAT(', ', tmpAccount.Type)
                   FROM customerportal.AccountOrders tmp
                       JOIN dbo.vwOrderHeaders tmpOrders
                           ON tmpOrders.OrderNum = tmp.OrderNum
                       JOIN salesforce.Opportunity tmpOpps
                           ON TRY_CONVERT(INT, tmpOpps.Order_Number__c) = tmp.OrderNum
                       JOIN salesforce.Account tmpAccount
                           ON tmpAccount.Id = tmpOpps.AccountId
                       JOIN dbo.vwSalesHierarchy tmpSales
                           ON SalesRep1_c = SalesRepCode
                              AND MainCompany_c = 1
                   WHERE tmp.UserID = ISNULL(tmpUsers.ParentID, tmpUsers.UserID)
                         AND tmpAccount.Type <> ''
                   FOR XML PATH('')
               ),
               1,
               2,
               ''
                    ) AS Account_Types,
               STUFF(
               (
                   SELECT DISTINCT
                          CONCAT(', ', tmpSales.CSRepName)
                   FROM customerportal.AccountOrders tmp
                       JOIN dbo.vwOrderHeaders tmpOrders
                           ON tmpOrders.OrderNum = tmp.OrderNum
                       JOIN dbo.vwSalesHierarchy tmpSales
                           ON SalesRep1_c = SalesRepCode
                              AND MainCompany_c = 1
                   WHERE tmp.UserID = ISNULL(tmpUsers.ParentID, tmpUsers.UserID)
                         AND tmpSales.CSRepID IS NOT NULL
                   FOR XML PATH('')
               ),
               1,
               2,
               ''
                    ) AS CS_Reps,
                CASE
                    WHEN
                        (SELECT COUNT(*)
                        FROM customerportal.AccountOrders tmp
                            JOIN salesforce.Opportunity tmpOpps
                                ON TRY_CONVERT(INT, tmpOpps.Order_Number__c) = tmp.OrderNum
                            JOIN salesforce.Account tmpAccount
                                ON tmpAccount.Id = tmpOpps.AccountId
                        WHERE tmpAccount.FOCUS_Account__c = 'Yes'
                            AND tmp.UserID = ISNULL(tmpUsers.ParentID, tmpUsers.UserID))
                        > 0 
                    THEN 'Y' 
                    ELSE 'N' END
                AS FOCUS_Account,
               tmpUsers.DateCreatedUTC,
               tmpUsers.LastLogin,
               --tmpUsers.LimitedAccess,
               --tmpUsers.OrderList,
               --tmpUsers.CreatedByID,
               CASE
                   WHEN tmpUsers.DateCreatedUTC IS NOT NULL THEN
                       ISNULL(Creator.EmailAddress, 'Automation')
                   ELSE
                       ISNULL(Creator.EmailAddress, 'N/A')
               END AS CreatedBy
        FROM customerportal.Users tmpUsers
            LEFT JOIN customerportal.Users Creator
                ON tmpUsers.CreatedByID = Creator.UserID
    ) Users
        ON Users.UserID = Logs.UserID
    LEFT JOIN dbo.EmpBasic
        ON EmpID = CONVERT(NVARCHAR(10), Logs.UserID)
           AND MainCompany_c = 1
WHERE LogText LIKE '{{%}}'
      AND LogType NOT IN ( 'Get Attachment', 'Retrieve Photo', 'Retrieve Gallery' )
      AND ISNULL(Users.Admin, 0) = 0  AND ISNULL(Users.ITAdmin, 0) = 0 AND Logs.UserID > 0
    {(!report.IncludeEmployees ? "AND ISNULL(EmpBasic.EmpID, 0) = 0" : string.Empty)}
    {(!report.SelectAll ? $"AND Logs.UserID IN ({(paramNames.Count > 0 ? string.Join(",", paramNames) : "0")})" : string.Empty)}
    {(report.StartDate.HasValue ? $"AND CONVERT(DATE, Logs.LogDateTime) >= @startDate" : string.Empty)}
    {(report.EndDate.HasValue ? $"AND CONVERT(DATE, Logs.LogDateTime) <= @endDate" : string.Empty)}
ORDER BY Logs.UserID,
         Logs.LogDateTime DESC;";
            }
            result = new ReportQuery(query, parameters.ToArray());

            return result;
        }

        public string GenerateSurveyHtml(int completionId)
        {

            var completion = CustomerPortal.SurveyComplete.Single(p => p.CompletionID == completionId);
            var answers = CustomerPortal.SurveyAnswer.Where(p => p.CompletionID == completionId).ToList();
            var survey = CustomerPortal.Surveys.Single(p => p.SurveyID == completion.SurveyID);
            var questions = CustomerPortal.SurveyQuestions.Where(p => p.SurveyID == completion.SurveyID).ToList();

            var html = new StringBuilder($@"<style>* {{ font-family: Arial, Helvetica, sans-serif;}}</style>
                <div style=""border-bottom: 1px solid lightgray; margin-bottom:4px; padding-bottom:2px; width: 100%;"">    
                    <div style=""display:flex; flex-direction:row; justify-content:space-between;"">
                        <div>
                            <span style="""">Customer Survey - Order #<span style=""font-weight:bold;"">{completion.OrderNum}</span></span>
                        </div>
                        <div style=""margin-left:250px;"">
                            <div style=""display:flex; flex-direction:column;"">
                                <div><span style=""font-size: 8pt;"">Completed By: {completion.CompletedBy}</span></div>                        
                                <div><span style=""font-size: 8pt;"">Completed On: {completion.CompleteDate.ToLocalTime():MMMM dd, yyyy hh:mm tt} {string.Empty}</span></div>                      
                            </div>
                        </div>
                    </div>
                  </div>");

            int counter = 0;
            foreach (var question in questions)
            {
                counter++;
                var answer = answers.Single(p => p.QuestionID == question.QuestionID && p.SubQuestionID == question.SubQuestionID);
                string text = string.Empty;

                switch (Enum.Parse(typeof(SurveyQuestionType), question.QuestionType))
                {
                    case SurveyQuestionType.Boolean:
                        bool.TryParse(answer.Answer, out bool boolean);
                        text = $@"
    <div>
      <input id=""input-{counter}-{completionId}"" role=""checkbox"" type=""checkbox"" value="""" {(boolean ? "checked" : "")}/>
      <label for=""input-{counter}-{completionId}"" style=""left: 0px; right: auto; position: relative"">{question.Label}</label>
    </div>";
                        break;
                    case SurveyQuestionType.Range:
                        int.TryParse(answer.Answer, out int integer);
                        var table = new StringBuilder($@"<div><table style='width: 100%;'><thead><tr><th></th>");

                        for (int i = question.QuestionDetails.MinValue; i <= question.QuestionDetails.MaxValue; i++)
                        {
                            table.Append($"<th style='text-align: center;'>{i}</th>");
                        }
                        table.Append($@"<th></th></tr></thead><tbody><tr><td style=""text-align: right; padding-right: 20px; width: 300px;"">{question.QuestionDetails.MinText ?? "Strongly Disagree"}</td>");

                        for (int i = question.QuestionDetails.MinValue; i <= question.QuestionDetails.MaxValue; i++)
                        {
                            table.Append($@"
            <td>
              <div style=""height: auto; text-align: center;"">
                <div role=""radiogroup"">
                  <input id=""input-{counter}-{i}-{completionId}"" role=""radio"" type=""radio"" value=""{i}"" {(i == integer ? "checked" : "")}/>
                </div>
              </div>
            </td>");
                        }


                        table.Append($@"<td style=""text-align: left; padding-left: 20px; width: 300px;"">{question.QuestionDetails.MaxText ?? "Strongly Agree"}</td></tr></tbody></table></div>");

                        text = table.ToString();

                        break;
                    case SurveyQuestionType.ShortText:
                    case SurveyQuestionType.Phone:
                        text = $@"
      <label for=""input-{counter}-{completionId}"" style=""left: 0px; right: auto; position: relative"">{question.Label}</label>
    <div id=""input-{counter}-{completionId}"" readonly=""readonly"" style=""border: 1px solid lightgray; padding: 10px; margin: 10px 0px; border-radius: 5px;width: 95%;"">{answer.Answer}</div>";
                        break;
                    case SurveyQuestionType.LongText:
                        text = $@"
    <div id=""input-{counter}-{completionId}"" readonly=""readonly"" rows=""5"" style=""border: 1px solid lightgray; padding: 10px; margin: 10px 0px; border-radius: 5px;width: 95%;min-height: 20px;"">{answer.Answer}</div>";
                        break;
                    default:
                        text = answer.Answer;
                        break;
                }

                html.Append($@"<div>{question.InstructionsHTML}</div><div style='font-weight: bold; margin-top: 20px; margin-bottom: 10px;'>{question.QuestionTextHTML}</div>{text}");

            }

            html.Append("<div style='page-break-after: always;' ></div>");
            return html.ToString();
        }


        public byte[] GenerateSurveyPdf(int[] completionIds)
        {
            byte[] data = null;
            try
            {
                Stopwatch timer = new Stopwatch();
                timer.Start();
                //Installation.DefaultRenderingEngine = IronPdf.Rendering.PdfRenderingEngine.Chrome;
                //ChromePdfRenderer Renderer = new ChromePdfRenderer();

                //int PDFMargin = 10;



                var html = string.Empty;

                foreach (int completionId in completionIds)
                {
                    html += GenerateSurveyHtml(completionId);
                }


                ConverterProperties props = new ConverterProperties();

                using (var ms = new MemoryStream())
                {
                    HtmlConverter.ConvertToPdf(html, ms, props);
                    return ms.ToArray();
                }
            

            // var Renderer = new IronPdf.HtmlToPdf();

            //Renderer.RenderingOptions.MarginBottom = PDFMargin + 10;
            //Renderer.RenderingOptions.MarginLeft = PDFMargin;
            //Renderer.RenderingOptions.MarginRight = PDFMargin;
            //Renderer.RenderingOptions.MarginTop = PDFMargin;

            //Renderer.RenderingOptions.FirstPageNumber = 2;

            //Renderer.RenderingOptions.HtmlHeader = new HtmlHeaderFooter()
            //{
            //    Height = 30,
            //    Spacing = 5,
            //    DrawDividerLine = false,
            //    LoadStylesAndCSSFromMainHtmlDocument = true
            //};

            //Renderer.RenderingOptions.HtmlFooter = new HtmlHeaderFooter()
            //{
            //    //Height = 15,
            //    HtmlFragment = @"<div style='display: flex; font-family: Roboto;border-top: 1px solid lightgray; padding-top: 10px; margin: 00px 40px 20px 40px;'>{date} {time} <div style='flex: 1;'></div> {page} of {total-pages}</div>",
            //    //HtmlFragment = "<center style='font-family: Roboto;border-top: 1px solid lightgray; margin-bottom: 20px'>{page} of {total-pages}</center>",
            //    DrawDividerLine = true,
            //    LoadStylesAndCSSFromMainHtmlDocument = false
            //};

                timer.Stop();

                string genLogType = "HTML Generated";

                CustomerPortal.Logs.Add(new Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = genLogType,
                    UserID = _credentials.UserID,
                    LogText = html
                });

                //string logType = "PDF Exported";
                //var logObj = new { CompletionIds = completionIds, completionIds.Length, timer.Elapsed, _credentials.IPAddress };

                //CustomerPortal.Logs.Add(new Log
                //{
                //    LogDateTime = DateTime.UtcNow,
                //    LogType = logType,
                //    UserID = _credentials.UserID,
                //    LogText = JsonConvert.SerializeObject(logObj)
                //});

                Logger.LogIt(genLogType, html);
                //Logger.LogIt(logType, logObj);

                CustomerPortal.SaveChanges();

                //var pdf = Renderer.RenderHtmlAsPdf(html);

                //data = pdf.BinaryData;
                                

                using (var ms = new MemoryStream())
                {
                    HtmlConverter.ConvertToPdf(html, ms, props);
                    data = ms.ToArray();
                }

            }
            catch (Exception e)
            {

                CustomerPortal.Logs.Add(new Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = "Error Exporting PDF",
                    UserID = _credentials.UserID,
                    LogText = JsonConvert.SerializeObject(new { e.Message })
                });

                CustomerPortal.SaveChanges();
            }

            return data;
        }

        public byte[] RetrieveSurveyPdf(int completionId)
        {
            var survey = CustomerPortal.SurveyComplete.Single(p => p.CompletionID == completionId);
            string path = GetSurveyPath(survey.OrderNum);
            FileInfo file = new FileInfo(path);

            if (!file.Exists)
            {
                SaveSurveyPdf(completionId);
                file.Refresh();
            }

            return File.ReadAllBytes(path);
        }

        public void SaveSurveyPdf(int completionId)
        {
            byte[] data = GenerateSurveyPdf(new int[] { completionId });

            if (data != null)
            {
                int orderNum = CustomerPortal.SurveyComplete.Single(p => p.CompletionID == completionId).OrderNum;
                string path = GetSurveyPath(orderNum);
                File.WriteAllBytes(path, data);
                CustomerPortal.Logs.Add(new Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = "Save Survey PDF",
                    UserID = _credentials.UserID,
                    LogText = JsonConvert.SerializeObject(new { completionId, orderNum, path })
                });
            }
        }

        static string GetSurveyPath(int orderNum) => Path.Combine(Constants.Directories.Surveys, $"{orderNum}.pdf");

        public static string GetReportTitle(ReportType type)
        {
            string title;

            switch (type)
            {
                case ReportType.UsageReport:
                    title = "Usage Report";
                    break;
                case ReportType.OrderItems:
                    title = "Order Items";
                    break;
                case ReportType.UserList:
                    title = "User List";
                    break;
                case ReportType.UserSalesCS:
                    title = "User Sales & CS Reps";
                    break;
                case ReportType.UserSalesCS_Created:
                    title = "User Sales & CS Reps (by Date Created)";
                    break;
                case ReportType.UserSalesCS_LastLogin:
                    title = "User Sales & CS Reps (by Last Login)";
                    break;
                case ReportType.MasterUsageReport:
                    title = "Master Usage Report";
                    break;
                default:
                    title = type.ToString();
                    break;
            }

            return title;
        }
    }

    public enum ReportType
    {
        UsageReport,
        OrderItems,
        UserList,
        UserSalesCS,
        UserSalesCS_Created,
        UserSalesCS_LastLogin,
        MasterUsageReport
    }

    public class ReportParams
    {
        public ReportType Type { get; set; }
        public string Data { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool SelectAll { get; set; } = false;
        public bool IncludeEmployees { get; set; } = false;
    }

    public class ReportParamsAccessibility
    {
        public ReportParamsAccessibility(ReportType type)
        {
            Type = type;
        }
        public ReportType Type { get; private set; }
        //public bool DataEnabled => new ReportType[] { ReportType.UserSalesCS }.Contains(Type);
        public bool DatesEnabled => !new ReportType[] { ReportType.UserList, ReportType.UserSalesCS }.Contains(Type);
        public bool ControlsEnabled => !new ReportType[] { ReportType.OrderItems, ReportType.UserList, ReportType.UserSalesCS, ReportType.UserSalesCS_Created, ReportType.UserSalesCS_LastLogin }.Contains(Type);
    }

    public class ReportQuery
    {
        public string Query { get; set; }
        public SqlParameter[] Parameters { get; set; }

        public ReportQuery(string query, SqlParameter[] parameters)
        {
            Query = query;
            Parameters = parameters;
        }
    }

    public class PDFGeneratorSettings
    {
        public bool Landscape { get; set; }
        public int FontSize { get; set; } = 5;
        public string TopRightHTML { get; set; }
        public string BottomCenterHTML { get; set; }
        public string TopCenterHTML { get; set; }
        public bool ShowRunDate { get; set; } = true;
        public string AdditionalStyle { get; set; }
        public bool ShowPageNumber { get; set; } = true;
    }
}
