﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Models
{
    public class SurveyAnswer
    {
        public int CompletionID { get; set; }
        //public int SurveyID { get; set; }
        public int QuestionID { get; set; }
        public int SubQuestionID { get; set; }
        public int AnswerValue { get; set; }
        public string AnswerText { get; set; }
        public string Answer { get; set; }
    }
}
