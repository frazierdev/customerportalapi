﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Models
{
    public class AccountOrders
    {
        public int UserID { get; set; }
        public int OrderNum { get; set; }
        public DateTime? LastAccessed { get; set; }
        public DateTime DateAddedUTC { get; set; }
    }
}
