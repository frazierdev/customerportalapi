﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomerPortal.Models
{
    public class SurveyComplete
    {
        public int SurveyID { get; set; }
        public int CompletionID { get; set; }
        public int OrderNum { get; set; }
        public string CompletedBy { get; set; }
        public DateTime CompleteDate { get; set; }
        public DateTime? SyncedToSF { get; set; }
        public int? AdminUserID { get; set; }
        
        [NotMapped]
        public string AdminUserName { get; set; }            
        [NotMapped]
        public double RangeAnswerAverage { get; set; }
    }
}
