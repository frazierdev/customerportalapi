﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Models
{
    public class Attachments
    {
        public int AttachmentID { get; set; }
        public string FileLocation { get; set; }
        public int OrderNum { get; set; }
        public string Category { get; set; }
        public string FileType { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public int Size { get; set; }
        public DateTime? DateAdded { get; set; }
        public int AttachedByID { get; set; }
        public string AttachedBy { get; set; }
    }
}
