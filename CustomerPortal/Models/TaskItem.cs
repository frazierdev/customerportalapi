﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Models
{
    public class TaskItem
    {
		public int TaskItemID { get; set; }

		public int OrderNum { get; set; }

		public string Description { get; set; }

		public bool Complete { get; set; }

		public DateTime? DueDate { get; set; }

		public DateTime DateAdded { get; set; }

		public int AddedByID { get; set; }

		public string AddedBy { get; set; }
	}
}
