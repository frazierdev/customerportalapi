﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Models
{
    public class Log
    {

        public int LogID { get; set; }
        public DateTime LogDateTime { get; set; }
        public int UserID { get; set; }
        public string LogType { get; set; }
        public string LogText { get; set; }
    }
}
