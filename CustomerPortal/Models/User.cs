﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CustomerPortal.Models
{
    public class User
    {
        public int UserID { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string PassKey { get; set; }
        public bool Admin { get; set; }
        public bool Verified { get; set; }
        public bool Master { get; set; }
        public int? ParentID { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Title { get; set; }
        public bool AgreementSigned { get; set; }
        public bool PromptNewPassword { get; set; }
        public bool ITAdmin { get; set; }
        public bool LimitedAccess { get; set; }
        public string OrderList { get; set; }
        public DateTime? LastLogin { get; set; }
        //public DateTime? DateCreatedUTC { get; set; }
        public string PlainText { get; set; }
        public int CreatedByID { get; set; }
        public int?[] orders
        { 
            get
            {
                return JsonConvert.DeserializeObject<int?[]>(OrderList ?? "");
            } 
        }
    }
}
