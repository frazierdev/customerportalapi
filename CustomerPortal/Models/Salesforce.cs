﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Models
{
    public class vwEmailOrderLink
    {
        public string Quote_Number__c { get; set; }
        public string Order_Number__c { get; set; }
        public string Email { get; set; }
    }


    public class vwContactOrder
    {
        public string Quote_Number__c { get; set; }
        public string Order_Number__c { get; set; }
        public string Email { get; set; }
    }

    public class vwAccountOrder
    {
        public string Quote_Number__c { get; set; }
        public string Order_Number__c { get; set; }
        public string Email { get; set; }
    }

}
