﻿using System;

namespace CustomerPortal.Models
{
    public class Surveys
    {
        public int SurveyID { get; set; }
        public string Name { get; set; }
        public string DescriptionHTML { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime LastUpdated { get; set; }
        public bool Active { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public bool Locked { get; set; }
    }
}
