﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Models
{
    public class Photos
    {
        //public static string DefaultDirectory { get { return @"\\nj-epicor-fs\EpicorData\TestPhotos\"; } }
#if DEBUG
        public static string DefaultDirectory { get { return @"\\nj-web-03\CSData$\TEST\Photos\"; } }
#else
        public static string DefaultDirectory { get { return @"\\nj-web-03\CSData$\Photos\"; } }
#endif
        public int PhotoID { get; set; }
        public string FileName { get; set; }
    }
}
