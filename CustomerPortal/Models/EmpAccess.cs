﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Models
{
    public class EmpAccess
    {
        public int AccessID { get; set; }
        public int UserID { get; set; }
    }
}
