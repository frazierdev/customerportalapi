﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomerPortal.Models
{
    public enum SurveyQuestionType
    {
        Range,
        Boolean,
        ShortText,
        LongText,
        Phone
    }

    public class SurveyQuestions
    {
        public int SurveyID { get; set; }
        public int QuestionID { get; set; }
        public int SubQuestionID { get; set; }
        public string QuestionType { get; set; }
        //public SurveyQuestionType QuestionType { get { return Enum.Parse(SurveyQuestionTyp); set; }
        public string InstructionsHTML { get; set; }
        public string QuestionTextHTML { get; set; }
        public string DetailsJSON { get; set; } // = JsonConvert.SerializeObject(new SurveyQuestionDetails());
        public bool Required { get; set; }
        public string Label { get; set; }
        public string ExcelHeader { get; set; }

        [NotMapped]
        public SurveyQuestionDetails QuestionDetails
        {
            get { return JsonConvert.DeserializeObject<SurveyQuestionDetails>(DetailsJSON); }
            set { DetailsJSON = JsonConvert.SerializeObject(value ?? new SurveyQuestionDetails()); }
        }

        public SurveyQuestions() { }

        public SurveyQuestions(int surveyID, int questionID) {
            SurveyID = surveyID;
            QuestionID = questionID;
            QuestionType = SurveyQuestionType.ShortText.ToString();
            QuestionDetails = new SurveyQuestionDetails();
            //DetailsJSON = JsonConvert.SerializeObject(new SurveyQuestionDetails());
        }
    }

    public class SurveyQuestionDetails
    {
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public string MinText { get; set; }
        public string MaxText { get; set; }
        public bool ShowQuestionID { get; set; }
        public bool ShowSubQuestionID { get; set; }
        //public string PlaceHolder { get; set; }
        //public string RulesJS { get; set; }
    }
}
