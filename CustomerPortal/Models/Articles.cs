﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Models
{
    public class Articles
    {
        public int ArticleID { get; set; }
        public string Author { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Headline { get; set; }
        public string Body { get; set; }
        public DateTime LastModifiedDate {get;set;}
        public DateTime? PublishedDate { get; set; }
    }
}
