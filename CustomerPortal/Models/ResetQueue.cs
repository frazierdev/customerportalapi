﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Models
{
    public class ResetQueue
    {
        public Guid ResetID { get; set; }
        public int UserID { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}
