﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Models
{
    public class ScheduledOrderEvents
    {
        public int ScheduledOrderEventID { get; set; }
        public int OrderNum { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Title { get; set; }
        public int AddedByID { get; set; }
        public string Color { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
