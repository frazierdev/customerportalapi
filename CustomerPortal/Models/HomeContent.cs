﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CustomerPortal.Models
{
    public class HomeContent
    {
        public static List<string> ReadOnlyFields { get { return new List<string>() { "ID" }; } }
        public int ID { get; set; }
        public bool Active { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
    }
}
