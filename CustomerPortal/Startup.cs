﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomerPortal.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
//using Microsoft.Owin.Cors;
using Newtonsoft.Json.Serialization;

namespace CustomerPortal
{
    public class Startup
    {
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
#if DEBUG
            string customerPortal = "CustomerPortalDevConnection";
            string salesforce = "SalesforceDevConnection";
#else
            string customerPortal = "CustomerPortalConnection";
            string salesforce = "SalesforceConnection";
#endif
            //IronPdf.Logging.Logger.EnableDebugging = true;
            //IronPdf.Logging.Logger.LogFilePath = "Default.log"; //May be set to a directory name or full file
            //IronPdf.Logging.Logger.LoggingMode = IronPdf.Logging.Logger.LoggingModes.All;
            //IronPdf.License.LicenseKey = "IRONPDF.FRAZIERINDUSTRIALCOMPANY.IRO210525.4192.68149.525012-6A12CBFFB0-IBAV2QXXEPLQT-B2K3SF3PQFXI-6UHIWSBGTKOO-36EMVQOIYDLK-LFSESKMFACMQ-MTI3Q5-LSIRHDLDZLWFUA-PROFESSIONAL.SUB-Y3AXIG.RENEW.SUPPORT.25.MAY.2022";

            services.AddDbContext<CustomerPortalContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString(customerPortal)));

            services.AddDbContext<SalesforceContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString(salesforce)));

            //services.AddOptions();

            services.AddSingleton<Utilities>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
               .AddJwtBearer(options =>
               {
                   options.TokenValidationParameters = new TokenValidationParameters
                   {
                       ValidateIssuer = true,
                       ValidateAudience = true,
                       ValidateLifetime = true,
                       ValidateIssuerSigningKey = true,
                       ValidIssuer = "frazier.com",
                       ValidAudience = "frazier.com",
                       IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["SecurityKey"]))
                   };
               });

            //services.AddAuthorization(options =>
            //{
            //    options.AddPolicy("TrainedStaffOnly",
            //        policy => policy
            //        .RequireClaim("CompletedBasicTraining")
            //        .AddRequirements(new MinimumMonthsEmployedRequirement(3)));
            //});
#if DEBUG
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
                    //.AllowCredentials()) ;
            });
#endif

            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
            });


            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });

            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                //options.InputFormatters.Insert(0, new RawJsonBodyInputFormatter());
                //options.SuppressInferBindingSourcesForParameters = true;
            });

            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            #region Activate 3rd Party Licenses                  
            
            Spire.License.LicenseProvider.SetLicenseKey("NAEArcK2L7fnv+QVtXgJPbDmtdmm3pNdXe6DmqgY6EZIfvCiBUhXHwfR3MhvnycmwcN9xc6bSmakXcqCJ7VP3XjxpGmIOWO2j6PXI2ePKtNh6fREHRU5MYWT4LJZFNwCHuZxeJEkjI7J1HHaXfuO0t1tVtE9in1x4sQHXGbgKs5W6zkDmbLX2VlAGHmRLpsE7pgG39cz9EVbRHyE3OFaYByrIb448rOn1gPi0PsFbmdy8/ruvNixPTqsnAkE9T9/BpWVM2NvWkhAEllM/CiSg1+7Q1/bZ5zIULR7cGHH484xMAEkrjAqkExDbhsTwMJVUXmnpntvUPT0vFtuKkHHOVhVmAgaBJ6Kz3GaHzXSlwhcudFhbgyw1SpAfEDqGBysSr9M3ovWd0E/pjGUWYQhD4mrMcJ39TUZ+xTN18BAgi7Ac8tvleifYDHxbzLTbN6+fw/WGQiJNPRmwk3wyE5Skfqvxuoh8aF8IPbn07f6hmlis68dhNw1KE+8jboBHWCRaNtAGeqpxP8cQMPQX25LQDGLkifYoS7oqcxvQfAo1nad5oDXA8zodE6b/deb5emn0g7g1scP/Z/su56yz2PjZS0bc04/zQQ4nXWzzKYyJ3Zi1v8XmdPI3uZ7ryNePGnM6oMfYqdpEOcD8FwcSnMCxFGcLd4DxV/5DqDssNRdZNc0miEnhVLmFTM6w3xlfN/skQv4uSR98XHUCw5j3sWF+4t0s9s13KD+il/RRm3Y/T6bd95pTH4ISUFdhR+XprSMLwEMruVVGFk/UsZQ38gXbFNykBR49nEWInOo6lMhx1iZPx7Q+y6AjBEy+wiHBHRFnKgRiD/Dlbcp5JXEXJyz8a+XQ3QZZ4zihcAQerv1PZvnHPI5HsBNiHd+OAmsF+fVVdHGJ2H3EzZCG13LVy8LyBtNGV7ShME/ZPYpYqCQJPjAqc2jDQ1WzLvoC8jCHOnDafXo72aUMD0xeFn/lsaJ2NPOasqlsYaROg2/R5KW5tzl5Jp8IT+5hOYnGyDTbS4JTbggx/MNUTswwdPOG/HasvDAD0/E7A3sTw74r4BfHuz5R8F3fBE/HN6fU0kYR/1LFGF/b4KR4CiLogwNUT2wtXzI4C1VkTIvY3lnd71UaawP0mT24PGyNeGMsJbLVw8fFYjobissq8fM8tr7/Nki1Sb6PrO907kWUOdxHXU5wj2wUbWkBYLf2KR24PfxI61+tkrR/2PxjPWw+1XZ/pim7VFW8S5uD/jeFOXLciQc48PjX+fr5ufVe+BVZWkzK8afXiOjZUvNs4mLmBs5436lzq06Ga9j1VLzSZaCEOcJkuciEnhehhA68q5OZrTz8dtgNCmX9/DBkOe86cePjL0XpMQlRDCILfvW/t+QdwmAC7UbQtytm9OPla6pyGxGKVhAL9uvOpn4Ty9hXS96iZPKVQu7TZPyFKD1qfKCEzSwDZ92QgNl2BLUalFQo30EJf9yX0CwRKtvYJLsgsP/rrlUkQDg60bPx+QBkrSvb3u4OjBD9NvcH050es1odoBxdMi46DoLtmFBOngEjtz5d2p0q7jNXHrNfmNSacq778mKkDvKNQeo/5yumDtDBN5TuM49");
            Spire.License.LicenseProvider.LoadLicense();

            #endregion

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        //public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
#if DEBUG

            app.UseCors("CorsPolicy");
#endif
            app.UseAuthentication();
            app.UseForwardedHeaders();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseRouting();
            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapGet("/", async context =>
            //    {
            //        //await context.Response.BodyWriter.WriteAsync("Hello World!");
            //        Console.WriteLine("Hello World!");
            //    });
            //});

            //MvcOptions options = new MvcOptions();
            //options.EnableEndpointRouting = fa
            //app.UseMvc.ConfigureServices(options =>
            //{

            //});
            //app.UseMvcWithDefaultRoute();
            //app.UseMvcWithDefaultRoute
            app.UseMvc();
        }
    }
}
