﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using CustomerPortal.Data;
using CustomerPortal.Models;
using System.Diagnostics;

namespace CustomerPortal
{

    //public class Geolocation
    //{
    //    public double Latitude { get; set; }
    //    public double Longitude { get; set; }
    //    public string PlaceID { get; set; }
    //}
    enum MapView
    {
        Aerial,
        Street
    }

    public class GoogleAPI
    {
        private const string _key = "AIzaSyCpv4b2Dh3K3-nYXJc0XhPd1FYgvOuq7z4";

        readonly HttpClient _client = new HttpClient
        {
            BaseAddress = new Uri("https://maps.googleapis.com/maps/api/")
        };

        private CustomerPortalContext _customerPortal { get; set; }

        public GoogleAPI(CustomerPortalContext portalContext)
        {
            _customerPortal = portalContext;
        }

        public Geolocations GetGeolocation(string address)
        {
            Geolocations loc = null;
            if (!string.IsNullOrEmpty(address))
            { 

                loc = _customerPortal.Geolocations.FirstOrDefault(p => p.Address.ToUpper().Equals(address.ToUpper()));

                if (loc == null)
                {
                    string formatted = System.Web.HttpUtility.UrlEncode(address);

                    var data = JObject.Parse(CallAPI($"geocode/json?address={formatted}&key={_key}"));

                    string[] errors = new string[] { "ZERO_RESULTS", "OVER_QUERY_LIMIT" };

                    if (errors.Contains(data["status"].ToString()))
                        return null;

                    loc = new Geolocations();
                    loc.Address = address;
                    loc.PlaceID = data["results"][0]["place_id"].ToString();
                    loc.Latitude = data["results"][0]["geometry"]["location"]["lat"].ToObject<decimal>();
                    loc.Longitude = data["results"][0]["geometry"]["location"]["lng"].ToObject<decimal>();

                    _customerPortal.Geolocations.Add(loc);
                    _customerPortal.SaveChanges();
                }
            }

            return loc;
        }

        public byte[] GetStaticAerialMapImage(string address, int zoom = 15)
        {
            //ServicePointManager.Expect100Continue = true;
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
            //       | SecurityProtocolType.Tls11
            //       | SecurityProtocolType.Tls12
            //       | SecurityProtocolType.Ssl3;


            return GetMapImage(address, MapView.Aerial);
        }

        public byte[] GetStaticStreetViewImage(string address)
        {
            try
            {
                return GetMapImage(address, MapView.Street);
            }
            catch (Exception e)
            {
                Logger.LogException(e);
            }

            return GetMapImage(address, MapView.Aerial);
        }

        private byte[] GetMapImage(string address, MapView view, int zoom = 15)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            string baseDir = "";
            string url = "";
            string log = "";
            string type = "cache";
            string formatted = System.Web.HttpUtility.UrlEncode(address);
            byte[] data = null;

            switch (view)
            {
                case MapView.Aerial:
                    baseDir = Constants.Directories.GeolocationsAerialMap;
                    url = $"staticmap?center={formatted}&zoom={zoom}&key={_key}&size=500x500&scale=2&maptype=hybrid&markers={formatted}";
                    log = "Aerial Map Retrieval";
                    break;
                case MapView.Street:
                    baseDir = Constants.Directories.GeolocationsStreetMap;
                    //url = $"streetview?size=800x600&key={_key}&location={formatted}&fov=120";
                    url = $"streetview?size=500x500&key={_key}&location={formatted}&fov=120";
                    log = "Street Map Retrieval";
                    break;
                default:
                    break;
            }

            DirectoryInfo dir = new DirectoryInfo(baseDir);

            if (!dir.Exists)
                dir.Create();

            //Logger.LogIt("Generate Address Hashcode", new { address, hash = address.GetHashCode() });
            FileInfo file = new FileInfo($"{baseDir}{address.GetHashCode()}.jpg");

            if (!file.Exists)
            {
                data = CallAPIStream(url);
                type = "google";

                File.WriteAllBytes(file.FullName, data);

                file.Refresh();

                if (!file.Exists)
                    Logger.LogException(new Exception("Unable to save new file: " + file.FullName));
            }
            else
            {
                data = File.ReadAllBytes(file.FullName);
            }

            timer.Stop();
            Logger.LogIt(log, new { address, timer.Elapsed, type, file.Name });

            return data;
        }

        private string CallAPI(string url)
        {
            try
            {
                HttpResponseMessage response = _client.GetAsync(url).Result;

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    System.Diagnostics.Debug.WriteLine(result);
                    return result;
                }
                else
                {
                    //Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                    throw new Exception(response.ReasonPhrase);
                }
            }
            catch(Exception e)
            {
                Logger.LogException(e);
                throw e;
            }
        }

        private byte[] CallAPIStream(string url)
        {
            try
            {
                HttpResponseMessage response = _client.GetAsync(url).Result;

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStreamAsync().Result;
                    //System.Diagnostics.Debug.WriteLine(result);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        result.CopyTo(ms);
                        return ms.ToArray();
                    }
                }
                else
                {
                    //Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                    throw new Exception(response.ReasonPhrase);
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e);
                throw e;
            }
        }


    }
}