﻿using CustomerPortal.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Data
{
    public class SalesforceContext: DbContext
    {
        public SalesforceContext(DbContextOptions<SalesforceContext> options) : base(options)
        {
        }

        public DbSet<vwEmailOrderLink> vwEmailOrderLinks { get; set; }
        public DbSet<vwContactOrder> vwContactOrders { get; set; }
        public DbSet<vwAccountOrder> vwAccountOrders { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<vwEmailOrderLink>().ToTable("vwEmailOrderLink");
            modelBuilder.Entity<vwEmailOrderLink>().HasKey(p => new { p.Order_Number__c, p.Quote_Number__c, p.Email });

            modelBuilder.Entity<vwContactOrder>().ToTable("vwContactOrders");
            modelBuilder.Entity<vwContactOrder>().HasKey(p => new { p.Order_Number__c, p.Quote_Number__c, p.Email });

            modelBuilder.Entity<vwAccountOrder>().ToTable("vwAccountOrders");
            modelBuilder.Entity<vwAccountOrder>().HasKey(p => new { p.Order_Number__c, p.Quote_Number__c, p.Email });
        }
    }
}
