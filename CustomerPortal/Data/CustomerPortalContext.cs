﻿using CustomerPortal.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerPortal.Data
{
    public class CustomerPortalContext : DbContext
    {
        //public CustomerPortalContext() : base(options)
        //{
        //    Options = options;
        //}

        public CustomerPortalContext(DbContextOptions<CustomerPortalContext> options) : base(options)
        {
            Options = options;
        }

        public DbContextOptions<CustomerPortalContext> Options { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<AccountOrders> AccountOrders { get; set; }
        public DbSet<EmpAccess> EmpAccess { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<Articles> Articles { get; set; }
        public DbSet<Attachments> Attachments { get; set; }
        public DbSet<Events> Events { get; set; }
        public DbSet<SurveyComplete> SurveyComplete { get; set; }
        public DbSet<SurveyAnswer> SurveyAnswer { get; set; }
        public DbSet<Surveys> Surveys { get; set; }
        public DbSet<SurveyQuestions> SurveyQuestions { get; set; }
        public DbSet<ResetQueue> ResetQueue { get; set; }
        public DbSet<Photos> Photos { get; set; }
        public DbSet<TaskItem> TaskItems { get; set; }
        public DbSet<HomePhotos> HomePhotos { get; set; }
        public DbSet<HomeContent> HomeContent { get; set; }
        public DbSet<ScheduledOrderEvents> ScheduledOrderEvents { get; set; }
        public DbSet<Geolocations> Geolocations { get; set; }

        //public DbSet<> { get; set; }
        //public DbSet<SurveyQuestion> SurveyQuestions {get; set;}
        //public DbSet<SurveyOption> SurveyOptions { get; set;}
        //public DbSet<SurveyQuestionAnswer> SurveyQuestionAnswers { get; set;}
        //public DbSet<Question> Questions { get; set;}


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().Property(u => u.UserID).UseIdentityColumn();

            modelBuilder.Entity<AccountOrders>().HasKey(u => new { u.UserID, u.OrderNum });

            modelBuilder.Entity<EmpAccess>().Property(u => u.AccessID).UseIdentityColumn();
            modelBuilder.Entity<EmpAccess>().HasKey(u => u.AccessID);

            modelBuilder.Entity<Log>().Property(u => u.LogID).UseIdentityColumn();

            modelBuilder.Entity<Attachments>().HasKey(u => u.AttachmentID);
            //modelBuilder.Entity<Attachments>().Property(u => u.AttachmentID).UseIdentityColumn();

            modelBuilder.Entity<Articles>().Property(u => u.ArticleID).UseIdentityColumn();
            modelBuilder.Entity<Articles>().HasKey(p => p.ArticleID);

            modelBuilder.Entity<Events>().Property(u => u.EventID).UseIdentityColumn();
            modelBuilder.Entity<Events>().HasKey(p => p.EventID);

            modelBuilder.Entity<Surveys>().HasKey(u => u.SurveyID);
            modelBuilder.Entity<Surveys>().Property(u => u.SurveyID).UseIdentityColumn();

            modelBuilder.Entity<SurveyQuestions>().HasKey(u => new { u.SurveyID, u.QuestionID, u.SubQuestionID });

            modelBuilder.Entity<SurveyComplete>().Property(u => u.CompletionID).UseIdentityColumn();
            modelBuilder.Entity<SurveyComplete>().HasKey(p => p.CompletionID);

            modelBuilder.Entity<SurveyAnswer>().HasKey(u => new { u.CompletionID, u.QuestionID, u.SubQuestionID });

            modelBuilder.Entity<ResetQueue>().HasKey(u => u.ResetID);

            modelBuilder.Entity<HomePhotos>().HasKey(u => new { u.ContentID, u.PhotoID });

            modelBuilder.Entity<Photos>().HasKey(u => u.PhotoID);
            modelBuilder.Entity<Photos>().Property(u => u.PhotoID).UseIdentityColumn();

            modelBuilder.Entity<TaskItem>().HasKey(u => u.TaskItemID);
            modelBuilder.Entity<TaskItem>().Property(u => u.TaskItemID).UseIdentityColumn();

            modelBuilder.Entity<Geolocations>().HasKey(u => u.ID);
            modelBuilder.Entity<Geolocations>().Property(u => u.ID).UseIdentityColumn();

            modelBuilder.Entity<HomeContent>().HasKey(u => u.ID);
            modelBuilder.Entity<HomeContent>().Property(u => u.ID).UseIdentityColumn();

            modelBuilder.Entity<ScheduledOrderEvents>().Property(u => u.ScheduledOrderEventID).UseIdentityColumn();
            modelBuilder.Entity<ScheduledOrderEvents>().HasKey(p => p.ScheduledOrderEventID);
        }
    }
}
