﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using System.Net.Sockets;
using System.Text;
using System.Configuration;

namespace CustomerPortal
{
    public static class Logger
    {
#if DEBUG
        private static bool _debug => true;
#else
        private static bool _debug => false;
#endif

        private static int _port = 10000;
        const string LOG_KEY = "e7817df0-bf2f-421b-833d-d5a0c4fe9edc";


        /// <summary>
        /// Recursive method for logging exceptions.  Logs until Exception.InnerException is null.
        /// </summary>
        /// <param name="e">The Exception</param>
        /// <param name="callingMethod">The method name provided by reflection</param>
        public static void LogException(Exception e, [CallerMemberName] string callingMethod = null)
        {
            LogIt("Exception", e.Message, callingMethod);
            if (e.InnerException != null)
            {
                LogException(e.InnerException, callingMethod);
            }
        }

        /// <summary>
        /// Logs data to log entries.
        /// </summary>
        /// <param name="Event">The event name</param>
        /// <param name="LogData">The serializable object to log</param>
        /// <param name="callingMethod">The method name provided by reflection</param>
        public static void LogIt(string Event, object LogData = null, [CallerMemberName] string callingMethod = null)
        {
            try
            {
                using (UdpClient client = new UdpClient("data.logentries.com", _port))
                {
                    var msgBytes = Encoding.UTF8.GetBytes(LOG_KEY + JsonConvert.SerializeObject(new
                    {
                        API = true,
                        Event,
                        InDebug = _debug,
                        Method = callingMethod,
                        //TransactionID,
                        LogData
                    }));
                    client.Send(msgBytes, msgBytes.Length);
                }
            }
            catch
            {
                // Empty catch to prevent interrupting application.
            }
        }
    }
}


