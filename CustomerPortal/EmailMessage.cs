﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Mail;
using Frobot.Email;
using Org.BouncyCastle.Crypto.IO;

namespace CustomerPortal.Email
{

    //public class Recipient
    //{
    //    public string EmailAddress { get; set; }
    //    public string Name { get; set; }

    //    public Recipient() { }
    //    public Recipient(string address, string name = "")
    //    {
    //        EmailAddress = address;
    //        Name = name.Length == 0 ? address : name;
    //    }
    //}

    public class EmailMessage
    {
        public static Recipient MktgEmail => new Recipient("mktg@frazier.com", "Marketing");
        //public static Recipient FrazierEmail => new Recipient("frazier@frazier.com", "Frazier Industrial");
        public static Recipient CSEmail => new Recipient("customerservice@frazier.com", "Frazier Customer Service");
        public static Recipient JAlmonte => new Recipient("jalmonte@frazier.com", "Johnny Almonte");
        public static Recipient bfattoross => new Recipient("bfattoross@frazier.com", "Brian Fattoross");
        public List<Recipient> To { get; set; } = new List<Recipient>();
        public List<Recipient> Cc { get; set; } = new List<Recipient>();
        public List<Recipient> Bcc { get; set; } = new List<Recipient>();
        //public MailAddressCollection Bcc { get { return new MailAddressCollection() { new MailAddress("jalmonte@frazier.com", "Johnny Almonte") }; } }
        public Recipient From { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }



        public EmailMessage(List<Recipient> to, string subject, string body, Recipient from = null, List<Recipient> cc = null, List<Recipient> bcc = null)
        {
            To = to;
            Subject = subject;
            Body = body;

            if (cc != null && cc.Count > 0)
                Cc = cc;

            if (bcc != null && bcc.Count > 0)
                Bcc = bcc;

#if DEBUG
            From = (from != null ? from : new Recipient("test@frazier.com", "Customer Portal Test"));
#else
            From = (from != null ? from : CSEmail);
#endif

        }

        public bool SendEmail()
        {
            bool success = false;

            try
            {
                //SmtpClient client = new SmtpClient("10.1.1.237");
                //client.Credentials = new System.Net.NetworkCredential("exchrelay@frazier.com", "Frazier.2013");

                MailMessage msg = new MailMessage();

                foreach (Recipient contact in To)
                {
                    msg.To.Add(new MailAddress(contact.Address, contact.Name));
                }

                foreach (Recipient contact in Cc)
                {
                    msg.CC.Add(new MailAddress(contact.Address, contact.Name));
                }

                foreach (Recipient contact in Bcc)
                {
                    msg.Bcc.Add(new MailAddress(contact.Address, contact.Name));
                }

#if DEBUG
                msg.To.Clear();
                msg.CC.Clear();
                msg.Bcc.Clear();
                //msg.To.Add(new MailAddress("jalmonte@frazier.com", "Johnny Almonte"));
                msg.To.Add(new MailAddress(bfattoross.Address, bfattoross.Name));
                //msg.To.Add(new MailAddress("frazier@frazier.com", "Frazier Industrial"));
                msg.Subject = "[DEBUG] - ";
#endif

                msg.From = new MailAddress(From.Address, From.Name);
                msg.Bcc.Add(new MailAddress(JAlmonte.Address, JAlmonte.Name));
                msg.Bcc.Add(new MailAddress(bfattoross.Address, bfattoross.Name));
                msg.Subject += $"Frazier Customer Portal - {Subject}";
                msg.Body = Body + $"<br/><br/><i>This is an automated message, please do not reply.</i>";
                msg.IsBodyHtml = true;

                //client.Send(msg);
                Frobot.Email.Queue.AddToQueue(msg, new List<string>());
                success = true;
                return success;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return success;
        }

    }
}
