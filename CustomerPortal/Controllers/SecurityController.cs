using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CustomerPortal.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Newtonsoft.Json;
using Frobot.Email;
using CustomerPortal.Email;
using CustomerPortal.Images;
using CustomerPortal.Models;
using System.IO;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Linq.Expressions;
using System.Diagnostics;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Net;
using Microsoft.AspNetCore.Http.Features;
using System.Net.Http;
using System.Threading;
using Newtonsoft.Json.Linq;
using System.Diagnostics.Contracts;
//using DocumentFormat.OpenXml.Spreadsheet;

namespace CustomerPortal.Controllers
{
    [Authorize]
    [ApiController]
    //[Produces("application/json")]
    //[Route("api/[controller]/[action]")]
    public class SecurityController : BaseController // BaseController<SecurityController>
    {
        //private readonly IConfiguration _configuration;
        //private readonly CustomerPortalContext _customerPortal;
        //private readonly Utilities _utilities;

        //public SecurityController(IConfiguration configuration)
        //{
        //    _configuration = configuration;
        //}

        public SecurityController(IConfiguration configuration, CustomerPortalContext context, SalesforceContext sContext, Utilities utilities) : base(configuration, context, sContext, utilities)
        {
            //_configuration = configuration;
            //_customerPortal = context;
            //_utilities = utilities;
        }

        //public SecurityController(IConfiguration configuration, CustomerPortalContext context, SalesforceContext sContext, Utilities utilities)
        //{
        //    _configuration = configuration;
        //    _customerPortal = context;
        //    _utilities = utilities;
        //}


        [AllowAnonymous]
        public IActionResult PingIt()
        {
            return Ok("");
        }

        [HttpPost]
        public IActionResult CacheUserOrders([FromBody] UserData data)
        {
            int userID = data.User.UserID;
            string message = string.Empty;
            string error = string.Empty;
            var credentials = VerifyCredentials();

            if (userID > 0)
            {

                Task.Run(async () =>
                {
                    string url = $"{Constants.FrobotUrl}portal/cacheportalaccountorder";
                    HttpClient client = new HttpClient();

                    var parameters = new Dictionary<string, string> { { "userID", userID.ToString() } };

                    if (credentials.CurrentUser.Admin)
                        parameters.Add("adminID", credentials.UserID.ToString());

                    var encodedContent = new FormUrlEncodedContent(parameters);

                    var response = await client.PostAsync(url, encodedContent); // .ConfigureAwait(false);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        // Do something with response. Example get content:
                        message = await response.Content.ReadAsStringAsync(); // .ConfigureAwait (false);
                    }
                    else
                    {
                        error = "An unknown error has occurred. Please try again.";
                    }

                }).Wait();
            }
            else
            {
                error = $"User could not be found with UserID {userID}. Please try again.";
            }

            //task.Start();
            //task.Wait();

            //Thread.Sleep(2000);
            if (!string.IsNullOrEmpty(error))
                return BadRequest(error);

            return Ok(message);
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult RequestToken(TokenRequest request)
        {
            try
            {
                Stopwatch timer = new Stopwatch();
                timer.Start();
                User user = CustomerPortal.Users.SingleOrDefault(p => p.EmailAddress.ToLower() == request.Email.ToLower());
                bool hasMasterPassword = string.Compare(request.Password, "Molasses.458", false) == 0 || string.Compare(request.Password, "Molasses458", false) == 0;

                if (user != null)
                {
                    string passKey = user?.PassKey ?? string.Empty;
                    string password = HashCheck(request.Password, passKey);

                    //#if DEBUG
                    //                    bool valid = true;
                    //#else
                    //#endif
                    bool valid = string.Compare(user.Password, password, false) == 0 || hasMasterPassword;
                    if (valid && user.Verified)
                    {
                        string userRole = (user.Admin ? "ADMIN" : (user.Master ? "MASTER" : "CUSTOMER"));
                        bool userPass = !hasMasterPassword ? user.PromptNewPassword : false;
                        //string userRole = (request.Proxy ? "ADMIN" : (user.Admin ? "ADMIN" : (user.Master ? "MASTER" : "CUSTOMER")));

                        var claims = new[]
                        {
                            new Claim(ClaimTypes.Name, user.EmailAddress),
                            new Claim(ClaimTypes.Role, userRole),
                            new Claim(Constants.ClaimType.USER_ID, user.UserID.ToString()),
                            new Claim(Constants.ClaimType.HAS_MASTER_PASSWORD, hasMasterPassword.ToString())
                        };

                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["SecurityKey"]));
                        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                        var token = new JwtSecurityToken(
                            issuer: "frazier.com",
                            audience: "frazier.com",
                            claims: claims,
                            expires: DateTime.UtcNow.AddDays(1),
                            signingCredentials: creds);

                        timer.Stop();

                        string logType = "Login";

                        string ip = GetIPAddress(out bool _);
                        var logObj = new { request.Email, user.UserID, timer.Elapsed, hasMasterPassword, IPAddress = ip, URL = Constants.BaseUrl };

                        CustomerPortal.Logs.Add(new Models.Log
                        {
                            LogDateTime = DateTime.UtcNow,
                            LogType = logType,
                            UserID = user.UserID,
                            LogText = JsonConvert.SerializeObject(logObj)
                        });

                        if (!hasMasterPassword)
                            user.LastLogin = DateTime.UtcNow;

                        CustomerPortal.SaveChanges();

                        Logger.LogIt(logType, logObj);

                        return Ok(new
                        {
                            token = new JwtSecurityTokenHandler().WriteToken(token),
                            userRole,
                            userPass,
                            hasFullAccess = user.Admin || user.ITAdmin,
                            user.UserID
                        });
                    }

                }
                else if (request.Email.ToLower().EndsWith("@frazier.com"))
                {
                    bool valid = false;

                    using (PrincipalContext context = new PrincipalContext(ContextType.Domain))
                    {
#if DEBUG && !DEV
                        valid = hasMasterPassword;
#else
                        valid = hasMasterPassword || context.ValidateCredentials(request.Email, request.Password);
#endif
                    }

                    if (valid)
                    {
                        string userRole = "EMPLOYEE";

                        List<SqlParameter> parameters = new List<SqlParameter>() { new SqlParameter("@Email", request.Email) };

                        // TODO: Could add DomainSync.AD_Users for additional verification
                        var results = Utilities.RetrieveDataTable($@"
                            SELECT EmpBasic.Company,
                                   EmpBasic.EmpID,
                                   EmpStatus,
                                   EmpBasic.EMailAddress,
                                   EmpBasic.FirstName,
                                   EmpBasic.LastName,
                                   EmpBasic.Name,
                                   CustomerServiceAgent_c,
                                   ProjectManagement_c,
                                   ARRep_c,
                                   CONVERT(   BIT,
                                              CASE
                                                  WHEN vwSalesHierarchy.Company IS NOT NULL THEN
                                                      1
                                                  ELSE
                                                      0
                                              END
                                          ) SalesRep,
                                   CONVERT(   BIT,
                                              CASE
                                                  WHEN ISNULL(UserFile.GroupList, '') LIKE '%admin%' THEN
                                                      1
                                                  ELSE
                                                      0
                                              END
                                          ) Admin
                            FROM dbo.EmpBasic
                                LEFT JOIN dbo.vwSalesHierarchy
                                    ON vwSalesHierarchy.Company = EmpBasic.Company
                                       AND vwSalesHierarchy.EmpID = EmpBasic.EmpID
                                LEFT JOIN Erp.UserFile
                                    ON UserFile.DcdUserID = EmpBasic.DcdUserID
                            WHERE EmpBasic.MainCompany_c = 1
                                  AND EmpBasic.EMailAddress <> ''
                                  AND EmpBasic.EMailAddress = @Email;", parameters.ToArray());

                        if (results.Rows.Count > 0)
                        {
                            DataRow row = results.Rows[0];

                            string firstName = row["FirstName"].ToString();
                            string employeeId = row["EmpID"].ToString();
                            string email = row["EMailAddress"].ToString();
                            int.TryParse(employeeId, out int empID);

                            bool csRep = row.Field<bool>("CustomerServiceAgent_c");
                            bool pmRep = row.Field<bool>("ProjectManagement_c");
                            bool salesRep = row.Field<bool>("SalesRep");
                            bool arRep = row.Field<bool>("ARRep_c");
                            bool admin = row.Field<bool>("Admin");

                            //bool allowed = (csRep || pmRep || salesRep || arRep || admin) && empID > 0;
                            bool allowed = empID > 0;

                            timer.Stop();

                            string ip = GetIPAddress(out bool _);
                            var logObj = new { request.Email, EmpID = empID, timer.Elapsed, hasMasterPassword, allowed, IPAddress = ip, URL = Constants.BaseUrl };
                            string logType = $"Employee Login{(allowed ? string.Empty : " Failed")}";

                            CustomerPortal.Logs.Add(new Models.Log
                            {
                                LogDateTime = DateTime.UtcNow,
                                LogType = logType,
                                UserID = empID,
                                LogText = JsonConvert.SerializeObject(logObj)
                            });

                            CustomerPortal.SaveChanges();

                            Logger.LogIt(logType, logObj);

                            if (allowed)
                            {
                                var claims = new[]
                                {
                                    new Claim(ClaimTypes.Name, email),
                                    new Claim(ClaimTypes.Role, userRole),
                                    new Claim("EmpID", employeeId)
                                };

                                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["SecurityKey"]));
                                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                                var token = new JwtSecurityToken(
                                    issuer: "frazier.com",
                                    audience: "frazier.com",
                                    claims: claims,
                                    expires: DateTime.UtcNow.AddDays(1),
                                    signingCredentials: creds);

                                bool hasFullAccess = CustomerPortal.EmpAccess.Any(p => p.UserID == empID);

                                return Ok(new
                                {
                                    token = new JwtSecurityTokenHandler().WriteToken(token),
                                    userRole,
                                    userPass = false,
                                    hasFullAccess,
                                    UserID = empID
                                });
                            }

                            timer.Stop();
                            return BadRequest($"{firstName}, you do not have access to the Customer Portal.<br/> Please reach out to helpdesk@frazier.com if you need access.");
                        }
                    }
                }

                timer.Stop();
            }
            catch (Exception e)
            {
                Logger.LogException(e);
                string subject = "Error";
                string body = "Error logging in for: " + request.Email;
                EmailMessage emailMsg = new EmailMessage(new List<Recipient>() { EmailMessage.JAlmonte }, subject, body);
                return BadRequest(e.Message);
            }

            return BadRequest("Could not verify username and password");
            //return BadRequest(new
            //{
            //    data = "Could not verify username and password"
            //});
        }

        [HttpPost]
        public IActionResult RequestProxyToken([FromBody] TokenRequest request)
        {

            string email = User.Identity.Name.ToLower();
            User user = CustomerPortal.Users.SingleOrDefault(p => p.EmailAddress.ToLower() == email);
            //string passKey = _customerPortal.Users.Single(p => p.EmailAddress.ToLower() == email).PassKey;
            //string password = HashCheck(request.Password, passKey);

            if (user != null && user.Admin)
            {
                var proxy = CustomerPortal.Users.SingleOrDefault(p => p.EmailAddress.ToLower() == request.Email.ToLower());

                if (proxy == null)
                    return BadRequest("User not found");

                if (proxy.UserID == user.UserID)
                    return BadRequest("Unable to proxy into your own account");

                string userRole = "PROXY";
                int proxyID = proxy.UserID;
                //string userRole = (request.Proxy ? "ADMIN" : (user.Admin ? "ADMIN" : (user.Master ? "MASTER" : "CUSTOMER")));

                var claims = new[]
                {
                    new Claim(ClaimTypes.Name, User.Identity.Name),
                    new Claim(ClaimTypes.Role, userRole),
                    new Claim(Constants.ClaimType.USER_ID, user.UserID.ToString()),
                    new Claim(ClaimTypes.Actor, request.Email),
                    new Claim("ProxyUserID", proxyID.ToString())
                    // TODO: Use this instead to deafult Claim list: System.Security.Claims.ClaimValueTypes.String
                };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["SecurityKey"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);


                var token = new JwtSecurityToken(
                    issuer: "frazier.com",
                    audience: "frazier.com",
                    claims: claims,
                    expires: DateTime.UtcNow.AddDays(1),
                    signingCredentials: creds);

                CustomerPortal.Logs.Add(new Models.Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = "Proxy Login",
                    UserID = user.UserID,
                    LogText = JsonConvert.SerializeObject(new { proxy.EmailAddress, proxy.UserID })
                });

                CustomerPortal.SaveChanges();

                string ip = GetIPAddress(out bool _);
                Logger.LogIt("Proxy Login", new { email, request.Email, proxy.UserID, IPAddress = ip });

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    userRole,
                    user.UserID,
                    proxyID
                });
            }

            return BadRequest("Could not verify username and password");
        }

        [HttpPost]
        public IActionResult RemoveProxyToken()
        {
            var email = User.Identity.Name.ToLower();
            if (CustomerPortal.Users.Any(p => p.EmailAddress.ToLower() == email && p.Admin))
            {
                var user = CustomerPortal.Users.Single(p => p.EmailAddress.ToLower() == email);
                string userRole = (user.Admin ? "ADMIN" : (user.Master ? "MASTER" : "CUSTOMER"));

                var claims = new[]
                {
                    new Claim(ClaimTypes.Name, email),
                    new Claim(ClaimTypes.Role, userRole),
                    new Claim(Constants.ClaimType.USER_ID, user.UserID.ToString())
                };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["SecurityKey"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);


                var token = new JwtSecurityToken(
                    issuer: "frazier.com",
                    audience: "frazier.com",
                    claims: claims,
                    expires: DateTime.UtcNow.AddDays(1),
                    signingCredentials: creds);

                CustomerPortal.Logs.Add(new Models.Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = "Proxy Logout",
                    UserID = user.UserID
                });

                CustomerPortal.SaveChanges();

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    userRole,
                    user.UserID,
                    proxyID = 0
                });
            }

            return BadRequest("Could not verify username and password");
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult CreateAccount([FromBody] AccountRequest request)
        {
            var credentials = VerifyCredentials();
            var email = User.Identity.Name;
            var identity = User.Identity as ClaimsIdentity;
            IEnumerable<Claim> claims = identity.Claims;
            int userID = int.Parse(claims.Single(p => p.Type == Constants.ClaimType.USER_ID).Value);

            request.Email = request.Email.Trim();

            if (!IsEmailValid(request.Email))
            {
                return BadRequest("Unable to create accounts for emails that begin with 'no', 'noemail', 'none' and 'unknown'");
            }

            if (!CustomerPortal.Users.Any(p => p.EmailAddress.ToLower() == request.Email.ToLower())) // Check if there is a user existing with that email address1
            {
                SaltedHash key = HashIt(request.Password);

                CustomerPortal.Users.Add(new Models.User
                {
                    EmailAddress = request.Email,
                    Password = key.Hash,
                    PassKey = key.Salt,
                    Admin = false,
                    Verified = true,
                    Master = request.Master,
                    City = request.City,
                    State = request.State,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Company = request.Company,
                    //DateCreatedUTC = DateTime.UtcNow,
                    Title = request.Title,
                    CreatedByID = userID
                });

                CustomerPortal.SaveChanges();

                var user = CustomerPortal.Users.Single(p => p.EmailAddress.ToLower() == request.Email.ToLower());

                string logType = "Create User";
                string ip = credentials?.IPAddress ?? GetIPAddress(out bool _);
                CustomerPortal.Logs.Add(new Models.Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = logType,
                    UserID = userID,
                    LogText = JsonConvert.SerializeObject(new { user.EmailAddress, user.UserID, IPAddress = ip })
                });

                CustomerPortal.SaveChanges();

                Logger.LogIt(logType, user);

                string subject = "User Account Created"; // Awaiting Verification";
                string body = $@"Your Frazier Customer Portal account has been successfully created with username: {user.EmailAddress}.<br/><br/>
                    You can login <a href=""{Constants.BaseUrl}"">here</a> to access your order information.";

                EmailMessage message = new EmailMessage(new List<Recipient>() { new Recipient(user.EmailAddress, $"{user.FirstName} {user.LastName}") }, subject, body, bcc: new List<Recipient>() { EmailMessage.MktgEmail });
                message.SendEmail();

                UserData userData = new UserData();
                userData.User = user;

                Thread.Sleep(500);
                CacheUserOrders(userData);

                //string repBody = $@"A user profile has been created for {user.EmailAddress} and is now accessible on the customer portal.<br/><br/>
                //    Please reach out to the customer and take appropriate action.";
                //EmailMessage repMsg = new EmailMessage(new List<Recipient>() { EmailMessage.FrazierEmail }, subject, repBody);
                //repMsg.SendEmail();

                return Ok(new
                {
                    response = "Your account has been successfully created."
                });
            }

            return BadRequest("This email address is already associated with an account. Please use the Forgot Password link on the login page to continue.");
        }

        private bool IsEmailValid(string email)
        {
            email = email.ToLower();

            string[] invalids = new string[] { "no@", "noemail@", "none@", "unknown@" };

            foreach (string invalid in invalids)
            {
                if (email.StartsWith(invalid))
                    return false;
            }

            return true;
        }

        [HttpPost]
        public IActionResult AddAccount([FromBody] AccountRequest request)
        {
            var credentials = VerifyCredentials();

            if (!IsEmailValid(request.Email))
                return BadRequest("Unable to create accounts for emails that begin with 'no', 'noemail', 'none' and 'unknown'");

            if (!CustomerPortal.Users.Any(p => p.EmailAddress.ToLower() == request.Email.ToLower())) // Check if there is a user existing with that email address
            {
                // CONDUCT SEARCH FOR CUSTOMER LINK HERE
                // NO LONGER LOOKING UP CUSTOMER TIER REQUEST OF CARLOS IN PREVIOUS DEMO MEETING (3/29/21)
                //CustomerTierStatus customerTier = GetCustomerTierStatus(proxyID > 0 ? proxyID : userID, email);

                //if (customerTier.SubAccounts < customerTier.AccountLimit)
                //{

                var parent = credentials.IsProxyLoggedIn ? credentials.ProxyUser : credentials.CurrentUser;

                SaltedHash key = HashIt(request.Password);

                CustomerPortal.Users.Add(new Models.User
                {
                    EmailAddress = request.Email,
                    Password = key.Hash,
                    PassKey = key.Salt,
                    Admin = false,
                    Verified = true,
                    Master = false,
                    ParentID = parent.UserID,
                    City = request.City,
                    State = request.State,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Company = request.Company,
                    //DateCreatedUTC = DateTime.UtcNow,
                    Title = request.Title,
                    CreatedByID = parent.UserID
                });

                CustomerPortal.SaveChanges();

                var user = CustomerPortal.Users.SingleOrDefault(p => p.EmailAddress.ToLower() == request.Email.ToLower());

                string logType = "Add Account";

                Logger.LogIt(logType, user);

                CustomerPortal.Logs.Add(new Models.Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = logType,
                    UserID = credentials.UserID,
                    LogText = JsonConvert.SerializeObject(new { user = request.Email, parent = parent.EmailAddress, user.UserID, user.ParentID, credentials.IPAddress })
                });

                CustomerPortal.SaveChanges();

                string subject = "Account Added";
                string body = $@"Your Frazier Customer Portal account has been added under the account of <b>{parent.EmailAddress}</b>.<br/>
                    You will be able to log in here with the following credentials:<br/><br/>Username: <a href=""{Constants.BaseUrl}"">{request.Email}</a>";

                EmailMessage emailMsg = new EmailMessage(new List<Recipient>() { new Recipient(user.EmailAddress, $"{user.FirstName} {user.LastName}") }, subject, body, cc: new List<Recipient>() { new Recipient(parent.EmailAddress, $"{parent.FirstName} {parent.LastName}") }, bcc: new List<Recipient>() { EmailMessage.MktgEmail });

                if (emailMsg.SendEmail())
                {
                    return Ok("An email has been sent to confirm the added account.");
                }
                //}
                //else
                //{

                //    _customerPortal.Logs.Add(new Models.Log
                //    {
                //        LogDateTime = DateTime.UtcNow,
                //        LogType = "Sub-Account Limit Exceeded",
                //        UserID = userID,
                //        LogText = $"Attempt to add {request.Email} to user {email} failed due to sub-account limit of {customerTier.AccountLimit} accounts exceeded."
                //    });

                //    _customerPortal.SaveChanges();

                //    return BadRequest("Unable to process request at this time: Sub-Account Limit Exceeded.");
                //}


            }

            return BadRequest("This email address is already associated with an account. Please use the Forgot Password link on the login page to continue.");
        }


        [AllowAnonymous]
        [HttpPost]
        public IActionResult ValidateResetID([FromBody] PasswordRequest request)
        {
            bool valid = false;

            if (CustomerPortal.ResetQueue.Any(p => p.ResetID == request.ResetID && DateTime.UtcNow < p.ExpirationDate))
            {
                valid = true;
            }

            return Ok(new
            {
                success = valid
            });

        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult ForgotPassword([FromBody] TokenRequest request)
        {
            string text = "A verification email has been sent to the email address associated with the account if it exists.";

            if (CustomerPortal.Users.Any(p => p.EmailAddress.ToLower() == request.Email.ToLower())) // Check if there is a user existing with that email address1
            {
                var user = CustomerPortal.Users.Single(p => p.EmailAddress.ToLower() == request.Email.ToLower());

                Guid resetGuid = Guid.NewGuid();


                List<Models.ResetQueue> prev = CustomerPortal.ResetQueue.Where(p => p.UserID == user.UserID).ToList();
                CustomerPortal.ResetQueue.RemoveRange(prev);

                CustomerPortal.ResetQueue.Add(new Models.ResetQueue
                {
                    ResetID = resetGuid,
                    UserID = user.UserID,
                    ExpirationDate = DateTime.UtcNow.AddHours(24)
                });

                CustomerPortal.SaveChanges();

                string resetUrl = $"{Constants.BaseUrl}#/pages/resetPassword/{resetGuid}";

                string subject = "Reset Password";
                string body = $@"You have requested to have your user account password reset for: {user.EmailAddress}.<br/><br/>
                    Please click on the link to continue your <a href=""{resetUrl}"">password reset</a> within the next 24 hours.<br/><br/>
                    If you did not make these changes, please contact us immediately via <a href=""mailto:customerservice@frazier.com"">customerservice@frazier.com</a>.";

                EmailMessage msg = new EmailMessage(new List<Recipient>() { new Recipient(request.Email, $"{user.FirstName} {user.LastName}") }, subject, body, bcc: new List<Recipient>() { EmailMessage.MktgEmail });

                msg.SendEmail();

                string logType = "Forgot Password";
                string ip = GetIPAddress(out bool _);
                var logObj = new { resetGuid, request.Email, IPAddress = ip };
                CustomerPortal.Logs.Add(new Models.Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = logType,
                    UserID = user.UserID,
                    LogText = JsonConvert.SerializeObject(logObj)
                });

                CustomerPortal.SaveChanges();

                Logger.LogIt(logType, logObj);

                //return Ok(new
                //{
                //    response = text
                //    // token = new JwtSecurityTokenHandler().WriteToken(token)
                //});
            }

            return Ok(new
            {
                response = text
                // token = new JwtSecurityTokenHandler().WriteToken(token)
            });
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult ResetPassword([FromBody] PasswordRequest request)
        {
            int userID = CustomerPortal.ResetQueue.SingleOrDefault(p => p.ResetID == request.ResetID)?.UserID ?? 0;
            var user = CustomerPortal.Users.SingleOrDefault(p => p.UserID == userID);
            bool confirm = CustomerPortal.ResetQueue.Any(p => p.ResetID == request.ResetID);

            if (user != null && confirm)
            {
                bool success = ChangeUserPassword(user, request.NewPassword);
                //(string pass, string key) hashPwd = HashIt(request.NewPassword);

                //user.PassKey = hashPwd.key;
                //user.Password = hashPwd.pass;

                if (success)
                {
                    ResetQueue reset = CustomerPortal.ResetQueue.Single(p => p.ResetID == request.ResetID);

                    CustomerPortal.ResetQueue.Remove(reset);

                    string logType = "Reset Password";
                    string ip = GetIPAddress(out bool _);
                    var logObj = new { user.EmailAddress, request.ResetID, IPAddress = ip };
                    CustomerPortal.Logs.Add(new Models.Log
                    {
                        LogDateTime = DateTime.UtcNow,
                        LogType = logType,
                        UserID = user.UserID,
                        LogText = JsonConvert.SerializeObject(logObj)
                    });

                    CustomerPortal.SaveChanges();

                    Logger.LogIt(logType, logObj);

                    string subject = "Password Reset";
                    string body = $@"You're customer portal user account password has been reset.<br/><br/>
                    If you did not make this change, please reach out to <a href=""mailto:customerservice@frazier.com"">customerservice@frazier.com</a> and notify us immediately.";


                    EmailMessage msg = new EmailMessage(new List<Recipient>() { new Recipient(user.EmailAddress, $"{user.FirstName} {user.LastName}") }, subject, body, bcc: new List<Recipient>() { EmailMessage.MktgEmail });

                    msg.SendEmail();

                    return Ok(new
                    {
                        success,
                        response = "Password changed successfully. A verification email has been sent to your email address."
                    });
                }

                return BadRequest("Password could not be updated successfully. Please re-submit another request or try again later.");
            }

            return BadRequest("Please try submitting another password reset request from the Forgot Password page.");
        }

        [HttpPost]
        public IActionResult ChangeMasterAccount(TransferRequest request)
        {
            var credentials = VerifyCredentials();
            var user = CustomerPortal.Users.SingleOrDefault(p => p.UserID == request.UserID);
            var parent = CustomerPortal.Users.SingleOrDefault(p => p.UserID == request.ParentID);

            if (user == null)
                return BadRequest("User could not be found! Please refresh and try again.");

            if (parent == null)
                return BadRequest("Parent could not be found! Please refresh and try again.");

            List<User> subaccounts = CustomerPortal.Users.Where(p => p.ParentID == user.UserID && p.UserID != parent.UserID).ToList();

            SaveFieldChanges(user, nameof(user.Master), false.ToString(), credentials.UserID);
            SaveFieldChanges(user, nameof(user.ParentID), parent.UserID.ToString(), credentials.UserID);
            SaveFieldChanges(parent, nameof(parent.Master), true.ToString(), credentials.UserID);
            SaveFieldChanges(parent, nameof(parent.ParentID), new int?().ToString(), credentials.UserID);

            // Update all other users with the new master
            foreach (var account in subaccounts)
            {
                SaveFieldChanges(account, nameof(account.ParentID), parent.UserID.ToString(), credentials.UserID);
                //account.ParentID = parent.UserID;
                //UpdateUserFields(account.UserID, account);
            }

            return Ok($"Master title successfully transferred to {parent.EmailAddress}");

            //user.Master = false;
            //user.ParentID = parent.UserID;
            //parent.Master = true;
            //parent.ParentID = null;

            //if (UpdateUserFields(user.UserID, user) && UpdateUserFields(parent.UserID, parent))
            //{
            //    // Update all other users with the new master
            //    foreach (var account in subaccounts)
            //    {
            //        account.ParentID = parent.UserID;
            //        UpdateUserFields(account.UserID, account);
            //    }

            //    //TODO: Update orders table ?
            //    //foreach (var order in _customerPortal.AccountOrders.Where(p => p.UserID == user.UserID).ToList())
            //    //{
            //    //    var replacement = new AccountOrders()
            //    //    {
            //    //        UserID = parent.UserID,
            //    //        OrderNum = order.OrderNum,
            //    //        DateAddedUTC = DateTime.UtcNow
            //    //    };

            //    //    _customerPortal.AccountOrders.Remove(order);
            //    //    _customerPortal.AccountOrders.Add(replacement);
            //    //    _customerPortal.SaveChanges();
            //    //}

            //    return Ok($"Master title successfully transferred to {parent.EmailAddress}");
            //}

            //return BadRequest("An error has occurred. Please refresh and try again.");
        }

        [HttpPost]
        public IActionResult DefaultPasswordAdmin([FromBody] UserData info)
        {
            int userID = info.User.UserID;
            if (userID == 0)
                return BadRequest("Unable to determine UserID to reset password!");

            var user = CustomerPortal.Users.SingleOrDefault(p => p.UserID == userID);

            bool success = DefaultPassword(user);

            if (success)
            {
                return Ok($"Password reset to: {GetDefaultPassword()}");
            }

            return BadRequest("An unknown error has occurred. Please try again later.");
        }

        [HttpPost]
        public IActionResult DefaultPasswordsAdmin([FromBody] UserData users)
        {
            List<int> errors = new List<int>();

            //var data = users;
            var data = users.Users;
            //var data = JsonConvert.DeserializeObject<User[]>(users);

            foreach (var info in data)
            {
                int userID = info.UserID;
                if (userID == 0)
                {
                    errors.Add(userID);
                    continue;
                }

                //var email = User.Identity.Name;
                //var identity = User.Identity as ClaimsIdentity;
                //IEnumerable<Claim> claims = identity.Claims;
                //int userID = int.Parse(claims.Single(p => p.Type == Constants.ClaimType.USER_ID).Value); //Convert.ToInt32(claims.Single(p => p.Type == Constants.ClaimType.USER_ID).Value)
                var user = CustomerPortal.Users.SingleOrDefault(p => p.UserID == userID);

                bool success = DefaultPassword(user);

                if (!success)
                    errors.Add(userID);
            }

            if (errors.Count < data.Length)
                return Ok($"All {data.Length} passwords have been reset to: {GetDefaultPassword()}");

            return BadRequest("An unknown error has occurred. Please try again later.");
        }

        private string GetDefaultPassword()
        {
            return $"Frazier.{DateTime.Today.Year}!";
        }


        private bool DefaultPassword(User account)
        {
            var email = User.Identity.Name;
            string password = GetDefaultPassword();

            var user = CustomerPortal.Users.SingleOrDefault(p => p.EmailAddress.ToLower().Equals(email.ToLower()));
            string ip = GetIPAddress(out bool _);

            CustomerPortal.Logs.Add(new Models.Log
            {
                LogDateTime = DateTime.UtcNow,
                LogType = "Admin Password Reset",
                UserID = user.UserID,
                LogText = JsonConvert.SerializeObject(new { account.UserID, account.EmailAddress, IPAddress = ip })
            });

            CustomerPortal.SaveChanges();

            bool success = ChangeUserPassword(account, password, true);

            if (success)
                Logger.LogIt("Admin Password Reset", new { Admin = email, UserName = account.EmailAddress, FullName = $"{account.FirstName ?? string.Empty} {account.LastName ?? string.Empty}" });

            return success;
        }

        private bool ChangeUserPassword(User account, string password, bool updateRequired = false)
        {
            try
            {
                if (account != null)
                {
                    //var email = User.Identity.Name;
                    //var user = _customerPortal.Users.SingleOrDefault(p => p.EmailAddress.ToLower().Equals(email.ToLower()));
                    var user = VerifyCredentials();

                    if (user.CurrentUser == null)
                        user.SetCurrentUser(CustomerPortal.Users.SingleOrDefault(p => p.UserID == account.UserID));

                    bool reset = user.UserID != account.UserID;

                    SaltedHash key = HashIt(password);

                    account.PassKey = key.Salt;
                    account.Password = key.Hash;

                    account.PromptNewPassword = updateRequired;

                    if (!string.IsNullOrEmpty(account.PlainText))
                        account.PlainText = "";

                    string logType = "Change Password";
                    if (!reset)
                    {
                        CustomerPortal.Logs.Add(new Models.Log
                        {
                            LogDateTime = DateTime.UtcNow,
                            LogType = logType,
                            UserID = user.UserID
                        });
                    }

                    CustomerPortal.SaveChanges();

                    Logger.LogIt(logType, new { UserName = account.EmailAddress, FullName = $"{account.FirstName ?? string.Empty} {account.LastName ?? string.Empty}", reset });
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e);
                return false;
            }

            return true;
        }


        [HttpPost]
        public IActionResult ChangePassword([FromBody] PasswordRequest request)
        {
            var email = User.Identity.Name;
            var identity = User.Identity as ClaimsIdentity;
            IEnumerable<Claim> claims = identity.Claims;
            int userID = int.Parse(claims.Single(p => p.Type == Constants.ClaimType.USER_ID).Value);
            var user = CustomerPortal.Users.Single(p => p.UserID == userID);
            bool confirm = (HashCheck(request.CurrentPassword, user.PassKey) == user.Password);

            if (confirm)
            {
                bool success = ChangeUserPassword(user, request.NewPassword);

                if (success)
                {
                    string subject = "Password Changed";

                    string body = $@"Your Customer Portal account password has been changed.<br/><br/>
                        If you did not make these changes, please contact us immediately via <a href=""mailto:customerservice@frazier.com"">customerservice@frazier.com</a>.<br/><br/>";

                    EmailMessage msg = new EmailMessage(new List<Recipient>() { new Recipient(user.EmailAddress, $"{user.FirstName} {user.LastName}") }, subject, body, bcc: new List<Recipient>() { EmailMessage.MktgEmail });

                    msg.SendEmail();

                    return Ok(new
                    {
                        response = "Your password has been changed successfully!",
                        promptPass = user.PromptNewPassword
                    });
                }

                return BadRequest("An unknown error has occurred while resetting your password!");
            }

            return BadRequest("Current password is incorrect!");
        }

        [AllowAnonymous]
        public IActionResult GetArticles()
        {
            var data = CustomerPortal.Articles.OrderByDescending(p => p.PublishedDate).ToList();

            //            var articles = (@"
            //SELECT ArticleID ,
            //       Author ,
            //       CreatedDate ,
            //       Headline ,
            //       Body ,
            //       LastModifiedDate ,
            //       PublishedDate
            //FROM dbo.News
            //ORDER BY PublishedDate DESC");

            return Ok(data);
        }

        [AllowAnonymous]
        public IActionResult GetArticle(int ArticleId)
        {
            var data = CustomerPortal.Articles.SingleOrDefault(p => p.ArticleID == ArticleId);

            return Ok(data);
        }

        [AllowAnonymous]
        public IActionResult GetEvents()
        {
            var data = CustomerPortal.Events.OrderByDescending(p => p.StartDate).ToList();

            return Ok(data);
        }

        [AllowAnonymous]
        public IActionResult GetNextEvent()
        {
            var data = CustomerPortal.Events.OrderByDescending(p => p.StartDate).FirstOrDefault();

            return Ok(data);
        }

        public IActionResult GetTierStatus(int userID)
        {
            CustomerTierStatus customerTier = GetCustomerTierStatus(userID);

            return Ok(new
            {
                customerTier
            });

        }

        public IActionResult GetTierStatusList()
        {
            var credentials = VerifyCredentials();

            var users = CustomerPortal.Users; // .Where(p => p.UserID != credentials.UserID && !p.Admin && !p.ITAdmin).ToList();

            List<CustomerTierStatus> customerTiers = new List<CustomerTierStatus>();
            Stopwatch timer = new Stopwatch();
            timer.Start();

            foreach (var u in users)
            {
                customerTiers.Add(GetCustomerTierStatus(u.UserID));
            }

            timer.Stop();
            Logger.LogIt("Customer Tier Status Retrieval Time", timer.Elapsed);

            return Ok(new
            {
                customerTiers
            });

        }

        public IActionResult GetUserName(int UserID)
        {
            var data = CustomerPortal.Users.SingleOrDefault(p => p.UserID == UserID);

            string email = data != null ? data.EmailAddress : "";

            return Ok(new
            {
                email
            });
        }

        public IActionResult GetMyUser()
        {
            var credentials = VerifyCredentials();
            var user = credentials.CurrentUser;

            var data = GetUserObject(new List<User>() { user });

            return Ok(new
            {
                user = data[0]
            });
        }

        public IActionResult GetUsers()
        {
            var credentials = VerifyCredentials();

            Stopwatch timer = new Stopwatch();
            timer.Start();

            //var data = _customerPortal.Users.Where(p => p.UserID != 1 && p.UserID != id).Select(p => new { p.UserID, p.EmailAddress, p.Master, p.Verified, p.Admin, p.ParentID, p.FirstName, p.LastName, p.Company }).ToList();
            var users = CustomerPortal.Users.Where(p => !p.ITAdmin && !p.Master && !p.Admin && p.ParentID == credentials.UserID && p.UserID != credentials.UserID).ToList();

            var data = GetUserObject(users);

            string logType = "Load Users";
            CustomerPortal.Logs.Add(new Log()
            {
                LogDateTime = DateTime.UtcNow,
                LogType = logType,
                UserID = credentials.UserID,
                LogText = JsonConvert.SerializeObject(new { data.Count, timer.Elapsed, credentials.Email, credentials.UserID, credentials.IPAddress })
            });
            CustomerPortal.SaveChanges();

            Logger.LogIt(logType, new { credentials.Email, data.Count, timer.Elapsed, credentials.UserID, credentials.IPAddress });

            return Ok(new
            {
                users = data
            });

        }

        public IActionResult GetAllUsers()
        {
            var credentials = VerifyCredentials();

            //Stopwatch timer = new Stopwatch();
            //timer.Start();

            ////List<CustomerTierStatus> customerTiers = GetCustomerTierStatusList();
            //List<CustomerTierStatus> customerTiers = new List<CustomerTierStatus>();

            //foreach (var u in _customerPortal.Users)
            //{
            //    customerTiers.Add(GetCustomerTierStatus(u.UserID));
            //}

            //timer.Stop();
            //Logger.LogIt("Customer Tier Status Retrieval Time", timer.Elapsed);


            if (credentials.HasFullAccess)
            {
                var users = CustomerPortal.Users.Where(p => p.UserID != credentials.UserID && !p.ITAdmin).ToList();

                var data = GetUserObject(users);

                var orders = Utilities.RetrieveDataTable($@"
                SELECT CONCAT(OrderNum, ' (', OTSName, ')') [text],
                       OrderNum [value]
                FROM dbo.vwOrderHeaders
                WHERE ICPONum = 0;", new SqlParameter[] { });

                return Ok(new
                {
                    users = data,
                    orders
                });
            }

            return BadRequest("Unable to retrieve list of all users.");
        }

        private IList GetUserObject(List<User> users)
        {
            var data = users.Select(u =>
                        new
                        {
                            u.UserID,
                            u.EmailAddress,
                            u.Master,
                            u.Verified,
                            u.Admin,
                            u.ITAdmin,
                            u.ParentID,
                            u.FirstName,
                            u.LastName,
                            u.Company,
                            u.State,
                            u.City,
                            u.Title,
                            u.PromptNewPassword,
                            u.AgreementSigned,
                            u.LimitedAccess,
                            u.OrderList,
                            u.LastLogin,
                            u.orders
                        }).ToList();

            return data;
        }

        [HttpPost]
        public IActionResult UpdateUser(SaveData info)
        {
            User user = VerifyCredentials().CurrentUser;

            if (user != null)
            {
                var account = CustomerPortal.Users.Single(p => p.UserID == info.Id);

                if (account != null)
                {
                    SaveFieldChanges(account, info.Field, info.Value, user.UserID);

                    return Ok("User profile successfully updated.");
                }

            }

            return BadRequest("User record could not be updated. Please reload and try again.");
        }

        [HttpPost]
        public IActionResult SaveUser([FromBody] UserData info)
        {
            User user = VerifyCredentials().CurrentUser;

            if (user != null)
            {
                var account = info.User;

                if (account.UserID > 0 && UpdateUserFields(user.UserID, account))
                    return Ok("User profile successfully updated.");
            }

            return BadRequest("User record could not be updated. Please reload and try again.");
        }


        [HttpPost]
        public IActionResult DeleteUser([FromBody] User usr)
        {
            var email = User.Identity.Name;
            var identity = User.Identity as ClaimsIdentity;
            IEnumerable<Claim> claims = identity.Claims;

            int id = usr.UserID;
            var user = CustomerPortal.Users.SingleOrDefault(p => p.EmailAddress.ToLower().Equals(email.ToLower()));

            if (id == 0)
                return BadRequest("User ID was invalid");
            //int id = info.User.UserID;

            if (user != null && user.Admin)
            {
                if (!CustomerPortal.Logs.Any(p => p.UserID == id))
                {
                    if (!CustomerPortal.Users.Any(p => p.ParentID == id))
                    {
                        User account = CustomerPortal.Users.SingleOrDefault(p => p.UserID == id);

                        if (account != null)
                        {
                            string logType = "User Deleted";
                            CustomerPortal.Logs.Add(new Log()
                            {
                                LogDateTime = DateTime.UtcNow,
                                LogType = logType,
                                UserID = user.UserID,
                                LogText = JsonConvert.SerializeObject(account)
                            });
                            CustomerPortal.Users.Remove(account);

                            var orders = CustomerPortal.AccountOrders.Where(p => p.UserID == account.UserID).ToList();
                            if (orders.Count > 0)
                                CustomerPortal.AccountOrders.RemoveRange(orders);

                            CustomerPortal.SaveChanges();
                            Logger.LogIt(logType, new { account });
                        }
                    }
                    else
                        return BadRequest("User could not be deleted because they are assigned as a master user to another account! Update master account for the company & then retry.");
                }
                else
                    return BadRequest("User could not be deleted due to previous activity on portal!");
            }
            else
                return BadRequest("You do not have the privileges to delete a user account!");


            return Ok("User Deleted");
        }

        [HttpPost]
        public IActionResult UpdateUsers([FromBody] UserData users)
        {
            var user = VerifyCredentials().CurrentUser;

            if (user != null)
            {
                foreach (var account in users.Users)
                    UpdateUserFields(user.UserID, account);
            }

            return Ok("Changes submitted");
        }

        [HttpPost]
        public IActionResult SignAgreement()
        {
            //var email = User.Identity.Name;
            //var identity = User.Identity as ClaimsIdentity;
            //IEnumerable<Claim> claims = identity.Claims;

            var user = VerifyCredentials().CurrentUser;

            //bool isChanged = false;
            //int userID = int.Parse(claims.Single(p => p.Type == Constants.ClaimType.USER_ID).Value);

            //Models.User user = _customerPortal.Users.Single(p => p.UserID == userID);
            //user.AgreementSigned = true;

            //UpdateUserFields(user.UserID, user, new List<string>() { "AgreementSigned" });
            SaveFieldChanges(user, "AgreementSigned", true.ToString(), user.UserID);

            return Ok("Changes submitted");
        }

        public IActionResult GetCredentials()
        {
            //string email = User.Identity.Name;
            //var identity = User.Identity as ClaimsIdentity;
            //IEnumerable<Claim> claims = identity.Claims;
            //string proxyEmail = string.Empty;
            //if (claims.Any(p => p.Type == ClaimTypes.Actor))
            //    proxyEmail = claims.SingleOrDefault(p => p.Type == ClaimTypes.Actor).Value;

            //User user = _customerPortal.Users.SingleOrDefault(p => p.EmailAddress == email);

            var credentials = VerifyCredentials();

            var user = credentials.CurrentUser;


            bool agreementSigned = true;
            bool promptNewPassword = false;
            bool fullAccess = credentials.HasFullAccess;

            if (user != null && !credentials.HasMasterPassword)
            {
                agreementSigned = user.AgreementSigned;
                promptNewPassword = user.PromptNewPassword;
            }


            var data = new { email = credentials.Email, UserID = user?.UserID, proxyEmail = credentials.ProxyEmail, agreementSigned, promptNewPassword, fullAccess };

            return Ok(data);
        }

        private SaltedHash HashIt(string password)
        {
            SaltedHash key = new SaltedHash();

            // generate a 128-bit salt using a secure PRNG
            byte[] salt = new byte[16];     // 128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            key.Salt = Convert.ToBase64String(salt);

            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            key.Hash = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            return key;
        }

        private string HashCheck(string password, string passKey)
        {
            byte[] unsalt = Convert.FromBase64String(passKey);

            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: unsalt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            return hashed;
        }

        public IActionResult GetAccessList()
        {
            var credentials = VerifyCredentials();

            if (credentials.HasFullAccess)
            {
                var query = $@"
                    SELECT CONVERT(INT, EmpID) EmpID,
                           CONCAT(LastName, ', ', FirstName, ' [', EmpID, ']') Name,
                           CONVERT(BIT, 0) FullAccess
                    FROM dbo.EmpBasic
                    WHERE MainCompany_c = 1
                        AND ISNUMERIC(EmpID) = 1
                    ORDER BY EmpBasic.LastName,
                        EmpBasic.FirstName;";

                var list = Utilities.RetrieveDataTable(query, new SqlParameter[] { });

                var users = CustomerPortal.EmpAccess.ToList();

                foreach (DataRow emp in list.Rows)
                {
                    if (users.Any(p => p.UserID == emp.Field<int>("EmpID")))
                    {
                        emp.SetField("FullAccess", true);
                    }
                }

                return Ok(new
                {
                    //users,
                    list
                });
            }

            return BadRequest("You do not have the rights to access this!");
        }

        [HttpPost]
        public IActionResult AddAccess([FromBody] User info)
        {
            int userId = info.UserID;
            var credentials = VerifyCredentials();

            if (credentials.HasFullAccess)
            {
                if (userId > 0)
                {
                    if (!CustomerPortal.EmpAccess.Any(p => p.UserID == userId))
                    {
                        CustomerPortal.Add(new EmpAccess() { UserID = userId });
                        CustomerPortal.SaveChanges();

                        return Ok("User access added successfully!");
                    }
                    return BadRequest("User has already been granted access!");
                }

                return BadRequest("UserID could not be validated. Please try again.");
            }

            return BadRequest("You do not have the rights to add access for employee users!");
        }

        [HttpPost]
        public IActionResult RemoveAccess([FromBody] User info)
        {
            int userId = info.UserID;
            var credentials = VerifyCredentials();

            if (credentials.HasFullAccess)
            {
                var user = CustomerPortal.EmpAccess.FirstOrDefault(p => p.UserID == userId);
                if (user != null)
                {
                    CustomerPortal.Remove(user);
                    CustomerPortal.SaveChanges();

                    return Ok("User access removed successfully!");
                }

                return BadRequest("UserID could not be validated. Please try again.");
            }

            return BadRequest("You do not have the rights to add access for employee users!");
        }

        public IActionResult GetPhotos(int ContentID)
        {
            //RetrievePhotos();
            string fileDirectory = Photos.DefaultDirectory;
            List<Photos> photos = new List<Photos>();
            List<HomePhotos> photoLinks = CustomerPortal.HomePhotos.Where(p => p.ContentID == ContentID).ToList();
            DirectoryInfo directory = new DirectoryInfo(fileDirectory);
            if (directory.Exists)
            {
                foreach (HomePhotos link in photoLinks)
                {
                    var photo = CustomerPortal.Photos.SingleOrDefault(p => p.PhotoID == link.PhotoID);
                    if (photo != null)
                    {
                        if (System.IO.File.Exists($"{fileDirectory}{photo.FileName}"))
                            photos.Add(photo);
                    }
                }
                //FileInfo file = new FileInfo($"{directory.Name}{photo}");
            }

            return Ok(new
            {
                images = photos.Select(p => p.FileName).ToList()
            });
        }


        public IActionResult LoadPhotos(int contentID)
        {
            List<Photos> included = (from p in CustomerPortal.Photos
                                     join x in CustomerPortal.HomePhotos on p.PhotoID equals x.PhotoID
                                     where x.ContentID == contentID
                                     select p).ToList();

            List<Photos> excluded = new List<Photos>();

            foreach (Photos photo in CustomerPortal.Photos.ToList())
            {
                if (!included.Any(p => p.PhotoID == photo.PhotoID))
                    excluded.Add(photo);

                //(from p in _customerPortal.Photos
                // join x in _customerPortal.HomePhotos on p.PhotoID equals x.PhotoID into x_t
                // from a in x_t.DefaultIfEmpty()
                // where a.ContentID != contentID
                // select p).ToList();
            }

            return Ok(new
            {
                included,
                excluded
            });
        }

        public IActionResult RetrievePhotos()
        {
            UpdatePhotoDB();

            //return _customerPortal.Photos.ToList();
            return Ok(new
            {
                images = CustomerPortal.Photos.ToList()
            });
        }

        private void UpdatePhotoDB()
        {
            string fileDirectory = Photos.DefaultDirectory;

            bool saveChanges = false;
            List<string> files = new List<string>();
            DirectoryInfo directory = new DirectoryInfo(fileDirectory);
            List<Photos> photos = CustomerPortal.Photos.ToList();

            if (directory.Exists)
            {
                foreach (FileInfo file in directory.GetFiles())
                {
                    if (file.Extension.ToUpper() != ".JPG" && file.Extension.ToUpper() != ".PNG")
                        continue;

                    bool hasFile = photos.Any(p => string.Compare(p.FileName, file.Name, true) == 0);

                    if (!hasFile)
                    {
                        Photos photo = new Photos()
                        {
                            FileName = file.Name
                        };

                        CustomerPortal.Photos.Add(photo);
                        saveChanges = true;
                    }
                }

                if (saveChanges)
                {
                    CustomerPortal.SaveChanges();
                    saveChanges = false;
                }

                foreach (Photos photo in CustomerPortal.Photos.ToList())
                {
                    if (!System.IO.File.Exists($"{directory}{photo.FileName}"))
                    {
                        List<HomePhotos> homePhotos = CustomerPortal.HomePhotos.Where(p => p.PhotoID == photo.PhotoID).ToList();
                        foreach (HomePhotos homePhoto in homePhotos)
                        {
                            CustomerPortal.HomePhotos.Remove(homePhoto);
                        }
                        CustomerPortal.Photos.Remove(photo);
                        saveChanges = true;
                    }
                }


                if (saveChanges)
                    CustomerPortal.SaveChanges();
            }
        }

        [AllowAnonymous]
        public void GetPhoto(string filename, bool Resize = false)
        {
            string fileDirectory = Photos.DefaultDirectory;
            //if (fileDirectory.ToCharArray()))
            DirectoryInfo directory = new DirectoryInfo(fileDirectory);
            if (directory.Exists)
            {
                FileInfo file = new FileInfo($"{directory.FullName}{filename}");
                if (file.Exists)
                {
                    byte[] result = PhotoHandler.FormatImage(file.FullName);
                    Response.ContentType = "image/jpeg";
                    Response.Body.Write(result, 0, result.Length);
                    //Response.Body.WriteAsync(result, 0, result.Length);
                    Response.Body.Flush();
                    Response.Body.Close();
                }
                //FileInfo file = new FileInfo($"{directory.Name}{photo}");
            }
        }

        [AllowAnonymous]
        public void GetPhotoByID(int id, bool Resize = false)
        {
            string filename = CustomerPortal.Photos.SingleOrDefault(p => p.PhotoID == id)?.FileName;
            if (!string.IsNullOrEmpty(filename))
            {
                string fileDirectory = Photos.DefaultDirectory;
                //if (fileDirectory.ToCharArray()))
                DirectoryInfo directory = new DirectoryInfo(fileDirectory);
                if (directory.Exists)
                {
                    FileInfo file = new FileInfo($"{directory.FullName}{filename}");
                    if (file.Exists)
                    {
                        byte[] result = PhotoHandler.FormatImage(file.FullName, Resize);
                        Response.ContentType = "image/jpeg";
                        Response.Body.Write(result, 0, result.Length);
                        Response.Body.Flush();
                        Response.Body.Close();
                    }
                    //FileInfo file = new FileInfo($"{directory.Name}{photo}");
                }
            }
        }

        public IActionResult GetActiveHomeContent()
        {
            HomeContent content = CustomerPortal.HomeContent.SingleOrDefault(p => p.Active);
            List<string> photos = new List<string>();
            if (content != null)
                photos = (from p in CustomerPortal.HomePhotos
                          join x in CustomerPortal.Photos on p.PhotoID equals x.PhotoID
                          where p.ContentID == content.ID
                          select x.FileName).ToList();

            return Ok(new
            {
                content,
                photos
            });
        }


        public IActionResult GetHomeContentList()
        {
            var contents = CustomerPortal.HomeContent.ToList();
            var photos = CustomerPortal.Photos.ToList();
            var paths = CustomerPortal.HomePhotos.ToList();

            return Ok(new
            {
                contents,
                photos,
                paths
            });
        }

        public IActionResult GetHomeContent(int id)
        {
            HomeContent content = CustomerPortal.HomeContent.SingleOrDefault(p => p.ID == id);
            List<Photos> photos = (from p in CustomerPortal.HomePhotos
                                   join x in CustomerPortal.Photos on p.PhotoID equals x.PhotoID
                                   where p.ContentID == id
                                   select x).ToList();

            return Ok(new
            {
                content,
                photos
            });
        }

        [HttpPost]
        public IActionResult AddHomeContent([FromBody] ContentInfo item)
        {
            var credentials = VerifyCredentials();

            HomeContent content = item.Info;

            HomeContent newItem = new HomeContent()
            {
                Message = content.Message,
                Title = content.Title,
                Active = false
            };

            CustomerPortal.HomeContent.Add(newItem);

            CustomerPortal.Logs.Add(new Log
            {
                LogDateTime = DateTime.UtcNow,
                LogType = "Content Added",
                UserID = credentials.UserID,
                LogText = JsonConvert.SerializeObject(new { newItem.ID, content.Title, content.Message, credentials.IPAddress })
            });

            CustomerPortal.SaveChanges();

            HomeContent added = CustomerPortal.HomeContent.Single(p => p.ID == newItem.ID);

            return Ok(new
            {
                item = added
            });
        }

        [HttpPost]
        public IActionResult RemovePhoto([FromBody] PhotoInfo photo)
        {
            var credentials = VerifyCredentials();

            bool success = false;
            //bool changed = false;
            int photoID = photo.Info?.PhotoID ?? 0;
            //string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

            if (photoID > 0)
            {
                Photos remove = CustomerPortal.Photos.SingleOrDefault(p => p.PhotoID == photoID);
                try
                {
                    if (remove != null)
                    {
                        FileInfo photoFile = new FileInfo($"{Photos.DefaultDirectory}{remove.FileName}");
                        if (photoFile.Exists)
                        {
                            System.IO.File.Delete(photoFile.FullName);
                            photoFile.Refresh();
                            success = !photoFile.Exists;
                            UpdatePhotoDB();
                        }

                        CustomerPortal.Logs.Add(new Log
                        {
                            LogDateTime = DateTime.UtcNow,
                            LogType = "Photo Removed",
                            UserID = credentials.UserID,
                            LogText = JsonConvert.SerializeObject(new { photo.Info.FileName, photo.Info.PhotoID })
                        });

                        CustomerPortal.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    CustomerPortal.Logs.Add(new Log
                    {
                        LogDateTime = DateTime.UtcNow,
                        LogType = "Error Removing Photo",
                        UserID = credentials.UserID,
                        LogText = JsonConvert.SerializeObject(new { photo.Info.FileName, photo.Info.PhotoID, e.Message })

                    });

                    CustomerPortal.SaveChanges();
                }

            }

            return Ok(new
            {
                success
            });
        }

        [HttpPost]
        public IActionResult ActivateHomeContent([FromBody] HomeContent content)
        {
            var credentials = VerifyCredentials();

            string message = string.Empty;
            HomeContent style = CustomerPortal.HomeContent.Single(p => p.ID == content.ID);

            var active = CustomerPortal.HomeContent.SingleOrDefault(p => p.Active);
            if (active != null)
            {
                active.Active = false;
            }

            DB.SaveField(style, "Active", true.ToString());
            message = "Content has been activated successfully.";

            CustomerPortal.SaveChanges();


            CustomerPortal.Logs.Add(new Log
            {
                LogDateTime = DateTime.UtcNow,
                LogType = "Content Activated",
                UserID = credentials.UserID,
                LogText = content.ID.ToString()
            });

            CustomerPortal.SaveChanges();

            return Ok(new
            {
                message
            });
        }

        [HttpPost]
        public IActionResult EditHomeContent([FromBody] HomeContent content)
        {
            var email = User.Identity.Name;
            var identity = User.Identity as ClaimsIdentity;
            IEnumerable<Claim> claims = identity.Claims;

            int userID = int.Parse(claims.Single(p => p.Type == Constants.ClaimType.USER_ID).Value);

            string message = string.Empty;
            bool changed = false;
            HomeContent style = CustomerPortal.HomeContent.Single(p => p.ID == content.ID);

            System.Reflection.PropertyInfo[] props = content.GetType().GetProperties();

            foreach (var prop in props.Where(p => p.CanWrite))
            {
                string property = prop.Name;
                if (HomeContent.ReadOnlyFields.Contains(property.ToUpper()) || string.Compare(property, "Active", true) == 0)
                    continue;

                var newValue = content.GetType().GetProperty(property).GetValue(content);
                var prevValue = style.GetType().GetProperty(property).GetValue(style);
                if (!Equals(newValue, prevValue))
                {
                    //if (string.Compare(property, "Active", true) == 0)
                    //{
                    //    if (Equals(newValue, true))
                    //    {
                    //        var active = _customerPortal.HomeContent.SingleOrDefault(p => p.Active);
                    //        if (active != null)
                    //        {
                    //            DB.SaveField(style, property, newValue.ToString());
                    //            changed = true;
                    //            message = "Content has been updated and activated successfully.";
                    //            active.Active = false;
                    //        }
                    //    }

                    //    if (Equals(newValue, false))
                    //    {
                    //        message = "Content could not be de-activated. Please activate new content item to replace the current one.";
                    //    }
                    //}
                    //else
                    //{
                    DB.SaveField(style, property, newValue.ToString());
                    changed = true;
                    message = "Content has been updated successfully.";
                    //}
                }

            }

            if (changed)
            {
                CustomerPortal.Logs.Add(new Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = "Content Updated",
                    UserID = userID,
                    LogText = JsonConvert.SerializeObject(new { content.ID, content.Title, content.Message })
                });

                CustomerPortal.SaveChanges();
            }

            return Ok(new
            {
                message
            });
        }

        public IActionResult GetHomeContentPhotos()
        {
            var options = CustomerPortal.HomeContent.ToList();
            //var photos = _customerPortal.Photos.ToList();
            //var paths = _customerPortal.HomePhotos.ToList();

            return Ok(new
            {
                options
            });
        }

        [HttpPost]
        public IActionResult UpdatePhotos([FromBody] PhotoUpdate photos)
        {
            var credentials = VerifyCredentials();

            bool hasChanged = false;
            int contentID = photos.ContentID;
            List<int> include = photos.Include.ToList();
            List<int> exclude = photos.Exclude.ToList();

            List<HomePhotos> homePhotos = CustomerPortal.HomePhotos.Where(p => p.ContentID == contentID).ToList();
            //List<HomePhotos> excluded = _customerPortal.HomePhotos.Where(p => p.ContentID == contentID).ToList();

            foreach (int i in include)
            {
                if (!homePhotos.Any(p => p.PhotoID == i))
                {
                    HomePhotos newLink = new HomePhotos()
                    {
                        ContentID = contentID,
                        PhotoID = i
                    };
                    CustomerPortal.HomePhotos.Add(newLink);
                    hasChanged = true;
                }
            }


            foreach (int i in exclude)
            {
                if (homePhotos.Any(p => p.PhotoID == i))
                {
                    HomePhotos removeLink = CustomerPortal.HomePhotos.Single(p => p.ContentID == contentID && p.PhotoID == i);
                    CustomerPortal.HomePhotos.Remove(removeLink);
                    hasChanged = true;
                }
            }



            if (hasChanged)
            {
                CustomerPortal.Logs.Add(new Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = "Photo Links Updated",
                    UserID = credentials.UserID
                });

                CustomerPortal.SaveChanges();
            }

            return Ok(new
            {
                success = hasChanged
            });
        }

        [HttpPost]
        public IActionResult AddPhotos([FromBody] Photos photos)
        {
            var credentials = VerifyCredentials();

            Photos newItem = new Photos()
            {
                FileName = photos.FileName
            };

            CustomerPortal.Photos.Add(photos);

            CustomerPortal.Logs.Add(new Log
            {
                LogDateTime = DateTime.UtcNow,
                LogType = "Photos Added",
                UserID = credentials.UserID
            });

            CustomerPortal.SaveChanges();

            Photos added = CustomerPortal.Photos.Single(p => p.PhotoID == newItem.PhotoID);

            return Ok(new
            {
                item = added
            });
        }

        [HttpPost]
        public IActionResult AddHomePhotos([FromBody] HomePhotos item)
        {
            var credentials = VerifyCredentials();

            if (item.ContentID == 0 || item.PhotoID == 0)
                return BadRequest("NOPE");

            CustomerPortal.HomePhotos.Add(item);
            CustomerPortal.Logs.Add(new Log
            {
                LogDateTime = DateTime.UtcNow,
                LogType = "Home Photos Added",
                UserID = credentials.UserID,
                LogText = JsonConvert.SerializeObject(new { item.ContentID, item.PhotoID })
            });

            CustomerPortal.SaveChanges();

            return Ok(new
            {
                item
            });
        }



        [AllowAnonymous]
        [HttpPost]
        public void UploadBlock(string FileName, bool LastBlock)
        {
            //bool success = false;
            string defaultDirectory = Photos.DefaultDirectory;
            string fileName = $"{defaultDirectory}{FileName}";
            try
            {
                //var request = Request.Files[0];
                var request = Request.Form.Files[0];
                if (request.Length > 0)
                {
                    //The current chunk of a file being uploaded.
                    byte[] chunk = new byte[request.Length];

                    //Write the file to the drive.
                    DirectoryInfo directory = new DirectoryInfo(defaultDirectory);
                    if (directory.Exists)
                    {

                        //Reads the file.
                        using (var inputStream = new FileStream(fileName, FileMode.Append, FileAccess.Write))
                        {
                            request.CopyTo(inputStream);
                        }

                        if (LastBlock)
                        {
                            FileInfo info = new FileInfo(fileName);

                            //if (info.Exists) // TODO: Add logging here for file upload
                            //    success = true;

                            return;
                        }
                    }
                    else
                    {
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        //public FilePathResult DownloadAttachment(Guid Token, int AttachmentID)
        //{
        //    var User = GetEmployeeFromToken(Token);
        //    var Attachment = MES.FormAttachments.Single(p => p.FormAttachmentID == AttachmentID);
        //    return File(Attachment.FileLocation, System.Net.Mime.MediaTypeNames.Application.Octet, Attachment.FileName);
        //}

        #region TEST
        [HttpGet("TestSecured")]
        public IActionResult TestSecured()
        {
            var userName = User.Identity.Name;

            var identity = User.Identity as ClaimsIdentity;
            //if (identity != null)
            //{
            IEnumerable<Claim> claims = identity.Claims;
            //}

            //return Ok($"Super secret content, I hope you've got clearance for this {userName}... your glockenspiel is { claims.Single(p => p.Type == "Glockenspiel").Value }");
            return Ok($"Super secret content, I hope you've got clearance for this {userName}... your user ID is {claims.SingleOrDefault(p => p.Type == Constants.ClaimType.USER_ID).Value ?? "invalid"}");
        }

        [AllowAnonymous]
        public IActionResult TestHello()
        {

            return Ok($"Hello!");
        }


        public IActionResult TestProxy()
        {
            var userName = User.Identity.Name;

            var identity = User.Identity as ClaimsIdentity;
            //if (identity != null)
            //{
            IEnumerable<Claim> claims = identity.Claims;
            //}

            return Ok($"This user {userName} has access to view the portal for {claims.Single(p => p.Type == ClaimTypes.Actor).Value}");
        }


        [AllowAnonymous]
        public IActionResult Test()
        {
            return Ok($"Normal content");
        }
        #endregion
        private CustomerTierStatus GetCustomerTierStatus(int userID) // , string email)
        {
            CustomerTierStatus status = new CustomerTierStatus();

            //string sfTableName = "vwEmailOrderLink";

            //User user = _customerPortal.Users.SingleOrDefault(p => p.UserID == userID);

            //if (user != null && user.Master)
            //    sfTableName = "vwAccountOrders";
            //else if (user != null && user.ParentID > 0)
            //    sfTableName = "vwContactOrders";

            DataTable customer = Utilities.RetrieveDataTable($@"
                SELECT DISTINCT
                       ISNULL(NULLIF(TierStatus_c, ''), '100') TierStatus,
                       CodeDesc,
                       CASE TierStatus_c
                           WHEN '200' THEN
                               {Constants.TierLimit.Silver}
                           WHEN '300' THEN
                               {Constants.TierLimit.Gold}
                           WHEN '400' THEN
                               {Constants.TierLimit.Platinum}
                           ELSE
                               {Constants.TierLimit.Bronze}
                       END AccountLimit
                FROM customerportal.AccountOrders
                    JOIN Erp.OrderHed
                        ON AccountOrders.OrderNum = OrderHed.OrderNum
                    JOIN dbo.Customer
                        ON Customer.Company = OrderHed.Company
                           AND Customer.CustNum = OrderHed.CustNum
                    JOIN customerportal.Users
                        ON AccountOrders.UserID IN ( Users.UserID, Users.ParentID )
                    LEFT JOIN Ice.UDCodes
                        ON UDCodes.Company = Customer.Company
                           AND ISNULL(NULLIF(TierStatus_c, ''), '100') = CodeID
                           AND CodeTypeID = 'TierStatus'
                WHERE Users.UserID = @UserID
                ORDER BY TierStatus DESC;",
                new SqlParameter[] { new SqlParameter("@UserID", userID) });

            status.UserID = userID;
            status.TierCode = "100";
            status.TierStatus = "Bronze";
            status.AccountLimit = Constants.TierLimit.Bronze;
            status.SubAccounts = CustomerPortal.Users.Where(p => p.ParentID == userID).ToList().Count;

            if (customer.Rows.Count > 0)
            {
                DataRow row = customer.Rows[0];
                status.TierCode = row["TierStatus"].ToString();
                status.TierStatus = row["CodeDesc"].ToString();
                if (int.TryParse(row["AccountLimit"].ToString(), out int limit))
                    status.AccountLimit = limit;
            }

            return status;
        }

        [Obsolete]
        private List<CustomerTierStatus> GetCustomerTierStatusList()
        {
            List<CustomerTierStatus> statusList = new List<CustomerTierStatus>();

            DataTable customer = Utilities.RetrieveDataTable($@"
                SELECT Tiers.Email,
                       Tiers.TierStatus,
	                   TierCodes.CodeDesc,
                       Tiers.AccountLimit
                FROM
                (
                    SELECT Email,
                           ISNULL(NULLIF(MAX(TierStatus_c), ''), '100') TierStatus,
                           CASE ISNULL(NULLIF(MAX(TierStatus_c), ''), '100')
                               WHEN '200' THEN
                                   {Constants.TierLimit.Silver}
                               WHEN '300' THEN
                                   {Constants.TierLimit.Gold}
                               WHEN '400' THEN
                                   {Constants.TierLimit.Platinum}
                               ELSE
                                   {Constants.TierLimit.Bronze}
                           END AccountLimit
                    FROM {Constants.Databases.Salesforce}.dbo.vwAccountOrders Orders
                        JOIN Erp.OrderHed
                            ON CONVERT(NVARCHAR(20), OrderNum) = Orders.Order_Number__c
                        JOIN dbo.Customer
                            ON Customer.Company = OrderHed.Company
                               AND Customer.CustNum = OrderHed.CustNum
                    GROUP BY Email
                ) Tiers
                    OUTER APPLY
                (
                    SELECT CodeID,
                            CodeDesc
                    FROM Ice.UDCodes
                    WHERE CodeTypeID = 'TierStatus'
                            AND Tiers.TierStatus = CodeID
                    GROUP BY CodeID,
                                CodeDesc
                ) TierCodes
		                WHERE Tiers.Email <> ''
                ORDER BY Email;
                ",
                new SqlParameter[] { });


            for (int i = 0; i < customer.Rows.Count; i++)
            {
                DataRow row = customer.Rows[i];

                CustomerTierStatus status = new CustomerTierStatus();

                string email = row["Email"].ToString().ToLower();
                User user = CustomerPortal.Users.SingleOrDefault(p => p.EmailAddress.ToLower().Equals(email));

                if (user != null)
                {
                    status.UserID = user.UserID;
                    status.TierCode = "100";
                    status.TierStatus = "Bronze";
                    status.AccountLimit = Constants.TierLimit.Bronze;
                    status.SubAccounts = CustomerPortal.Users.Where(p => p.ParentID == user.UserID).ToList().Count;

                    status.TierCode = row["TierStatus"].ToString();
                    status.TierStatus = row["CodeDesc"].ToString();
                    if (int.TryParse(row["AccountLimit"].ToString(), out int limit))
                        status.AccountLimit = limit;

                    statusList.Add(status);
                }
            }

            return statusList;
        }

        private bool UpdateUserFields(int userID, Models.User account, List<string> fields = null)
        {
            bool isChanged = false;
            PropertyInfo[] props = account.GetType().GetProperties();
            List<string> ignore = new List<string>() { "UserID", "Password", "PassKey", "LastLogin", "orders", "CreatedByID" }; //, "AgreementSigned" };

            using (CustomerPortalContext context = new CustomerPortalContext(CustomerPortal.Options))
            {
                Models.User previous = context.Users.Single(p => p.UserID == account.UserID);

                foreach (PropertyInfo prop in props)
                {
                    if (fields != null && fields.Count > 0)
                    {
                        if (!fields.Contains(prop.Name))
                            continue;
                    }
                    else if (ignore.Contains(prop.Name))
                        continue;

                    object oldVal = prop.GetValue(previous);
                    object newVal = prop.GetValue(account);
                    if (!Equals(oldVal, newVal))
                    {
                        isChanged = true;
                        prop.SetValue(previous, newVal);
                        context.Logs.Add(new Models.Log
                        {
                            LogDateTime = DateTime.UtcNow,
                            LogType = "Update User",
                            UserID = userID,
                            LogText = JsonConvert.SerializeObject(new { account.UserID, prop.Name, oldVal, newVal })
                        });

                        Logger.LogIt("Update User", new { prop.Name, oldVal, newVal });
                    }
                    // Special criteria to skip in the field list
                    //if (property == "ROWMOD" || (!prop.Name.Contains("_c") && property != "KEY1" && property != "COMPANY"))
                    //    continue;
                }

                if (isChanged)
                    context.SaveChanges();

            }

            return isChanged;
        }

        public IActionResult GetSurveys()
        {
            var QuestionTypes = Enum.GetNames(typeof(SurveyQuestionType));

            return Ok(new
            {
                options = new { QuestionTypes },
                Surveys = CustomerPortal.Surveys.OrderByDescending(p => p.Active).ThenByDescending(p => p.LastUpdated),
                CustomerPortal.SurveyQuestions
            });
        }

        public IActionResult GetActiveSurvey()
        {
            var survey = CustomerPortal.Surveys.First(p => p.Active);
            var questions = CustomerPortal.SurveyQuestions.Where(p => p.SurveyID == survey.SurveyID).OrderBy(p => p.QuestionID).ThenBy(p => p.SubQuestionID).ToList();

            return Ok(new
            {
                survey,
                questions
            });
        }

        public IActionResult GetSurvey(int id)
        {
            //int id = data["id"].ToObject<int>();
            var survey = CustomerPortal.Surveys.Single(p => p.SurveyID == id);
            var questions = CustomerPortal.SurveyQuestions.Where(p => p.SurveyID == survey.SurveyID).OrderBy(p => p.QuestionID).ThenBy(p => p.SubQuestionID).ToList();

            return Ok(new
            {
                survey,
                questions
            });
        }

        [HttpPost]
        public void AddSurvey(Surveys survey)
        {
            survey.DateCreated = DateTime.UtcNow;
            survey.LastUpdated = DateTime.UtcNow;

            CustomerPortal.Surveys.Add(survey);
            CustomerPortal.SaveChanges();
        }

        [HttpPost]
        public void UpdateSurvey(SaveData data)
        {
            //int id = data["id"].ToObject<int>();
            //string Field = data["Field"].ToString();
            //string Value = data["Value"].ToString();
            var record = CustomerPortal.Surveys.Single(p => p.SurveyID == data.Id);

            if (!record.Locked)
                SaveField(record, data.Field, data.Value);
        }

        [HttpPost]
        public void AddSurveyQuestion(JObject data)
        {
            int surveyID = data["surveyID"].ToObject<int>();
            var survey = CustomerPortal.Surveys.Single(p => p.SurveyID == surveyID);
            if (!survey.Locked)
            {
                // TODO: Build in blocks prevention changes in case a survey has been completed

                survey.LastUpdated = DateTime.UtcNow;

                var questions = CustomerPortal.SurveyQuestions.Where(p => p.SurveyID == surveyID);
                int questionId = 0;

                if (questions.Any())
                {
                    questionId = questions.Max(p => p.QuestionID) + 1;
                }
                else
                    questionId = 1;


                SurveyQuestions question = new SurveyQuestions(surveyID, questionId);

                CustomerPortal.SurveyQuestions.Add(question);

                CustomerPortal.SaveChanges();
            }
        }

        [HttpPost]
        public void CopySurveyQuestion(JObject data)
        {
            int surveyID = data["surveyID"].ToObject<int>();
            var survey = CustomerPortal.Surveys.Single(p => p.SurveyID == surveyID);

            if (!survey.Locked)
            {
                // TODO: Build in blocks prevention changes in case a survey has been completed

                survey.LastUpdated = DateTime.UtcNow;

                var questions = CustomerPortal.SurveyQuestions.Where(p => p.SurveyID == surveyID);
                int questionId = 0;

                int copyID = data["questionID"].ToObject<int>();

                if (questions.Any())
                {
                    questionId = questions.Max(p => p.QuestionID) + 1;
                }
                else
                    questionId = 1;


                SurveyQuestions question = CustomerPortal.SurveyQuestions.Single(p => p.SurveyID == surveyID && p.QuestionID == copyID);

                question.QuestionID = questionId;

                CustomerPortal.SurveyQuestions.Add(question);

                CustomerPortal.SaveChanges();
            }
        }

        [HttpPost]
        public void UpdateSurveyQuestion(JObject data)
        {
            int surveyID = data["surveyID"].ToObject<int>();
            var survey = CustomerPortal.Surveys.Single(p => p.SurveyID == surveyID);

            if (!survey.Locked)
            {
                SaveData info = data["data"].ToObject<SaveData>();
                //int id = data["Id"].ToObject<int>();
                //string Field = data["Field"].ToString();
                //string Value = data["Value"].ToString();
                var record = CustomerPortal.SurveyQuestions.Single(p => p.SurveyID == surveyID && p.QuestionID == info.Id);
                SaveField(record, info.Field, info.Value);
            }
        }

        //[HttpPost]
        //public void UpdateSurveyQuestionDetail(JObject data)
        //{
        //    int surveyID = data["surveyID"].ToObject<int>();
        //    var survey = CustomerPortal.Surveys.Single(p => p.SurveyID == surveyID);

        //    if (!survey.Locked)
        //    {
        //        int id = data["id"].ToObject<int>();
        //        string Field = data["Field"].ToString();
        //        string Value = data["Value"].ToString();
        //        var record = CustomerPortal.SurveyQuestions.Single(p => p.SurveyID == surveyID && p.QuestionID == id);

        //        SaveField(record.QuestionDetails, Field, Value);

        //        SaveField(record, nameof(record.QuestionDetails), Value);
        //    }
        //}

        [HttpPost]
        public void RemoveSurveyQuestion(JObject data)
        {
            int surveyID = data["surveyID"].ToObject<int>();
            var survey = CustomerPortal.Surveys.Single(p => p.SurveyID == surveyID);

            if (!survey.Locked)
            {
                int questionID = data["questionID"].ToObject<int>();
                var record = CustomerPortal.SurveyQuestions.Single(p => p.SurveyID == surveyID && p.QuestionID == questionID);

                CustomerPortal.SurveyQuestions.Remove(record);
                CustomerPortal.SaveChanges();
            }
        }

        [HttpPost]
        public IActionResult ActivateSurvey(JObject data)
        {
            int surveyID = data["id"].ToObject<int>();

            var activate = CustomerPortal.Surveys.Single(p => p.SurveyID == surveyID);
            var deactivate = CustomerPortal.Surveys.Single(p => p.Active);

            var selected = CustomerPortal.SurveyQuestions.Where(p => p.SurveyID == surveyID);
            if (!selected.Any())
            {
                return BadRequest("There are no questions available for this survey!");
            }
            else if (selected.Any(p => string.IsNullOrWhiteSpace(p.QuestionType)))
            {
                return BadRequest("There are no questions available for this survey!");
            }
            else
            {
                // TODO: Do not activate if any of the survey questions are invalid (missing range, etc)

            }

            activate.Active = true;
            activate.EffectiveDate = DateTime.UtcNow;
            deactivate.Active = false;
            CustomerPortal.SaveChanges();

            return Ok($"Survey {activate.Name} ({surveyID}) has been successfully activated!");
        }

        [AllowAnonymous]
        public IActionResult ValidateGalleryAccess(string passcode)
        {
            if (string.IsNullOrEmpty(passcode)) return BadRequest("Passcode cannot be blank! Please try again.");

            if (string.Compare(Constants.GalleryPassword, passcode, true) == 0)
            {
                //var claims = new[]
                //{
                //            new Claim(ClaimTypes.Name, user.EmailAddress),
                //    new Claim(ClaimTypes.Role, userRole),
                //            new Claim(Constants.ClaimType.USER_ID, user.UserID.ToString()),
                //            new Claim(Constants.ClaimType.HAS_MASTER_PASSWORD, hasMasterPassword.ToString())
                //        };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["SecurityKey"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                    issuer: "frazier.com",
                    audience: "frazier.com",
                    //claims: claims,
                    expires: DateTime.UtcNow.AddDays(1),
                    signingCredentials: creds);

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    valid = true
                });
            }

            return BadRequest("Passcode invalid! Please try again.");
        }

    }

    public class SaltedHash
    {
        public string Salt { get; set; }
        public string Hash { get; set; }
    }

    public class CustomerTierStatus
    {
        public int UserID { get; set; }
        public string TierStatus { get; set; }
        public string TierCode { get; set; }
        public int AccountLimit { get; set; }
        public int SubAccounts { get; set; }
    }

    public class TokenRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class AccountRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Title { get; set; }
        public bool Master { get; set; }
    }

    public class TransferRequest
    {
        public int UserID { get; set; }
        public int ParentID { get; set; }
        //public bool IncludeSubAccounts { get; set; }
    }

    //public class Users
    //{
    //    [JsonProperty("Users")]
    //    public List<UserInfo> UserList { get; set; }
    //}

    public class UserData
    {
        public User User { get; set; }
        public User[] Users { get; set; }
    }

    public class UserCredentials
    {
        public string IPAddress { get; set; }
        public int UserID
        {
            get
            {
                return CurrentUser != null ? CurrentUser.UserID : EmpID;
            }
        }
        public int EmpID { get; private set; }
        public string Email { get; private set; }
        public string ProxyEmail { get; private set; }
        public bool IsEmployee
        {
            get
            {
                return CurrentUser == null && !string.IsNullOrEmpty(Email) && EmpID > 0;
            }
        }
        public bool HasMasterPassword { get; private set; }
        public bool HasFullAccess
        {
            //get
            //{
            //    if (IsEmployee)
            //    {
            //        int[] full_access = new int[] { 100122, 100167, 100503, 100100, 100137, 100151, 100088, 100040 };
            //        ustomerPortal.
            //        return full_access.Contains(EmpID);
            //    }

            //    return CurrentUser != null ? CurrentUser.Admin || CurrentUser.ITAdmin : false;
            //}
            get; private set;
        }
        public bool IsProxyLoggedIn
        {
            get
            {
                return !string.IsNullOrEmpty(ProxyEmail);
            }
        }
        public User CurrentUser { get; private set; }
        public User ProxyUser { get; private set; }
        //public UserCredentials Parent { get; set; }

        public UserCredentials(CustomerPortalContext customerPortal, string email, string proxy, int empId, string ipAddress, bool hasMasterPassword)
        {
            IPAddress = ipAddress;
            Email = email;
            ProxyEmail = proxy;
            EmpID = empId;
            HasMasterPassword = hasMasterPassword;

            if (!string.IsNullOrEmpty(email))
                CurrentUser = customerPortal.Users.SingleOrDefault(p => p.EmailAddress.ToLower() == email.ToLower());

            if (!string.IsNullOrEmpty(proxy))
                ProxyUser = customerPortal.Users.SingleOrDefault(p => p.EmailAddress.ToLower() == proxy.ToLower());

            if (IsEmployee)
            {
                HasFullAccess = customerPortal.EmpAccess.Any(p => p.UserID == EmpID);
            }
            else
            {
                HasFullAccess = CurrentUser != null ? CurrentUser.Admin || CurrentUser.ITAdmin : false;
            }
        }

        public void SetCurrentUser(User user)
        {
            //if (user != null)
            CurrentUser = user;
        }
    }

    public class SaveData
    {
        public int Id { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }
    }

    public class PasswordRequest
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public Guid ResetID { get; set; }
    }

    public class ContentInfo
    {
        [JsonProperty("info")]
        public HomeContent Info { get; set; }
    }
    public class PhotoInfo
    {
        public Photos Info { get; set; }
    }
    public class PathInfo
    {
        public HomePhotos Info { get; set; }
    }
    public class PhotoUpdate
    {
        public int ContentID { get; set; }
        public int[] Include { get; set; }
        public int[] Exclude { get; set; }
    }
}