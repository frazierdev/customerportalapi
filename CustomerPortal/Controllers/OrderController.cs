using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CustomerPortal.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using CustomerPortal.Models;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net.Http;
using System.IO;
using System.IO.Compression;
using CustomerPortal.Email;
using Frobot.Email;
using CustomerPortal.Images;
using System.Diagnostics;
using System.Net;
using System.Reflection;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace CustomerPortal.Controllers
{
    [Authorize]
    //[Produces("application/json")]
    //[Route("api/[controller]/[action]")]
    public class OrderController : BaseController // BaseController<OrderController>
    {
        //private readonly IConfiguration _configuration;
        //private readonly CustomerPortalContext _customerPortal;
        //private readonly SalesforceContext _salesforceContext;
        //private readonly Utilities _utilities;

        public OrderController(IConfiguration configuration, CustomerPortalContext cpContext, SalesforceContext sContext, Utilities utilities) : base(configuration, cpContext, sContext, utilities)
        {
            //_configuration = configuration;
            //_customerPortal = cpContext;
            //_salesforceContext = sContext;
            //_utilities = utilities;
        }

        [AllowAnonymous]
        public IActionResult TestHash()
        {
            string address = "47 Elizabeth Drive Chester NY 10918 USA";
            List<int> list = new List<int>();
            for (int i = 0; i < 5; i++)
            {
                System.Threading.Thread.Sleep(3000);
                int hash = address.GetHashCode();
                list.Add(hash);
                System.Diagnostics.Debug.WriteLine(hash);
            }

            return Ok(list);
        }

        public IActionResult RetrieveOrders(int userId)
        {
            var credentials = VerifyCredentials();

            if (userId == 0)
                return BadRequest("User ID not recognized. Please refresh & try again.");

            Stopwatch timer = new Stopwatch();
            timer.Start();

            var user = CustomerPortal.Users.SingleOrDefault(p => p.UserID == userId);
            User account = null;

            if (user != null)
            {
                if (user.Master)
                    account = user;
                else if (!user.Master && user.ParentID > 0)
                    account = CustomerPortal.Users.SingleOrDefault(p => p.UserID == user.ParentID);
            }

            if (account == null)
            {
                timer.Stop();
                return Ok();
            }

            credentials.SetCurrentUser(account);
            string[] orders = GetOrderList(credentials);

            //if (orders.Length == 0)
            //    return Ok();

            List<string> parameterNames = new List<string>();
            List<SqlParameter> parameters = new List<SqlParameter>();

            for (int i = 0; i < orders.Length; i++)
            {
                parameterNames.Add($"@order{i}");
                parameters.Add(new SqlParameter(parameterNames[i], orders[i]));
            }

            var data = Utilities.RetrieveDataTable($@"
                    SELECT DISTINCT
                           OrderHed.Company,
                           OrderNum,
                           OTSName,
                           OTSAddress1,
                           OTSCity,
                           OTSState,
                           OTSZIP,
	                       Description AS OTSCountry,
                           OrderDate,
                           Customer.Name AS SoldTo,
                           PONum
                    FROM dbo.OrderHed
                        JOIN Erp.Customer
                            ON Customer.Company = OrderHed.Company
                               AND Customer.CustNum = OrderHed.CustNum
                        LEFT JOIN Erp.Country
                            ON Country.Company = OrderHed.Company
                               AND OTSCountryNum = Country.CountryNum
                    WHERE ICPONum = 0
                        AND OrderNum in ({(parameterNames.Count > 0 ? string.Join(",", parameterNames) : "0")})
                    ORDER BY OrderDate DESC", parameters.ToArray());

            timer.Stop();

            string logType = "Retrieve Orders";
            CustomerPortal.Logs.Add(new Models.Log
            {
                LogDateTime = DateTime.UtcNow,
                LogType = logType,
                UserID = credentials.UserID,
                LogText = JsonConvert.SerializeObject(new { data.Rows.Count, timer.Elapsed, credentials.Email, credentials.UserID, credentials.IPAddress })
            });

            CustomerPortal.SaveChanges();
            Logger.LogIt(logType, new { data.Rows.Count, timer.Elapsed, credentials.Email, credentials.UserID });

            return Ok(data);
        }

        public IActionResult GetOrders()
        {
            var credentials = VerifyCredentials();

            Stopwatch timer = new Stopwatch();
            timer.Start();
            string[] orders = GetOrderList(credentials);

            //if (orders.Length == 0)
            //{
            //    timer.Stop();
            //    return Ok();
            //}

            List<string> parameterNames = new List<string>();
            List<SqlParameter> parameters = new List<SqlParameter>();

            for (int i = 0; i < orders.Length; i++)
            {
                parameterNames.Add($"@order{i}");
                parameters.Add(new SqlParameter(parameterNames[i], orders[i]));
            }

            // TODO: Need to add functionality for certains users to have FULL ACCESS TO ALL orders
            // TODO: don't show more than 500-1000 orders for map tab
            // TODO: Put a note on cards + Maps that becuase user has full access they don't show.
            var data = Utilities.RetrieveDataTable($@"
                    SELECT DISTINCT
                           OrderHed.Company,
                           OrderNum,
                           OTSName,
                           OTSAddress1,
                           OTSCity,
                           OTSState,
                           OTSZIP,
	                       Description AS OTSCountry,
                           OrderDate,
                           Customer.Name AS SoldTo,
                           PONum
                    FROM dbo.OrderHed
                        JOIN Erp.Customer
                            ON Customer.Company = OrderHed.Company
                               AND Customer.CustNum = OrderHed.CustNum
                               AND Customer.ICCust = 0
                        LEFT JOIN Erp.Country
                            ON Country.Company = OrderHed.Company
                               AND OTSCountryNum = Country.CountryNum
                    WHERE ICPONum = 0
                        {(credentials.HasFullAccess && !credentials.IsProxyLoggedIn ? string.Empty : $"AND OrderNum IN ({(parameterNames.Count > 0 ? string.Join(",", parameterNames) : "0")})")}
                    ORDER BY OrderDate DESC", parameters.ToArray());

            timer.Stop();

            string logType = "Load Orders";
            CustomerPortal.Logs.Add(new Models.Log
            {
                LogDateTime = DateTime.UtcNow,
                LogType = logType,
                UserID = credentials.UserID,
                LogText = JsonConvert.SerializeObject(new { data.Rows.Count, timer.Elapsed, credentials.UserID, credentials.IPAddress })
            });

            CustomerPortal.SaveChanges();

            Logger.LogIt(logType, new { credentials.Email, data.Rows.Count, timer.Elapsed, credentials.UserID });

            return Ok(data);
        }

        public IActionResult GetOrder(int OrderNum)
        {
            var credentials = VerifyCredentials();

            Stopwatch timer = new Stopwatch();
            timer.Start();

            string[] orderList = GetOrderList(credentials);

            if (orderList.Contains(OrderNum.ToString()) || credentials.HasFullAccess)
            {
                if (!credentials.IsProxyLoggedIn && !credentials.IsEmployee && !credentials.HasMasterPassword && !credentials.HasFullAccess)
                {
                    var portalOrder = CustomerPortal.AccountOrders.SingleOrDefault(p => p.OrderNum == OrderNum && p.UserID == credentials.UserID);

                    if (portalOrder != null)
                    {
                        portalOrder.LastAccessed = DateTime.UtcNow;
                        CustomerPortal.SaveChanges();
                    }
                }

                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@OrderNum", OrderNum));

                DataTable orders = GetOrderInfo(OrderNum);

                var trackers = Utilities.RetrieveDataTable($@"
                    SELECT CONVERT(   BIT,
                          CASE
                              WHEN OrderDate IS NOT NULL THEN
                                  1
                              ELSE
                                  0
                          END
                      ) AS HasOrderDate,
                           CONVERT(   BIT,
                                      CASE
                                          WHEN OrderDate IS NOT NULL THEN
                                              1
                                          ELSE
                                              0
                                      END
                                  ) AS IsProcessing,
                           CONVERT(   BIT,
                                      CASE
                                          WHEN OrderDate IS NOT NULL
                                               AND ISNULL(Shipments.ShippedQty, 0) > 0 THEN
                                              1
                                          ELSE
                                              0
                                      END
                                  ) AS IsShipping,
                           CONVERT(   BIT,
                                      CASE
                                          WHEN OrderDate IS NOT NULL
                                               AND ISNULL(Shipments.ShippedQty, 0) > 0
                                               AND PaidInFull_c = 1 THEN
                                              1
                                          ELSE
                                              0
                                      END
                                  ) AS IsComplete
                    FROM dbo.vwOrderHeaders
                        LEFT JOIN
                        (
                            SELECT Company,
                                   OrderNum,
                                   SUM(OurJobShippedQty + OurStockShippedQty) ShippedQty
                            FROM dbo.OrderRel
                            GROUP BY Company,
                                     OrderNum
                        ) Shipments
                            ON Shipments.Company = vwOrderHeaders.Company
                               AND Shipments.OrderNum = vwOrderHeaders.OrderNum
                    WHERE vwOrderHeaders.OrderNum = @OrderNum;
                    ", parameters.ToArray());

                bool surveyCompleted = CustomerPortal.SurveyComplete.Any(p => p.OrderNum == OrderNum);

                timer.Stop();

                string logType = "Load Order";
                var logObj = new { OrderNum, timer.Elapsed, surveyCompleted, credentials.Email, credentials.UserID, credentials.IPAddress };
                CustomerPortal.Logs.Add(new Models.Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = logType,
                    UserID = credentials.UserID,
                    LogText = JsonConvert.SerializeObject(logObj)
                });

                CustomerPortal.SaveChanges();

                Logger.LogIt(logType, logObj);

                return Ok(new
                {
                    orders,
                    trackers = trackers.Rows[0].ItemArray,
                    surveyCompleted
                });
            }

            timer.Stop();
            return BadRequest($"Unable to locate order {OrderNum}.");
        }

        private DataTable GetOrderInfo(int OrderNum)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@OrderNum", OrderNum));

            var data = Utilities.RetrieveDataTable($@"
SELECT vwOrderHeaders.Company,
       vwOrderHeaders.OrderNum,
       OTSName,
       OTSAddress1,
       OTSAddress2,
       OTSAddress3,
       OTSCity,
       OTSState,
       OTSZIP,
       ISNULL(OTSCountry.Description, '') AS OTSCountry,
       vwOrderHeaders.OrderDate,
       FOB.Description AS FreightTerms,
       InstallationComplete_c AS InstallationComplete,
       ShippedComplete_c AS ShippedComplete,
       InstallationOnly_c AS InstallationOnly,
       InstallationStartDate_c AS InstallationStartDate,
       InstallationEndDate_c AS InstallationEndDate,
       FrazierInstallation_c AS FrazierInstallation,
       FullyReleasedFromEngineering_c AS FullyReleasedFromEngineering,
       ProjectedFabRelease_c AS ProjectedFabRelease,
       FPBQuoteNumber_c AS QuoteNum,
       AllMaterialOnSite_c AS AllMaterialOnSite,
       BilledInFull_c AS BilledInFull,
       PaidInFull_c AS PaidInFull,
       RequestDate,
       vwOrderHeaders.CurrencyCode,
       DocOrderAmt,
       DocTotalCharges,
       PayApp_c AS PayApp,
       SpecialBilling_c AS SpecialBilling,
       ISNULL(PB.MaterialPrice, 0) AS MaterialPrice,
       ISNULL(PB.FreightPrice, 0) AS FreightPrice,
       ISNULL(PB.InstallationPrice, 0) AS InstallationPrice,
       ISNULL(PB.TeardownPrice, 0) AS TeardownPrice,
       ISNULL(PB.BondPrice, 0) AS BondPrice,
       ISNULL(PB.PermitsPrice, 0) AS PermitsPrice,
       ISNULL(PB.CalcsPrice, 0) AS CalcsPrice,
       ISNULL(PB.MiscPrice, 0) AS MiscPrice,
       ISNULL(PB.SalesTaxPrice, 0) AS SalesTaxPrice,
       ISNULL(PB.ProjectPrice, 0) AS ProjectPrice,
       ISNULL(PB.OPPrice, 0) AS OPPrice,
       SoldTo.Name AS SoldTo,
       SoldTo.Address1 AS SoldToAddress1,
       SoldTo.Address2 AS SoldToAddress2,
       SoldTo.Address3 AS SoldToAddress3,
       SoldTo.City AS SoldToCity,
       SoldTo.State AS SoldToState,
       SoldTo.Zip AS SoldToZip,
       SoldTo.Country AS SoldToCountry,
       BillTo.Name AS BillTo,
       BillTo.Address1 AS BillToAddress1,
       BillTo.Address2 AS BillToAddress2,
       BillTo.Address3 AS BillToAddress3,
       BillTo.City AS BillToCity,
       BillTo.State AS BillToState,
       BillTo.Zip AS BillToZip,
       BillTo.Country AS BillToCountry,
       PONum,
       CASE
           WHEN ISNULL(SalesRep1.InActive, 1) = 0 THEN
               SalesRep1.SalesRepCode
           ELSE
               ReportsTo1.SalesRepCode
       END AS SalesRepID1,
       CASE
           WHEN ISNULL(SalesRep1.InActive, 1) = 0 THEN
               SalesRep1.Name
           ELSE
               ReportsTo1.Name
       END AS SalesRepName1,
       CASE
           WHEN ISNULL(SalesRep1.InActive, 1) = 0 THEN
               SalesRep1.EMailAddress
           ELSE
               ReportsTo1.EMailAddress
       END AS SalesRepEmail1,
       CASE
           WHEN ISNULL(SalesRep1.InActive, 1) = 0 THEN
               ISNULL(NULLIF(SalesRep1.OfficePhoneNum, ''), '908-876-3001')
           ELSE
               ISNULL(NULLIF(ReportsTo1.OfficePhoneNum, ''), '908-876-3001')
       END AS SalesRepPhone1,
       CASE
           WHEN ISNULL(SalesRep1.InActive, 1) = 0 THEN
               SalesRep1.SalesRepTitle
           ELSE
               ReportsTo1.SalesRepTitle
       END AS SalesRepTitle1,
       CASE
           WHEN ISNULL(SalesRep2.InActive, 1) = 0 THEN
               SalesRep2.SalesRepCode
           WHEN ReportsTo2.Company IS NOT NULL THEN
               ReportsTo2.SalesRepCode
           ELSE
               NULL
       END AS SalesRepID2,
       CASE
           WHEN ISNULL(SalesRep2.InActive, 1) = 0 THEN
               SalesRep2.Name
           WHEN ReportsTo2.Company IS NOT NULL THEN
               ReportsTo2.Name
           ELSE
               NULL
       END AS SalesRepName2,
       CASE
           WHEN ISNULL(SalesRep2.InActive, 1) = 0 THEN
               SalesRep2.EMailAddress
           WHEN ReportsTo2.Company IS NOT NULL THEN
               ReportsTo2.EMailAddress
           ELSE
               NULL
       END AS SalesRepEmail2,
       CASE
           WHEN ISNULL(SalesRep2.InActive, 1) = 0 THEN
               ISNULL(NULLIF(SalesRep2.OfficePhoneNum, ''), '908-876-3001')
           WHEN ReportsTo2.Company IS NOT NULL THEN
               ISNULL(NULLIF(ReportsTo2.OfficePhoneNum, ''), '908-876-3001')
           ELSE
               NULL
       END AS SalesRepPhone2,
       CASE
           WHEN ISNULL(SalesRep2.InActive, 1) = 0 THEN
               SalesRep2.SalesRepTitle
           WHEN ReportsTo2.Company IS NOT NULL THEN
               ReportsTo2.SalesRepTitle
           ELSE
               NULL
       END AS SalesRepTitle2,
       CASE
           WHEN ISNULL(SalesRep3.InActive, 1) = 0 THEN
               SalesRep3.SalesRepCode
           WHEN ReportsTo3.Company IS NOT NULL THEN
               ReportsTo3.SalesRepCode
           ELSE
               NULL
       END AS SalesRepID3,
       CASE
           WHEN ISNULL(SalesRep3.InActive, 1) = 0 THEN
               SalesRep3.Name
           WHEN ReportsTo3.Company IS NOT NULL THEN
               ReportsTo3.Name
           ELSE
               NULL
       END AS SalesRepName3,
       CASE
           WHEN ISNULL(SalesRep3.InActive, 1) = 0 THEN
               SalesRep3.EMailAddress
           WHEN ReportsTo3.Company IS NOT NULL THEN
               ReportsTo3.EMailAddress
           ELSE
               NULL
       END AS SalesRepEmail3,
       CASE
           WHEN ISNULL(SalesRep3.InActive, 1) = 0 THEN
               ISNULL(NULLIF(SalesRep3.OfficePhoneNum, ''), '908-876-3001')
           WHEN ReportsTo3.Company IS NOT NULL THEN
               ISNULL(NULLIF(ReportsTo3.OfficePhoneNum, ''), '908-876-3001')
           ELSE
               NULL
       END AS SalesRepPhone3,
       CASE
           WHEN ISNULL(SalesRep3.InActive, 1) = 0 THEN
               SalesRep3.SalesRepTitle
           WHEN SalesRep3.Company IS NOT NULL THEN
               ReportsTo3.SalesRepTitle
           ELSE
               NULL
       END AS SalesRepTitle3,
       ISNULL(PM.Name, SL.Name) PM,
       ISNULL(PM.EmpID, SL.EmpID) PMID,
       ISNULL(PM.EMailAddress, SL.EMailAddress) PMEmail,
       ISNULL(PM.Title_c, SL.Title_c) PMTitle,
       CASE
           WHEN NULLIF(ISNULL(PM.Phone, SL.Phone), '') IS NOT NULL THEN
               ISNULL(PM.Phone, SL.Phone)
           ELSE
               '908-876-3001'
       END PMPhone,
       ISNULL(CSRep.EmpID, CS_Manager.EmpID) CSRepID,
       ISNULL(CSRep.Name, CS_Manager.Name) CSRepName,
       ISNULL(CSRep.Title_c, CS_Manager.Title_c) CSRepTitle,
       ISNULL(CSRep.EMailAddress, CS_Manager.EMailAddress) CSRepEmail,
       CASE
           WHEN NULLIF(ISNULL(CSRep.Phone, CS_Manager.Phone), '') IS NOT NULL THEN
               ISNULL(CSRep.Phone, CS_Manager.Phone)
           ELSE
               '908-876-3001'
       END CSRepPhone,
       ISNULL(ARRep.EmpID, AR_Manager.EmpID) ARRepID,
       ISNULL(ARRep.Name, AR_Manager.Name) ARRepName,
       ISNULL(ARRep.Title_c, AR_Manager.Title_c) ARRepTitle,
       'ACCTSREC@frazier.com' ARRepEmail,
       --ISNULL(ARRep.EMailAddress, AR_Manager.EMailAddress) ARRepEmail,

       CASE
           WHEN NULLIF(ISNULL(ARRep.Phone, AR_Manager.Phone), '') IS NOT NULL THEN
               ISNULL(ARRep.Phone, AR_Manager.Phone)
           ELSE
               '908-876-3001'
       END ARRepPhone,
       CONVERT(   BIT,
                  CASE
                      WHEN (
                               (
                                   FrazierInstallation_c = 1
                                   AND InstallationComplete_c = 1
                               )
                               OR FrazierInstallation_c = 0
                           )
                           AND
                           (
                               InstallationOnly_c = 1
                               OR ShippedComplete_c = 1
                           ) THEN
                          1
                      ELSE
                          0
                  END
              ) SurveyReady
FROM dbo.vwOrderHeaders
    LEFT JOIN transfer.tblProject PB
        ON TRY_CONVERT(INT, PB.OrderNumber) = OrderNum
           AND PB.OrderLevel = CONumber_c
    JOIN Erp.Customer AS SoldTo
        ON SoldTo.Company = vwOrderHeaders.Company
           AND SoldTo.CustNum = vwOrderHeaders.CustNum
    LEFT JOIN Erp.Country AS OTSCountry
        ON OTSCountry.Company = vwOrderHeaders.Company
           AND OTSCountry.CountryNum = vwOrderHeaders.OTSCountryNum
    JOIN Erp.Customer AS BillTo
        ON BillTo.Company = vwOrderHeaders.Company
           AND BillTo.CustNum = vwOrderHeaders.BTCustNum
    LEFT JOIN Erp.FOB
        ON FOB.Company = vwOrderHeaders.Company
           AND FOB.FOB = vwOrderHeaders.FOB
    LEFT JOIN dbo.EmpBasic Sales1
        ON Sales1.MainCompany_c = 1
           AND Sales1.EmpID = vwOrderHeaders.SalesRep1_c
    LEFT JOIN dbo.EmpBasic Sales2
        ON Sales2.MainCompany_c = 1
           AND Sales2.EmpID = vwOrderHeaders.SalesRep2_c
    LEFT JOIN dbo.EmpBasic Sales3
        ON Sales3.MainCompany_c = 1
           AND Sales3.EmpID = vwOrderHeaders.SalesRep3_c
    LEFT JOIN dbo.SalesRep SalesRep1
        ON SalesRep1.Company = Sales1.Company
           AND SalesRep1.SalesRepCode = Sales1.EmpID
    LEFT JOIN dbo.SalesRep SalesRep2
        ON SalesRep2.Company = Sales2.Company
           AND SalesRep2.SalesRepCode = Sales2.EmpID
    LEFT JOIN dbo.SalesRep SalesRep3
        ON SalesRep3.Company = Sales3.Company
           AND SalesRep3.SalesRepCode = Sales3.EmpID
    --AND SalesRep.InActive = 0
    LEFT JOIN dbo.EmpBasic PM
        ON PM.MainCompany_c = 1
           AND ProjectManager_c = PM.EmpID
           AND PM.ProjectManagement_c = 1
           AND PM.EmpStatus = 'A'
    LEFT JOIN dbo.EmpBasic AS CSRep
        ON CSRep.MainCompany_c = 1
           AND vwOrderHeaders.CustomerServiceRep_c = CSRep.EmpID
           AND CSRep.CustomerServiceAgent_c = 1
           AND CSRep.EmpStatus = 'A'
    LEFT JOIN dbo.EmpBasic AS ARRep
        ON ARRep.MainCompany_c = 1
           AND vwOrderHeaders.ARRep_c = ARRep.EmpID
           AND ARRep.ARRep_c = 1
           AND ARRep.EmpStatus = 'A'
    LEFT JOIN dbo.EmpBasic AS SL
        ON SL.MainCompany_c = 1
           AND '100137' = SL.EmpID
    LEFT JOIN dbo.EmpBasic AS AR_Manager
        ON AR_Manager.MainCompany_c = 1
           AND '100893' = AR_Manager.EmpID
    LEFT JOIN dbo.EmpBasic AS CS_Manager
        ON CS_Manager.MainCompany_c = 1
           AND '100122' = CS_Manager.EmpID
    LEFT JOIN dbo.SalesRep AS ReportsTo1
        ON SalesRep1.Company = ReportsTo1.Company
           AND ISNULL(NULLIF(SalesRep1.RepReportsTo, ''), '100039') = ReportsTo1.SalesRepCode
    LEFT JOIN dbo.SalesRep AS ReportsTo2
        ON SalesRep2.Company = ReportsTo2.Company
           AND ISNULL(NULLIF(SalesRep2.RepReportsTo, ''), '100039') = ReportsTo2.SalesRepCode
    LEFT JOIN dbo.SalesRep AS ReportsTo3
        ON SalesRep3.Company = ReportsTo3.Company
           AND ISNULL(NULLIF(SalesRep3.RepReportsTo, ''), '100039') = ReportsTo3.SalesRepCode
WHERE vwOrderHeaders.OrderNum = @OrderNum;
                    ", parameters.ToArray());

            return data;
        }

        public IActionResult GetOrderBilling(int OrderNum)
        {
            var credentials = VerifyCredentials();

            string[] orderList = GetOrderList(credentials);

            if (orderList.Contains(OrderNum.ToString()) || credentials.HasFullAccess)
            {

                List<SqlParameter> parameters = new List<SqlParameter>();

                if (OrderNum > 0)
                    parameters.Add(new SqlParameter("@OrderNum", OrderNum));

                var invoices = GetInvoices(OrderNum);

                var special = Utilities.RetrieveDataTable($@"
                    SELECT Attachments.AttachmentID,
                           FileName,
                           Description,
                           FileType,
                           Category
                    FROM dbo.vwOrderHeaders
                        JOIN customerportal.Attachments
                            ON Attachments.OrderNum = vwOrderHeaders.OrderNum
                               AND Attachments.Category IN ('Special Billing', 'Billing Related' )
                    WHERE vwOrderHeaders.OrderNum = @OrderNum
                          AND
                          (
                              PayApp_c = 1
                              OR SpecialBilling_c = 1
                              OR BilledInvoices_c = 1
                              OR CashReceived_c = 1
                          )
                    ORDER BY Attachments.DateAdded DESC;", parameters.ToArray());

                var changes = Utilities.RetrieveDataTable($@"
                    SELECT Curr.AuthorID,
                            Curr.ProjectID,
                            Curr.OrderNumber,
                            Curr.OrderLevel CO,
                            Curr.ProjectPrice - Curr.SalesTaxPrice CurrentPrice,
                            ISNULL(Prev.projectprice, 0) - ISNULL(Prev.SalesTaxPrice, 0) PreviousPrice,
                            (Curr.ProjectPrice - Curr.SalesTaxPrice) - (ISNULL(Prev.projectprice, 0) - ISNULL(Prev.SalesTaxPrice, 0)) Price
                    FROM transfer.tblProject Curr
                        LEFT JOIN transfer.tblProject Prev
                            ON Prev.AuthorID = Curr.AuthorID
                                AND Prev.ProjectID = Curr.ProjectID
                                AND Prev.OrderNumber = Curr.OrderNumber
                                AND Prev.OrderLevel = Curr.OrderLevel - 1
                    WHERE ((Curr.ProjectPrice - Curr.SalesTaxPrice) - (ISNULL(Prev.ProjectPrice, 0) - ISNULL(Prev.SalesTaxPrice, 0)) <> 0)
                            AND Curr.OrderNumber = CONVERT(NVARCHAR(20), @OrderNum)
                    ORDER BY Curr.OrderLevel;", parameters.ToArray());

                var schedule = Utilities.RetrieveDataTable($@"
                    SELECT OrderNum,
                           InvoicingDetailJSON,
                           EngineeringReleasePercentComplete,
                           OpOrdersPercentComplete,
                           FabricationPercentComplete,
                           FreightPercentComplete,
                           InstallationPercentComplete,
                           TeardownPercentComplete,
                           PermitsPercentComplete,
                           CalcPercentComplete,
                           MaterialPercentComplete,
                           BondsPercentComplete,
                           MiscPercentComplete
                    FROM dbo.Orders
                    WHERE OrderNum = @OrderNum;", parameters.ToArray(), Constants.Connections.MES);


                var orderInvoicingTotals = Utilities.RetrieveDataTable(@"
                    SELECT dtl.Company,
                           dtl.OrderNum,
                           dtl.PartNum,
                           dtl.UnitPrice,
                           SUM(invcDtl.ExtPrice) AS TotalInvoiced,
                           CASE
                               WHEN dtl.UnitPrice > 0 THEN
                                   SUM(invcDtl.ExtPrice) / dtl.UnitPrice
                               ELSE
                                   0
                           END AS InvoicedPercent,
                           CONCAT(
                                     'Invoiced ',
                                     FORMAT(SUM(invcDtl.ExtPrice), 'C', 'en-us'),
                                     ' of ',
                                     FORMAT(dtl.UnitPrice, 'C', 'en-us'),
                                     ' sold<br>',
                                     FORMAT(dtl.UnitPrice - SUM(invcDtl.ExtPrice), 'C', 'en-us'),
                                     ' remains to invoice.'
                                 ) Tooltip
                    FROM Erp.InvcDtl AS invcDtl
                        INNER JOIN Erp.InvcHead AS ih
                            ON ih.Company = invcDtl.Company
                               AND ih.InvoiceNum = invcDtl.InvoiceNum
                        INNER JOIN dbo.OrderDtl AS dtl
                            ON dtl.Company = invcDtl.Company
                               AND dtl.OrderNum = invcDtl.OrderNum
                               AND dtl.OrderLine = invcDtl.OrderLine
                               AND dtl.RevenueLine_c = 1
                    WHERE dtl.OrderNum = @OrderNum
                          AND ih.Posted = 1
                    GROUP BY dtl.Company,
                             dtl.OrderNum,
                             dtl.PartNum,
                             dtl.UnitPrice;", parameters.ToArray());


                return Ok(new
                {
                    invoices,
                    special,
                    changes,
                    schedule,
                    orderInvoicingTotals
                });
            }

            return Ok();
        }

        private DataTable GetInvoices(int OrderNum)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            if (OrderNum > 0)
                parameters.Add(new SqlParameter("@OrderNum", OrderNum));

            var invoices = Utilities.RetrieveDataTable($@"
                        SELECT vwBillingDetail.Company,
                               vwBillingDetail.InvoiceNum,
                               vwBillingDetail.InvoiceDate,
                               vwBillingDetail.DueDate,
                               vwBillingDetail.CreditMemo,
                               vwBillingDetail.Material,
                               vwBillingDetail.Freight,
                               vwBillingDetail.Install,
                               vwBillingDetail.Teardown,
                               vwBillingDetail.Calcs,
                               vwBillingDetail.Bond,
                               vwBillingDetail.Perm,
                               vwBillingDetail.Misc,
                               vwBillingDetail.Tax,
                               vwBillingDetail.PreTax,
                               vwBillingDetail.DocInvoiceAmt,
                               vwBillingDetail.DocInvoiceBal,
                               vwBillingDetail.Collected,
                               vwBillingDetail.OrderNum,
                               vwBillingDetail.HideInvoiceInPortal_c,
                               Attachments.AttachmentID,
                               FileName
                        FROM dbo.vwBillingDetail
                            JOIN dbo.vwOrderHeaders
                                ON vwOrderHeaders.Company = vwBillingDetail.Company
                                   AND vwOrderHeaders.OrderNum = vwBillingDetail.OrderNum
                                   AND YEAR(vwOrderHeaders.OrderDate) > 2019
                            JOIN customerportal.Attachments
                                ON Attachments.OrderNum = vwBillingDetail.OrderNum
                                   AND CONCAT(vwBillingDetail.InvoiceNum, '.pdf') = FileName
                                   AND Attachments.Category = 'Billing'
                        WHERE vwBillingDetail.OrderNum = @OrderNum
                              AND vwBillingDetail.HideInvoiceInPortal_c = 0
                        ORDER BY vwBillingDetail.DueDate DESC, vwBillingDetail.InvoiceDate DESC;", parameters.ToArray());

            return invoices;
        }

        [AllowAnonymous]
        public FileResult GetStaticMapImage(string address, int zoom)
        {
            //System.Threading.Thread.Sleep(30000);
            var b = new GoogleAPI(CustomerPortal).GetStaticAerialMapImage(address, zoom);
            return File(b, "image/jpeg");
        }

        [AllowAnonymous]
        public FileResult GetStaticStreetViewImage(string address)
        {
            var b = new GoogleAPI(CustomerPortal).GetStaticStreetViewImage(address);
            return File(b, "image/jpeg");
        }

        [AllowAnonymous]
        public IActionResult GetGeolocation(string address)
        {
            return Ok(new GoogleAPI(CustomerPortal).GetGeolocation(address));
        }

        [AllowAnonymous]
        public IActionResult GetGeolocations(string addresses)
        {
            string[] addressList = JsonConvert.DeserializeObject<string[]>(addresses);

            List<Geolocations> locations = new List<Geolocations>();

            foreach (string address in addressList)
                locations.Add(new GoogleAPI(CustomerPortal).GetGeolocation(address));

            return Ok(locations);
        }

        public IActionResult GetFrazierContacts()
        {
            var credentials = VerifyCredentials();

            Stopwatch timer = new Stopwatch();
            timer.Start();
            List<string> orders = GetOrderList(credentials).ToList();

            var contacts = GetContacts(orders);
            timer.Stop();

            int count = contacts != null && contacts.Rows.Count > 0 ? contacts.Rows.Count : 0;

            string logType = "Load Contacts";
            CustomerPortal.Logs.Add(new Models.Log
            {
                LogDateTime = DateTime.UtcNow,
                LogType = logType,
                UserID = credentials.UserID,
                LogText = JsonConvert.SerializeObject(new { Count = count, timer.Elapsed, credentials.Email, credentials.UserID, credentials.IPAddress })
            });

            CustomerPortal.SaveChanges();

            Logger.LogIt(logType, new { credentials.Email, Count = count, timer.Elapsed, credentials.UserID, credentials.IPAddress });

            return Ok(new
            {
                contacts
            });
        }

        public IActionResult GetContactOrders(string ID)
        {
            var credentials = VerifyCredentials();

            Stopwatch timer = new Stopwatch();
            timer.Start();
            string[] orders = GetOrderList(credentials);

            //if (orders.Length == 0)
            //{
            //    timer.Stop();
            //    return Ok();
            //}

            var parameterNames = new List<string>();
            var parameters = new List<SqlParameter>();

            for (int i = 0; i < orders.Length; i++)
            {
                parameterNames.Add($"@order{i}");
                parameters.Add(new SqlParameter(parameterNames[i], orders[i]));
            }

            parameters.Add(new SqlParameter("@ID", ID));

            if (!string.IsNullOrEmpty(ID))
            {
                var contactOrders = Utilities.RetrieveDataTable($@"
                    SELECT DISTINCT
                           vwOrderHeaders.Company,
                           vwOrderHeaders.OrderNum,
                           vwOrderHeaders.OrderDate,
                           OTSName,
                           OTSCity,
                           OTSState
                    FROM dbo.vwOrderHeaders
                        JOIN Erp.Customer
                            ON Customer.Company = vwOrderHeaders.Company
                               AND Customer.CustNum = vwOrderHeaders.CustNum
                    WHERE (
                              SalesRep1_c = @ID
                              OR SalesRep2_c = @ID
                              OR SalesRep3_c = @ID
                              OR CustomerServiceRep_c = @ID
                              OR ProjectManager_c = @ID
                              OR ARRep_c = @ID
                          )
                          AND OrderNum IN ({(parameterNames.Count > 0 ? string.Join(", ", parameterNames) : "0")})
                    ORDER BY vwOrderHeaders.OrderDate DESC;
                ", parameters.ToArray());
                //OR AssignedDetailer_c = @ID
                //OR AssignedDetailer2_c = @ID
                //OR AssignedDetailer3_c = @ID
                //OR OrderHed.AR_Person_c = @ID
                //OR InstallationMgr_c = @ID
                //OR ProjectCoordinator_c = @ID

                DataTable person = Utilities.RetrieveDataTable($@"
                    SELECT Name
                    FROM dbo.EmpBasic
                    WHERE EmpID = @ID
                        AND EmpBasic.MainCompany_c = 1;
                ", new SqlParameter[] {
                                new SqlParameter("@ID", ID),
                            });

                string name = "";
                if (person.Rows.Count > 0)
                {
                    DataRow row = person.Rows[0];
                    name = row["Name"].ToString();
                }

                timer.Stop();
                string logType = "Load Contact Orders";
                CustomerPortal.Logs.Add(new Models.Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = logType,
                    UserID = credentials.UserID,
                    LogText = JsonConvert.SerializeObject(new { ID, Name = name, contactOrders.Rows.Count, timer.Elapsed, credentials.Email, credentials.UserID, credentials.IPAddress })
                });

                CustomerPortal.SaveChanges();

                Logger.LogIt(logType, new { credentials.Email, Name = name, contactOrders.Rows.Count, timer.Elapsed, credentials.UserID, credentials.IPAddress });

                return Ok(new
                {
                    contactOrders,
                    name
                });
            }

            timer.Stop();
            return BadRequest("Unable to lookup contact to retrieve related orders.");

        }

        // TODO: Finish setting up this method when full billing page section is deployed
        public IActionResult GetBilling()
        {
            throw new NotImplementedException("This method is not yet in use.");


            //var credentials = VerifyCredentials();

            //_customerPortal.Logs.Add(new Models.Log
            //{
            //    LogDateTime = DateTime.UtcNow,
            //    LogType = "Load Billing",
            //    UserID = credentials.UserID,
            //    LogText = (credentials.IsProxyLoggedIn? $" [{credentials.ProxyEmail}]" : string.Empty)
            //});

            //_customerPortal.SaveChanges();

            //var invoices = _utilities.RetrieveEpicorDataTable($@"

            //       SELECT  InvcHead.OrderNum ,
            //        InvoiceNum ,
            //        InvoiceDate ,
            //        InvoiceAmt ,
            //        InvoiceBal ,
            //        DueDate,
            //        OTSName,
            //        OTSCity,
            //        OTSState
            //FROM    erp.InvcHead
            //        JOIN dbo.vwOrderHeaders ON vwOrderHeaders.Company = InvcHead.Company
            //                             AND vwOrderHeaders.OrderNum = InvcHead.OrderNum
            //        JOIN erp.Customer ON Customer.Company = vwOrderHeaders.Company
            //                             AND Customer.CustNum = vwOrderHeaders.CustNum
            //WHERE   Posted = 1
            //        AND InvoiceType <> 'SHP'
            //        AND Email IN (@EMailAddress,@ParentEmail)
            //        AND Email <> ''
            //        ORDER BY InvcHead.OrderNum, InvoiceDate;

            //    ", new SqlParameter[] {
            //        new SqlParameter("@EMailAddress", email),
            //        new SqlParameter("@ParentEmail", parent)
            //    });

            //return Ok(new
            //{
            //    invoices
            //});

        }

        public IActionResult GetScheduleEvents(int OrderNum)
        {
            List<CustomerPortalScheduleEvent> dates = new List<CustomerPortalScheduleEvent>();

            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@OrderNum", OrderNum));

            var data = Utilities.RetrieveDataTable($@"
                SELECT vwOrderHeaders.OrderNum,
                       OrderDate,
                       RequestDate,
                       ProjectedFabRelease_c,
                       FrazierInstallation_c,
                       InstallationStartDate_c,
                       InstallationEndDate_c,
                       AllMaterialOnSite_c
                FROM dbo.vwOrderHeaders
                WHERE vwOrderHeaders.OrderNum = @OrderNum;
                ", parameters.ToArray());

            DataRow order = data.Rows.Count > 0 ? data.Rows[0] : null;

            if (order != null)
            {
                bool hasOrderDate = DateTime.TryParse(order["OrderDate"].ToString(), out DateTime orderDate);
                bool hasRequestDate = DateTime.TryParse(order["RequestDate"].ToString(), out DateTime requestDate);
                bool hasProjFabRelease = DateTime.TryParse(order["ProjectedFabRelease_c"].ToString(), out DateTime projFabRelease);
                bool hasFrazierInstall = bool.TryParse(order["FrazierInstallation_c"].ToString(), out bool isFrazierInstall);
                bool hasInstallStartDate = DateTime.TryParse(order["InstallationStartDate_c"].ToString(), out DateTime installStartDate);
                bool hasInstallEndDate = DateTime.TryParse(order["InstallationEndDate_c"].ToString(), out DateTime installEndDate);
                bool hasMtlOnSite = DateTime.TryParse(order["AllMaterialOnSite_c"].ToString(), out DateTime mtlOnSite);

                // Order Date
                if (hasOrderDate)
                {
                    dates.Add(new CustomerPortalScheduleEvent
                    {
                        name = "Order Placed",
                        startDate = orderDate,
                        color = "#1AD219"
                    });
                }

                // STA/Delivery Date
                if (hasRequestDate)
                {
                    dates.Add(new CustomerPortalScheduleEvent
                    {
                        name = "Delivery Date",
                        startDate = requestDate,
                        color = "#27D8A6"
                        //color = "#1976D2"
                    });
                }

                // Engineering Time
                if (hasProjFabRelease)
                {
                    dates.Add(new CustomerPortalScheduleEvent
                    {
                        name = "Engineering",
                        startDate = orderDate,
                        endDate = projFabRelease,
                        color = "#D27519"
                    });
                }

                // Installation Time
                if (hasFrazierInstall && isFrazierInstall && hasInstallStartDate && hasInstallEndDate)
                {
                    dates.Add(new CustomerPortalScheduleEvent
                    {
                        name = "Installation",
                        startDate = installStartDate,
                        endDate = installEndDate,
                        color = "#D82759"
                        //color = "#4D27D8"
                    });
                }

                // All Material On Site Date
                if (hasMtlOnSite)
                {
                    dates.Add(new CustomerPortalScheduleEvent
                    {
                        name = "All Material On Site",
                        startDate = mtlOnSite,
                        color = "#D119D2"
                    });
                }

                // Manufacturing
                if (hasMtlOnSite && hasProjFabRelease)
                {
                    dates.Add(new CustomerPortalScheduleEvent
                    {
                        name = "Manufacturing",
                        startDate = projFabRelease,
                        endDate = mtlOnSite,
                        color = "#1976D2"
                        //color = "#D82759"
                    });
                }
            }

            dates.AddRange(CustomerPortal.ScheduledOrderEvents.Where(p => p.OrderNum == OrderNum).Select(p => new CustomerPortalScheduleEvent
            {
                name = p.Title,
                startDate = p.StartDate,
                endDate = p.EndDate,
                color = p.Color,
                editable = true,
                id = p.ScheduledOrderEventID,
                addedBy = p.AddedBy
            }));

            return Ok(new
            {
                events = dates.OrderBy(p => p.startDate).ToList()
            });
        }

        public IActionResult GetAttachments(int OrderNum)
        {
            string[] ignore = new string[] { "billing", "special billing", "billing related" };

            var attachments = CustomerPortal.Attachments.Where(p => p.OrderNum == OrderNum && !ignore.Contains(p.Category.ToLower())).OrderByDescending(p => p.DateAdded).ToList();

            return Ok(new
            {
                attachments
            });
        }

        public IActionResult GetTasks(int OrderNum)
        {
            var tasks = CustomerPortal.TaskItems.Where(p => p.OrderNum == OrderNum).OrderBy(p => p.DueDate).ToList();

            return Ok(new
            {
                tasks
            });
        }

        //[AllowAnonymous]
        // TODO: Should proabably merge the two methods DownloadAttachment & GetAttachmentById
        public void DownloadAttachment(int AttachmentID)
        {
            var file = CustomerPortal.Attachments.SingleOrDefault(p => p.AttachmentID == AttachmentID);
            FileInfo info = new FileInfo(file?.FileLocation ?? "");
            //return File(info.FullName, System.Net.Mime.MediaTypeNames.Application.Octet, info.Name);

            if (info.Exists)
            {
                var credentials = VerifyCredentials();

                string logType = "Download Attachment";
                var attachment = new { credentials.Email, file.AttachmentID, file.Description, file.Category, file.OrderNum, info.FullName, file.AttachedBy, info.Name, credentials.UserID, credentials.IPAddress };
                CustomerPortal.Logs.Add(new Models.Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = logType,
                    UserID = credentials.UserID,
                    LogText = JsonConvert.SerializeObject(attachment)
                });
                CustomerPortal.SaveChanges();
                Logger.LogIt(logType, attachment);

                byte[] result = System.IO.File.ReadAllBytes(info.FullName);
                Response.Body.Write(result, 0, result.Length);
                Response.Body.Flush();
                Response.Body.Close();

            }
        }

        public void DownloadAllOrderAttachments(int OrderNum)
        {
            //var file = _customerPortal.Attachments.SingleOrDefault(p => p.OrderNum == OrderNum);
            //FileInfo info = new FileInfo(file?.FileLocation ?? "");

            DirectoryInfo info = new DirectoryInfo($@"{Constants.Directories.PortalAttachments}{OrderNum}");
            //DirectoryInfo zip = new DirectoryInfo($@"{Constants.Directories.TempAttachments}\Test{OrderNum}");
            FileInfo zip = new FileInfo($@"{Constants.Directories.TempAttachments}\Temp_{OrderNum}.zip");
            // TODO: Change this approach to be based on the Attachments table & Category can be easier to handle
            if (info.Exists)
            {
                if (zip.Exists)
                {
                    zip.Delete();
                    zip.Refresh();

                    //int attempt = 0;
                    //while (zip.Exists && attempt < 3)
                    //{
                    //    attempt++;
                    //    System.Threading.Thread.Sleep(3000);
                    //    zip.Refresh();
                    //}
                }

                ZipFile.CreateFromDirectory(info.FullName, zip.FullName);

                zip.Refresh();

                var credentials = VerifyCredentials();

                string logType = "Download Zip Attachment";

                var attachment = new { credentials.Email, OrderNum, info.FullName, info.Name, credentials.UserID, credentials.IPAddress };
                //var attachment = new { credentials.Email, file.AttachmentID, file.Description, file.Category, file.OrderNum, info.FullName, file.AttachedBy, info.Name, credentials.IPAddress };

                CustomerPortal.Logs.Add(new Models.Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = logType,
                    UserID = credentials.UserID,
                    LogText = JsonConvert.SerializeObject(attachment)
                });
                CustomerPortal.SaveChanges();
                Logger.LogIt(logType, attachment);


                byte[] result = System.IO.File.ReadAllBytes(zip.FullName);

                Response.Body.Write(result, 0, result.Length);
                Response.Body.Flush();
                Response.Body.Close();

                if (System.IO.File.Exists(zip.FullName))
                    System.IO.File.Delete(zip.FullName);
            }
        }

        public void DownloadZipAttachment(int OrderNum, string Category = "")
        {
            DirectoryInfo info = new DirectoryInfo($@"{Constants.Directories.PortalAttachments}{OrderNum}");

            if (info.Exists)
            {
                var credentials = VerifyCredentials();

                string fileLocation = CreateZipFile(OrderNum, Category);

                if (!string.IsNullOrEmpty(fileLocation))
                {
                    FileInfo zip = new FileInfo(fileLocation);

                    if (zip.Exists)
                    {
                        string logType = "Download Zip Attachment";

                        var attachment = new { credentials.Email, OrderNum, Category, Directory = info.FullName, info.Name, credentials.UserID, credentials.IPAddress, ZipFile = zip.FullName };
                        //var attachment = new { credentials.Email, file.AttachmentID, file.Description, file.Category, file.OrderNum, info.FullName, file.AttachedBy, info.Name, credentials.IPAddress };

                        CustomerPortal.Logs.Add(new Models.Log
                        {
                            LogDateTime = DateTime.UtcNow,
                            LogType = logType,
                            UserID = credentials.UserID,
                            LogText = JsonConvert.SerializeObject(attachment)
                        });
                        CustomerPortal.SaveChanges();
                        Logger.LogIt(logType, attachment);


                        byte[] result = System.IO.File.ReadAllBytes(zip.FullName);

                        Response.Body.Write(result, 0, result.Length);
                        Response.Body.Flush();
                        Response.Body.Close();

                        if (System.IO.File.Exists(zip.FullName))
                            System.IO.File.Delete(zip.FullName);

                    }
                }
            }
        }

        private string CreateZipFile(int OrderNum, string Category)
        {
            string saved = string.Empty;
            var attachments = CustomerPortal.Attachments.Where(p => p.OrderNum == OrderNum).ToList();

            string temp = $@"{Constants.Directories.TempAttachments}Temp_{OrderNum}_{DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss")}";

            if (!string.IsNullOrEmpty(Category))
                attachments = attachments.Where(p => p.Category.ToLower() == Category.ToLower()).ToList();

            if (attachments.Count > 0)
            {
                DirectoryInfo tempDir = new DirectoryInfo($@"{temp}\");

                if (tempDir.Exists)
                {
                    tempDir.Delete(true);
                    System.Threading.Thread.Sleep(3000);
                }

                tempDir.Create();

                foreach (var category in attachments.GroupBy(p => p.Category).ToList())
                {
                    bool isBilling = string.Compare(category.Key, "billing", true) == 0;
                    List<int> invoiceNums = new List<int>();
                    if (isBilling)
                    {
                        DataTable invoices = GetInvoices(OrderNum);

                        foreach (DataRow invoice in invoices.Rows)
                        {
                            invoiceNums.Add(invoice.Field<int>("InvoiceNum"));
                        }
                    }

                    string dirName = $@"{tempDir.FullName}{category.Key}\";
                    DirectoryInfo dir = new DirectoryInfo(dirName);
                    if (!dir.Exists)
                    {
                        dir.Create();
                        dir.Refresh();
                    }

                    foreach (var attachment in category)
                    {
                        if (isBilling)
                        {
                            var order = GetOrderInfo(OrderNum);
                            bool payApp = order.Rows[0].Field<bool>("PayApp");
                            bool special = order.Rows[0].Field<bool>("SpecialBilling");
                            if (!(int.TryParse(attachment.FileName.Replace(".pdf", ""), out int invoiceNum) && invoiceNums.Contains(invoiceNum) && !payApp && !special))
                                continue;
                        }

                        DirectoryInfo file = new DirectoryInfo(attachment.FileLocation);
                        System.IO.File.Copy(file.FullName, $"{dir.FullName}{file.Name}");
                    }

                    if (isBilling)
                    {
                        var files = dir.GetFiles();

                        if (files.Length == 0)
                            dir.Delete();
                    }
                }

                FileInfo zip = new FileInfo($@"{temp}.zip");

                if (zip.Exists)
                    System.IO.File.Delete(zip.FullName);

                ZipFile.CreateFromDirectory(tempDir.FullName, zip.FullName);
                saved = zip.FullName;

                tempDir.Refresh();
                if (tempDir.Exists)
                    tempDir.Delete(true);

            }

            return saved;
        }

        [AllowAnonymous]
        public void GetAttachmentById(int AttachmentID)
        {
            var file = CustomerPortal.Attachments.SingleOrDefault(p => p.AttachmentID == AttachmentID);
            string filename = file?.FileLocation ?? "";
            if (!string.IsNullOrEmpty(filename))
            {
                FileInfo info = new FileInfo(filename);
                if (info.Exists)
                {
                    var credentials = VerifyCredentials();
                    string logType = "Get Attachment";
                    var attachment = new { file.AttachmentID, file.Description, file.Category, file.OrderNum, info.FullName, file.AttachedBy, file.FileName, credentials.Email, credentials.UserID, credentials.IPAddress };
                    CustomerPortal.Logs.Add(new Models.Log
                    {
                        LogDateTime = DateTime.UtcNow,
                        LogType = logType,
                        UserID = credentials.UserID,
                        LogText = JsonConvert.SerializeObject(attachment)
                    });
                    CustomerPortal.SaveChanges();

                    Logger.LogIt(logType, attachment);

                    byte[] result = PhotoHandler.FormatImage(info.FullName, false);
                    Response.ContentType = "image/jpeg";
                    Response.Body.Write(result, 0, result.Length);
                    Response.Body.Flush();
                    Response.Body.Close();
                }
                //FileInfo file = new FileInfo($"{directory.Name}{photo}");
            }
        }
                
        public IActionResult GetSurveyResponses(DateTime? startDate, DateTime? endDate)
        {
            var credentials = VerifyCredentials();

            Stopwatch timer = new Stopwatch();
            timer.Start();
            var surveys = new List<SurveyComplete>();
            
            if (startDate.HasValue && endDate.HasValue)
            {
                surveys = CustomerPortal.SurveyComplete.Where(p => p.CompleteDate > startDate && p.CompleteDate <= endDate).ToList();
            }
            else { 
                surveys = CustomerPortal.SurveyComplete.ToList();
            }
            
            // loop through all completed surveys to add custom properties:
            foreach (SurveyComplete surveyComplete in surveys) {
                // if an AdminUserID exists get details for the user:
                if (surveyComplete.AdminUserID != null) {
                    User adminUser = CustomerPortal.Users.FirstOrDefault(p => p.UserID == surveyComplete.AdminUserID);
                    if (adminUser != null)
                    {
                        surveyComplete.AdminUserName = $"{adminUser.FirstName} {adminUser.LastName}";
                    }
                }

                // average all "Range" type questions on given survey:
                try
                {
                    surveyComplete.RangeAnswerAverage = 
                        (from answer in CustomerPortal.SurveyAnswer
                            join question in CustomerPortal.SurveyQuestions on answer.QuestionID equals question.QuestionID
                            where answer.CompletionID == surveyComplete.CompletionID
                                && question.SurveyID == surveyComplete.SurveyID
                                && question.QuestionType == "Range"
                            select answer).Average(answer => Convert.ToInt32(answer.Answer));
                }
                catch(Exception ex) {
                    CustomerPortal.Logs.Add(new Models.Log
                    {
                        LogDateTime = DateTime.UtcNow,
                        LogType = "SurveyComplete - Range Averages",
                        UserID = credentials.UserID,
                        LogText = JsonConvert.SerializeObject(new { surveys.Count, timer.Elapsed, credentials.Email, credentials.UserID, credentials.IPAddress, exceptionMessage = ex.Message })
                    });

                    CustomerPortal.SaveChanges();
                }
            }

            if (surveys.Count > 0)
            {
                surveys = surveys.OrderByDescending(p => p.CompleteDate).ToList();
            }

            timer.Stop();
            string logType = "Load Survey Responses";

            CustomerPortal.Logs.Add(new Models.Log
            {
                LogDateTime = DateTime.UtcNow,
                LogType = logType,
                UserID = credentials.UserID,
                LogText = JsonConvert.SerializeObject(new { surveys.Count, timer.Elapsed, credentials.Email, credentials.UserID, credentials.IPAddress })
            });

            CustomerPortal.SaveChanges();

            Logger.LogIt(logType, new { credentials.Email, surveys.Count, timer.Elapsed, credentials.UserID, credentials.IPAddress });

            return Ok(new
            {
                surveys
            });

        }

        public IActionResult GetSurveyResponse(int OrderNum)
        {
            var credentials = VerifyCredentials();

            Stopwatch timer = new Stopwatch();
            timer.Start();

            SurveyComplete completion = CustomerPortal.SurveyComplete.SingleOrDefault(p => p.OrderNum == OrderNum);

            if (completion == null) return BadRequest($"Unable to retrieve survey results for {OrderNum}!<br/>Please try again.");

            int completionID = completion.CompletionID;

            List<SurveyAnswer> answers = CustomerPortal.SurveyAnswer.Where(p => p.CompletionID == completionID).OrderBy(p => p.QuestionID).ThenBy(p => p.SubQuestionID).ToList();

            Surveys survey = CustomerPortal.Surveys.Single(p => p.SurveyID == completion.SurveyID);
            var questions = CustomerPortal.SurveyQuestions.Where(p => p.SurveyID == completion.SurveyID).OrderBy(p => p.QuestionID);

            timer.Stop();
            string logType = "Load Survey Response";

            CustomerPortal.Logs.Add(new Models.Log
            {
                LogDateTime = DateTime.UtcNow,
                LogType = logType,
                UserID = credentials.UserID,
                LogText = JsonConvert.SerializeObject(new { OrderNum, timer.Elapsed, credentials.Email, credentials.UserID, credentials.IPAddress })
            });

            CustomerPortal.SaveChanges();

            Logger.LogIt(logType, new { OrderNum, timer.Elapsed, credentials.Email, credentials.UserID, credentials.IPAddress });

            return Ok(new
            {
                survey,
                questions,
                completion,
                answers
            });
        }


        public IActionResult GetOrderSurveys(DateTime ?startDate, DateTime ?endDate)
        {            
            var dateWhereClause = "";

            Stopwatch timer = new Stopwatch();
            timer.Start();

            if (startDate != null && endDate != null) {
                dateWhereClause = $"AND ISNULL(StartedClosingOrder_c, OrderDate) BETWEEN '{startDate}' AND '{endDate}'";
            }

            var credentials = VerifyCredentials();
            string[] orders = GetOrderList(credentials);

            //if (orders.Length == 0)
            //    return Ok(new { orders = new DataTable() });

            var parameterNames = new List<string>();
            var parameters = new List<SqlParameter>();
            for (int i = 0; i < orders.Length; i++)
            {
                parameterNames.Add($"@order{i}");
                parameters.Add(new SqlParameter(parameterNames[i], orders[i]));
            }

            var filter = Utilities.RetrieveDataTable($@"
                SELECT DISTINCT
                       vwOrderHeaders.OrderNum,
                       vwOrderHeaders.OrderDate,
                       vwOrderHeaders.OkToStartClosingOrder_c,
	                   vwOrderHeaders.StartedClosingOrder_c,
                       ISNULL(StartedClosingOrder_c, OrderDate) as CloseDate,
                       Customer.Name AS SoldTo,
                       OTSName,
                       OTSAddress1,
                       OTSAddress2,
                       OTSAddress3,
                       OTSCity,
                       OTSState,
                       OTSZIP,
                       ISNULL(OTSCountry.Description, '') AS OTSCountry,
                       ProjectManager.Name AS ProjectManagerName,
					   ProjectManager.EmailAddress AS ProjectManagerEmail,
					   SalesRep1.Name AS SalesRep1Name,
					   SalesRep1.EmailAddress AS SalesRep1Email,
                       --CASE UseOTS
                       --    WHEN 1 THEN
                       --        vwOrderHeaders.OTSName
                       --    ELSE
                       --        Customer.Name
                       --END OTSName,
                       --CASE UseOTS
                       --    WHEN 1 THEN
                       --        vwOrderHeaders.OTSAddress1
                       --    ELSE
                       --        Customer.Address1
                       --END OTSAddress1,
                       --CASE UseOTS
                       --    WHEN 1 THEN
                       --        vwOrderHeaders.OTSCity
                       --    ELSE
                       --        Customer.City
                       --END OTSCity,
                       --CASE UseOTS
                       --    WHEN 1 THEN
                       --        vwOrderHeaders.OTSState
                       --    ELSE
                       --        Customer.State
                       --END OTSState,
                       --CASE UseOTS
                       --    WHEN 1 THEN
                       --        vwOrderHeaders.OTSZIP
                       --    ELSE
                       --        Customer.OTSZIP
                       --END OTSZIP,
                       --CASE UseOTS
                       --    WHEN 1 THEN
                       --        vwOrderHeaders.OTSState
                       --    ELSE
                       --        Customer.State
                       --END OTSState,
                       vwOrderHeaders.InstallationComplete_c,
                       vwOrderHeaders.ShippedComplete_c,
                       InstallationOnly_c,
                       vwOrderHeaders.FrazierInstallation_c
                FROM dbo.vwOrderHeaders
                    JOIN Erp.Customer
                        ON Customer.Company = vwOrderHeaders.Company
                           AND Customer.CustNum = vwOrderHeaders.CustNum
                    LEFT JOIN Erp.Country AS OTSCountry
                        ON OTSCountry.Company = vwOrderHeaders.Company
                            AND OTSCountry.CountryNum = vwOrderHeaders.OTSCountryNum
                    --JOIN Erp.Country AS CustCountry
                    --    ON OTSCountry.Company = vwOrderHeaders.Company
                    --        AND OTSCountry.CountryNum = vwOrderHeaders.OTSCountryNum
                    LEFT JOIN dbo.EmpBasic ProjectManager ON ProjectManager_c = ProjectManager.EmpID
						AND ProjectManager.MainCompany_c = 1
						AND ProjectManagement_c = 1					
					LEFT JOIN dbo.EmpBasic SalesRep1 ON vwOrderHeaders.SalesRep1_c = SalesRep1.EmpID
						AND SalesRep1.MainCompany_c = 1
                WHERE (
                          (
                              (
                                  FrazierInstallation_c = 1
                                  AND InstallationComplete_c = 1
                              )
                              OR FrazierInstallation_c = 0
                          )
                          AND
                          (
                              InstallationOnly_c = 1
                              OR ShippedComplete_c = 1
                          )
                      )
                       -- AND OrderNum IN ({(parameterNames.Count > 0 ? string.Join(", ", parameterNames) : "0")})
                        {(credentials.HasFullAccess && !credentials.IsProxyLoggedIn ? string.Empty : $"AND OrderNum IN ({(parameterNames.Count > 0 ? string.Join(",", parameterNames) : "0")})")}
                        {dateWhereClause}
                    ORDER BY 
                        Customer.Name ASC,
                        ISNULL(StartedClosingOrder_c, OrderDate) DESC
                        --vwOrderHeaders.OrderNum;
                ", parameters.ToArray());

            DataTable orderTable = filter.Clone();
            foreach (DataRow order in filter.Rows)
            {
                int orderNum = order.Field<int>("OrderNum");
                if (!CustomerPortal.SurveyComplete.Any(p => p.OrderNum == orderNum))
                    orderTable.Rows.Add(order.ItemArray);
            }

            timer.Stop();

            string logType = "Load Surveys";
            CustomerPortal.Logs.Add(new Models.Log
            {
                LogDateTime = DateTime.UtcNow,
                LogType = logType,
                UserID = credentials.UserID,
                LogText = JsonConvert.SerializeObject(new { orderTable.Rows.Count, timer.Elapsed, credentials.Email, credentials.UserID, credentials.IPAddress })
            });

            CustomerPortal.SaveChanges();

            Logger.LogIt(logType, new { credentials.Email, orderTable.Rows.Count, timer.Elapsed, credentials.UserID, credentials.IPAddress });

            return Ok(new
            {
                orders = orderTable
            });

        }

        [HttpPost]
        public IActionResult SubmitOrderSurvey([FromBody] OrderSurvey survey)
        {
            //TODO: Test Order Surveys thoroughly
            var credentials = VerifyCredentials();

            var record = CustomerPortal.Surveys.Single(p => p.SurveyID == survey.SurveyID);

            if (!record.Locked)
                record.Locked = true;

            string fullNameForEmailResponse = $"{credentials.CurrentUser.FirstName} {credentials.CurrentUser.LastName}";
            string completedBy = credentials.Email;
            int? adminUserId = null;
            User surveyPortalUser = null;            

            // if client sends a value for "SurveyPortalUserId" then an "Admin" user is completing the survey on behalf of the "SurveyPortalUser"
            if (survey.SurveyPortalUserID != null) {
                surveyPortalUser = CustomerPortal.Users.FirstOrDefault(p => p.UserID == survey.SurveyPortalUserID);        
                if (surveyPortalUser != null) {
                    completedBy = surveyPortalUser.EmailAddress;
                    fullNameForEmailResponse = $"{surveyPortalUser.FirstName} {surveyPortalUser.LastName}";
                    adminUserId = credentials.UserID;                    
                }
            }

            CustomerPortal.SurveyComplete.Add(new SurveyComplete
            {
                OrderNum = survey.OrderNum,
                SurveyID = record.SurveyID,
                CompleteDate = DateTime.UtcNow,
                //CompletedBy = credentials.Email
                CompletedBy = completedBy,
                AdminUserID = adminUserId
            });

            CustomerPortal.SaveChanges();

            SurveyComplete completedSurvey = CustomerPortal.SurveyComplete.Single(p => p.OrderNum == survey.OrderNum);

            foreach (var result in survey.Results)
            {
                SurveyQuestions question = CustomerPortal.SurveyQuestions.First(p => p.SurveyID == survey.SurveyID && p.QuestionID == result.QuestionID && p.SubQuestionID == result.SubQuestionID);
                string answer = result.Answer;

                switch (Enum.Parse(typeof(SurveyQuestionType), question.QuestionType))
                {
                    case SurveyQuestionType.Boolean:
                        if (string.IsNullOrWhiteSpace(result.Answer))
                            answer = false.ToString().ToLower();
                        break;
                }

                CustomerPortal.SurveyAnswer.Add(new SurveyAnswer
                {
                    CompletionID = completedSurvey.CompletionID,
                    QuestionID = result.QuestionID,
                    SubQuestionID = result.SubQuestionID,
                    AnswerValue = result.AnswerValue,
                    AnswerText = result.AnswerText,
                    Answer = answer
                });
                //surveyResults.Add(JsonConvert.DeserializeObject<SurveyAnswer>(JsonConvert.SerializeObject(result, Formatting.None)));
            }

            string logType = "Submit Order Survey";
            CustomerPortal.Logs.Add(new Models.Log
            {
                LogDateTime = DateTime.UtcNow,
                LogType = logType,
                UserID = credentials.UserID,
                LogText = survey.OrderNum.ToString()
            });

            CustomerPortal.SaveChanges();

            Logger.LogIt(logType, new { survey.OrderNum, credentials.Email, credentials.UserID, credentials.IPAddress });

            try
            {
                Reports reports = new Reports(CustomerPortal, credentials);

                reports.SaveSurveyPdf(completedSurvey.CompletionID);
            }
            catch(Exception ex)
            {
                Logger.LogException(ex);
            }

            string subject_old = "Survey Completed";
            string body_old = $@"Survey has been completed for order # {survey.OrderNum}.<br/><br/>
                    Please review this at your earliest convenience.";
            var parameters = new List<SqlParameter>();


            var orderInfo = Utilities.RetrieveDataTable($@"
                        SELECT TOP 1 OTSName, OTSCity, OTSState
                        FROM dbo.vwOrderHeaders
                        WHERE OrderNum = {survey.OrderNum}", parameters.ToArray());            

            if (orderInfo.Rows.Count == 1)
            {
                string subject = "Survey Completed";

                // check if OTSName already has a "city, state" in the name (if so do not add it again to email template):
                string cityState = $"{orderInfo.Rows[0]["OTSCity"].ToString().Replace(" ", "")}{orderInfo.Rows[0]["OTSState"]}";
                string name = orderInfo.Rows[0]["OTSName"].ToString().Replace(" ", "").Replace(",", "");
                string nameCityState = $"{orderInfo.Rows[0]["OTSName"]} in {orderInfo.Rows[0]["OTSCity"]}, {orderInfo.Rows[0]["OTSState"]}";
                if (name.Contains(cityState)) {
                    nameCityState = orderInfo.Rows[0]["OTSName"].ToString();
                }
                
                string body = $@"Thank you, {fullNameForEmailResponse} for completing the survey regarding: {nameCityState}. We are grateful for your feedback and your responses are invaluable in our pursuit of excellence.<br/><br/><br/> 
                    Thank you once again,<br/>
                    Frazier Customer Service<br/>
                    <a href=""mailto:customerservice@frazier.com"">customerservice@frazier.com</a><br/>
                    800-859-1342, Option #8";


                //EmailMessage surveyMsg = new EmailMessage(new List<Recipient>() {  EmailMessage.MktgEmail, EmailMessage.CSEmail }, subject, body);
                Recipient customerEmail = new Recipient(completedBy, fullNameForEmailResponse);
                EmailMessage surveyMsg = new EmailMessage(
                    to: new List<Recipient>() { customerEmail }, 
                    subject: subject, 
                    body: body, 
                    from: EmailMessage.CSEmail, 
                    cc: null,
                    bcc: new List<Recipient>() { EmailMessage.MktgEmail, EmailMessage.CSEmail }
                    );

                surveyMsg.SendEmail();
            }

            return Ok(new
            {
                survey.OrderNum
            });
        }

        public IActionResult IsSurveyComplete(int OrderNum)
        {
            bool isComplete = CustomerPortal.SurveyComplete.Any(p => p.OrderNum == OrderNum);
            return Ok(new
            {
                isComplete
            });
        }

        [AllowAnonymous]
        public void GetEmployeePhoto(string photo, bool Resize = false)
        {
            //string filename = $@"{Constants.Directories.EpiPhotos}{photo}.jpg"; // + photo.Replace(".jpg", ".bmp");
            string filename = $@"{Constants.Directories.EmpPhotos}{photo}.bmp";
            if (!System.IO.File.Exists(filename) && System.IO.File.Exists(filename.Replace("FUS", "FCAN")))
            {
                filename = filename.Replace("FUS", "FCAN");
            }
            else if (!System.IO.File.Exists(filename))
            {
                Random r = new Random();
                filename = $@"{Constants.Directories.EpiPhotos}avatar-default.png";
            }

            byte[] result = PhotoHandler.FormatImage(filename);
            Response.ContentType = "image/jpeg";
            Response.Body.Write(result, 0, result.Length);
            Response.Body.Flush();
            Response.Body.Close();
        }

        public IActionResult GetSearchResults(string searchText)
        {
            var credentials = VerifyCredentials();


            Stopwatch timer = new Stopwatch();
            timer.Start();
            string[] orders = GetOrderList(credentials);

            //if (!credentials.HasFullAccess && orders.Length == 0)
            //{
            //    timer.Stop();
            //    return Ok();
            //}


            var parameterNames = new List<string>();
            var parameters = new List<SqlParameter>();

            //if (!credentials.HasFullAccess)
            //{
            for (int i = 0; i < orders.Length; i++)
            {
                parameterNames.Add($"@order{i}");
                parameters.Add(new SqlParameter(parameterNames[i], orders[i]));
            }
            //}

            SqlParameter searchParam = new SqlParameter("@searchText", searchText ?? string.Empty);
            parameters.Add(searchParam);

            var orderData = Utilities.RetrieveDataTable($@"
                   SELECT DISTINCT TOP (2000)
                            OrderHed.Company,
                            OrderNum,
                            OTSName,
                            OTSAddress1,
                            OTSCity,
                            OTSState,
                            OrderDate,
                            Customer.Name AS SoldTo,
                            PONum
                    FROM dbo.OrderHed
                        JOIN Erp.Customer
                            ON Customer.Company = OrderHed.Company
                                AND Customer.CustNum = OrderHed.CustNum
                    WHERE ICPONum = 0
                            {(credentials.HasFullAccess && !credentials.IsProxyLoggedIn ? string.Empty : $"AND OrderNum IN ({(parameterNames.Count > 0 ? string.Join(",", parameterNames) : "0")})")}
                            AND
                            (
                                OrderHed.Company LIKE CONCAT('%', @searchText, '%')
                                OR OrderNum LIKE CONCAT('%', @searchText, '%')
                                OR OTSName LIKE CONCAT('%', @searchText, '%')
                                OR OTSAddress1 LIKE CONCAT('%', @searchText, '%')
                                OR OTSCity LIKE CONCAT('%', @searchText, '%')
                                OR OTSState LIKE CONCAT('%', @searchText, '%')
                                OR OrderDate LIKE CONCAT('%', @searchText, '%')
                                OR Customer.Name LIKE CONCAT('%', @searchText, '%')
                                OR PONum LIKE CONCAT('%', @searchText, '%')
                            )
                    ORDER BY OrderDate DESC", parameters.ToArray());

            var contactData = GetContacts(orders.ToList(), true, searchText, credentials.HasFullAccess);

            timer.Stop();

            string logType = "Load Search";
            CustomerPortal.Logs.Add(new Models.Log
            {
                LogDateTime = DateTime.UtcNow,
                LogType = logType,
                UserID = credentials.UserID,
                LogText = JsonConvert.SerializeObject(new { searchText, OrderCount = orderData.Rows.Count, ContactCount = contactData.Rows.Count, timer.Elapsed, credentials.Email, credentials.UserID, credentials.IPAddress })
            });

            CustomerPortal.SaveChanges();

            Logger.LogIt(logType, new { credentials.Email, searchText, OrderCount = orderData.Rows.Count, ContactCount = contactData.Rows.Count, timer.Elapsed, credentials.UserID, credentials.IPAddress });

            return Ok(new
            {
                orderData,
                contactData
            });
        }

        [Obsolete]
        private List<string> GetListOfOrders(User user, User parent)
        {
            throw new NotImplementedException("This is method is deprecated due to added logic.");
            //if (parent != null)
            //{
            //    return _salesforceContext.vwContactOrders.Where(p => p.Email.ToUpper() == user.EmailAddress.ToUpper()).Select(p => p.Order_Number__c).Distinct().ToList();
            //}
            //else
            //{
            //    if (!user.Master)
            //        return _salesforceContext.vwEmailOrderLinks.Where(p => p.Email.ToUpper() == user.EmailAddress.ToUpper()).Select(p => p.Order_Number__c).Distinct().ToList();
            //    else
            //        return _salesforceContext.vwAccountOrders.Where(p => p.Email.ToUpper() == user.EmailAddress.ToUpper()).Select(p => p.Order_Number__c).Distinct().ToList();
            //}
        }

        private string[] GetOrderList(UserCredentials credentials, bool ignoreProxy = false)
        {
            List<string> orders = new List<string>();

            User user = (credentials.IsProxyLoggedIn && !ignoreProxy ? credentials.ProxyUser : credentials.CurrentUser);
            Stopwatch timer = new Stopwatch();
            timer.Start();
            if (user != null)
            {
                //DataExport export = new DataExport(_configuration, Constants.Connections.Salesforce);
                //List<SqlParameter> parameters;
                //string spName = "spAccountOrders";
                //string tableName = "Orders";
                //string columnName = "Order_Number__c";

                if ((user.ParentID ?? 0) > 0)
                {
                    User parent = CustomerPortal.Users.SingleOrDefault(p => p.UserID == user.ParentID && p.Verified && p.Master);
                    if (parent != null)
                    {
                        if (user.LimitedAccess)
                        {
                            string[] list = JsonConvert.DeserializeObject<string[]>(user.OrderList);

                            // TODO: Limit 2000 records
                            //parameters = new List<SqlParameter>() { new SqlParameter("@Email", parent.EmailAddress) };
                            //var result = export.QueryToTable(spName, tableName, parameters.ToArray(), true);
                            //orders = result.Rows.Cast<DataRow>().Where(p => list.Contains(p[columnName].ToString())).Select(row => row[columnName].ToString()).ToList();

                            //orders = _salesforceContext.vwAccountOrders.Where(p => p.Email.ToUpper() == parent.EmailAddress.ToUpper() && list.Contains(p.Order_Number__c)).Select(p => p.Order_Number__c).Distinct().ToList();
                            orders = CustomerPortal.AccountOrders.Where(p => (p.UserID == parent.UserID && list.Contains(p.OrderNum.ToString())) || p.UserID == user.UserID).Select(p => p.OrderNum.ToString()).Distinct().ToList();
                        }
                        else
                        {
                            //parameters = new List<SqlParameter>() { new SqlParameter("@Email", parent.EmailAddress) };
                            //var result = export.QueryToTable(spName, tableName, parameters.ToArray(), true);
                            //orders = result.Rows.Cast<DataRow>().Select(row => row[columnName].ToString()).ToList();

                            //orders = _salesforceContext.vwAccountOrders.Where(p => p.Email.ToUpper() == parent.EmailAddress.ToUpper()).Select(p => p.Order_Number__c).Distinct().ToList();
                            orders = CustomerPortal.AccountOrders.Where(p => p.UserID == parent.UserID || p.UserID == user.UserID).Select(p => p.OrderNum.ToString()).Distinct().ToList();
                        }
                    }

                    // DO NOT SHOW ANYTHING IF CHILD ACCOUNT IS NOT SET UP TO A MASTER ACCOUNT
                    //else
                    //{
                    //    //orders = _salesforceContext.vwContactOrders.Where(p => p.Email.ToUpper() == user.EmailAddress.ToUpper()).Select(p => p.Order_Number__c).Distinct().ToList();
                    //    orders = _customerPortal.Orders.Where(p => p.UserID == user.UserID).Select(p => p.OrderNum.ToString()).ToList();
                    //}
                }                
                else
                {
                    //orders = _salesforceContext.vwAccountOrders.Where(p => p.Email.ToUpper() == user.EmailAddress.ToUpper()).Select(p => p.Order_Number__c).Distinct().ToList();

                    //parameters = new List<SqlParameter>() { new SqlParameter("@Email", user.EmailAddress) };
                    //var result = export.QueryToTable(spName, tableName, parameters.ToArray(), true);
                    //orders = result.Rows.Cast<DataRow>().Select(row => row[columnName].ToString()).ToList();

                    orders = CustomerPortal.AccountOrders.Where(p => p.UserID == user.UserID).Select(p => p.OrderNum.ToString()).ToList();
                }

                // DO NOT SHOW ANYTHING IF USER IS NOT SET UP AS A MASTER OR CHILD ACCOUNT
                //else
                //{
                //    //orders = _salesforceContext.vwEmailOrderLinks.Where(p => p.Email.ToUpper() == user.EmailAddress.ToUpper()).Select(p => p.Order_Number__c).Distinct().ToList();
                //    orders = _customerPortal.Orders.Where(p => p.UserID == user.UserID).Select(p => p.OrderNum.ToString()).ToList();
                //}
            }
            else
            {
                if (credentials.IsEmployee)
                {
                    //int[] full_access = new int[] { 100122 };

                    //if (!full_access.Contains(credentials.EmpID))
                    //{
                    SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@ID", credentials.EmpID.ToString()) };

                    var employee = Utilities.RetrieveDataTable($@"
                        SELECT OrderNum
                        FROM dbo.vwOrderHeaders
                        WHERE ICPONum = 0
                              AND OrderCancelled_c = 0
                              AND
                              (
                                  (
                                      ProjectManager_c = @ID
                                      OR PMSupportRep_c = @ID
                                      OR ProjectCoordinator_c = @ID
                                      OR PurchasingAgentID_c = @ID
                                      OR ARRep_c = @ID
                                      OR InstallationManager_c = @ID
                                      OR SiteSupervisor1_c = @ID
                                      OR SiteSupervisor2_c = @ID
                                      OR SiteSupervisor3_c = @ID
                                      OR Detailer1_c = @ID
                                      OR Detailer2_c = @ID
                                      OR Detailer3_c = @ID
                                      OR Engineer_c = @ID
                                      OR Engineer2_c = @ID
                                      OR EngineeringTeamLead_c = @ID
                                      OR CustomerServiceRep_c = @ID
                                      OR SalesRepList LIKE CONCAT('%', @ID, '%')
                                      OR OtherAssignedPersonnel1_c = @ID
                                      OR OtherAssignedPersonnel2_c = @ID
                                      OR OtherAssignedPersonnel3_c = @ID
                                  )
                                  OR
                                  (
                                      @ID = '100024'
                                      AND Company = 'FCAN'
                                  )
                              );
                    ", parameters);

                    foreach (DataRow row in employee.Rows)
                        orders.Add(row["OrderNum"].ToString());
                    //}
                    //else
                    //    orders.Add("ALL");
                }
            }

            timer.Stop();
            Logger.LogIt("Get Order List", new { credentials.UserID, credentials.Email, orders.Count, timer.Elapsed });

            return orders.ToArray();
        }        

        //TODO: Include DomainSync database
        private DataTable GetContacts(List<string> orders, bool isSearch = false, string searchText = "", bool fullAccess = false)
        {

            if (orders.Count == 0 && !fullAccess)
            {
                return new DataTable();
            }

            var parameterNames = new List<string>();
            var parameters = new List<SqlParameter>();

            if (!fullAccess)
            {
                for (int i = 0; i < orders.Count; i++)
                {
                    parameterNames.Add($"@order{i}");
                    parameters.Add(new SqlParameter(parameterNames[i], orders[i]));
                }
            }

            if (isSearch)
            {
                parameters.Add(new SqlParameter("@searchText", searchText ?? string.Empty));
            }

            var data = Utilities.RetrieveDataTable($@"
                SELECT DISTINCT
                       Contacts.ID,
                       Contacts.Name,
                       Contacts.Email,
                       Contacts.Title,
                       Contacts.Phone,
                       Contacts.Image,
                       Contacts.Dept
                FROM
                (
                    SELECT OrderNum,
                           CASE
                               WHEN PM.EmpStatus = 'A' THEN
                                   PM.EmpID
                               ELSE
                                   PG.EmpID
                           END AS ID,
                           CASE
                               WHEN PM.EmpStatus = 'A' THEN
                                   PM.Name
                               ELSE
                                   PG.Name
                           END AS Name,
                           CASE
                               WHEN PM.EmpStatus = 'A' THEN
                                   PM.EMailAddress
                               ELSE
                                   PG.EMailAddress
                           END AS Email,
                           CASE
                               WHEN PM.EmpStatus = 'A' THEN
                                   PM.Title_c
                               ELSE
                                   PG.Title_c
                           END AS Title,
                           ISNULL(   NULLIF(CASE
                                                WHEN PM.EmpStatus = 'A' THEN
                                                    PM.Phone
                                                ELSE
                                                    PG.Phone
                                            END, ''),
                                     '908-876-3001'
                                 ) AS Phone,
                           NULLIF(CONCAT(   '{Constants.Directories.EmpPhotos}',
                                            CASE
                                                WHEN PM.EmpStatus = 'A' THEN
                                                    Image.ImageID
                                                ELSE
                                                    PG_Image.ImageID
                                            END,
                                            CASE
                                                WHEN PM.EmpStatus = 'A' THEN
                                                    Image.FileType
                                                ELSE
                                                    PG_Image.FileType
                                            END
                                        ), '{Constants.Directories.EmpPhotos}') Image,
                           'PM' Dept
                    FROM dbo.vwOrderHeaders
                        JOIN dbo.EmpBasic PM
                            ON PM.MainCompany_c = 1
                               AND ProjectManager_c = PM.EmpID
                               AND ProjectManagement_c = 1
                        JOIN dbo.EmpBasic PG
                            ON PG.MainCompany_c = 1
                               AND '100137' = PG.EmpID
                        LEFT JOIN Erp.Image
                            ON PM.EmpID = Image.ImageID
                        LEFT JOIN Erp.Image PG_Image
                            ON PG.EmpID = PG_Image.ImageID
                    UNION
                    SELECT OrderNum,
                           CASE
                               WHEN ISNULL(SalesRep.InActive, 1) = 0 THEN
                                   SalesRep.SalesRepCode
                               ELSE
                                   ISNULL(ReportsTo.SalesRepCode, DI.SalesRepCode)
                           END AS ID,
                           CASE
                               WHEN ISNULL(SalesRep.InActive, 1) = 0 THEN
                                   SalesRep.Name
                               ELSE
                                   ISNULL(ReportsTo.Name, DI.Name)
                           END AS Name,
                           CASE
                               WHEN ISNULL(SalesRep.InActive, 1) = 0 THEN
                                   SalesRep.EMailAddress
                               ELSE
                                   ISNULL(ReportsTo.EMailAddress, DI.EMailAddress)
                           END AS Email,
                           CASE
                               WHEN ISNULL(SalesRep.InActive, 1) = 0 THEN
                                   SalesRep.SalesRepTitle
                               ELSE
                                   ISNULL(ReportsTo.SalesRepTitle, DI.SalesRepTitle)
                           END AS Title,
                           ISNULL(   NULLIF(CASE
                                                WHEN ISNULL(SalesRep.InActive, 1) = 0 THEN
                                                    SalesRep.OfficePhoneNum
                                                ELSE
                                                    ISNULL(ReportsTo.OfficePhoneNum, DI.OfficePhoneNum)
                                            END, ''),
                                     '908-876-3001'
                                 ) AS Phone,
                           NULLIF(CONCAT(
                                            '{Constants.Directories.EmpPhotos}',
                                            CASE
                                                WHEN SalesRep.InActive = 0 THEN
                                                    Image.ImageID
                                                ELSE
                                                    ISNULL(ReportsTo_Image.ImageID, DI_Image.ImageID)
                                            END,
                                            CASE
                                                WHEN SalesRep.InActive = 0 THEN
                                                    Image.FileType
                                                ELSE
                                                    ISNULL(ReportsTo_Image.FileType, DI_Image.FileType)
                                            END
                                        ), '{Constants.Directories.EmpPhotos}') Image,
                           'Sales' Dept
                    FROM dbo.vwOrderHeaders
                        JOIN dbo.EmpBasic Sales
                            ON Sales.MainCompany_c = 1
                               AND Sales.EmpID IN ( SalesRep1_c, SalesRep2_c, SalesRep3_c )
                        JOIN dbo.SalesRep
                            ON SalesRep.Company = Sales.Company
                               AND Sales.EmpID = SalesRep.SalesRepCode
                        --ON SalesRep.SalesRepCode IN ( SalesRep1_c ) --, SalesRep2_c, SalesRep3_c )
                        LEFT JOIN dbo.SalesRep ReportsTo
                            ON ReportsTo.SalesRepCode = SalesRep.RepReportsTo
                               AND ReportsTo.InActive = 0
                        JOIN dbo.SalesRep DI
                            ON DI.Company = vwOrderHeaders.Company
                               AND '100039' = DI.SalesRepCode
                        LEFT JOIN Erp.Image
                            ON SalesRep.SalesRepCode = Image.ImageID
                        LEFT JOIN Erp.Image ReportsTo_Image
                            ON ReportsTo.SalesRepCode = ReportsTo_Image.ImageID
                        LEFT JOIN Erp.Image DI_Image
                            ON DI.SalesRepCode = DI_Image.ImageID
                    UNION
                    SELECT OrderNum,
                           CASE
                               WHEN CS.EmpStatus = 'A' THEN
                                   CS.EmpID
                               ELSE
                                   CS_Manager.EmpID
                           END AS ID,
                           CASE
                               WHEN CS.EmpStatus = 'A' THEN
                                   CS.Name
                               ELSE
                                   CS_Manager.Name
                           END AS Name,
                           CASE
                               WHEN CS.EmpStatus = 'A' THEN
                                   CS.EMailAddress
                               ELSE
                                   CS_Manager.EMailAddress
                           END AS Email,
                           CASE
                               WHEN CS.EmpStatus = 'A' THEN
                                   CS.Title_c
                               ELSE
                                   CS_Manager.Title_c
                           END AS Title,
                           ISNULL(   NULLIF(CASE
                                                WHEN CS.EmpStatus = 'A' THEN
                                                    CS.Phone
                                                ELSE
                                                    CS_Manager.Phone
                                            END, ''),
                                     '908-876-3001'
                                 ) AS Phone,
                           NULLIF(CONCAT(   '{Constants.Directories.EmpPhotos}',
                                            CASE
                                                WHEN CS.EmpStatus = 'A' THEN
                                                    Image.ImageID
                                                ELSE
                                                    DD_Image.ImageID
                                            END,
                                            CASE
                                                WHEN CS.EmpStatus = 'A' THEN
                                                    Image.FileType
                                                ELSE
                                                    DD_Image.FileType
                                            END
                                        ), '{Constants.Directories.EmpPhotos}') Image,
                           'CS' Dept
                    FROM dbo.vwOrderHeaders
                        JOIN dbo.EmpBasic CS
                            ON CS.MainCompany_c = 1
                               AND
                               (
                                   CS.EmpID IN (CustomerServiceRep_c, '100122', '100041', '100125')
                               ) --REQUEST TO ADD DIANE TO ALL CONTACT PAGES
                               AND CustomerServiceAgent_c = 1
                        JOIN dbo.EmpBasic CS_Manager
                            ON CS_Manager.MainCompany_c = 1
                               AND '100122' = CS_Manager.EmpID
                        LEFT JOIN Erp.Image
                            ON CS.EmpID = Image.ImageID
                        LEFT JOIN Erp.Image DD_Image
                            ON CS_Manager.EmpID = DD_Image.ImageID
                    UNION
                    SELECT OrderNum,
                           CASE
                               WHEN AR.EmpStatus = 'A' THEN
                                   AR.EmpID
                               ELSE
                                   AR_Manager.EmpID
                           END AS ID,
                           CASE
                               WHEN AR.EmpStatus = 'A' THEN
                                   AR.Name
                               ELSE
                                   AR_Manager.Name
                           END AS Name,
                           --CASE
                           --    WHEN AR.EmpStatus = 'A' THEN
                           --        AR.EMailAddress
                           --    ELSE
                           --        AR_Manager.EMailAddress
                           --END AS Email,
                           'ACCTSREC@frazier.com' AS Email,
                           CASE
                               WHEN AR.EmpStatus = 'A' THEN
                                   AR.Title_c
                               ELSE
                                   AR_Manager.Title_c
                           END AS Title,
                           ISNULL(   NULLIF(CASE
                                                WHEN AR.EmpStatus = 'A' THEN
                                                    AR.Phone
                                                ELSE
                                                    AR_Manager.Phone
                                            END, ''),
                                     '908-876-3001'
                                 ) AS Phone,
                           NULLIF(CONCAT(   '{Constants.Directories.EmpPhotos}',
                                            CASE
                                                WHEN AR.EmpStatus = 'A' THEN
                                                    Image.ImageID
                                                ELSE
                                                    BS_Image.ImageID
                                            END,
                                            CASE
                                                WHEN AR.EmpStatus = 'A' THEN
                                                    Image.FileType
                                                ELSE
                                                    BS_Image.FileType
                                            END
                                        ), '{Constants.Directories.EmpPhotos}') Image,
                           'AR' Dept
                    FROM dbo.vwOrderHeaders
                        JOIN dbo.EmpBasic AR
                            ON AR.MainCompany_c = 1
                               AND vwOrderHeaders.ARRep_c = AR.EmpID
                        JOIN dbo.EmpBasic AR_Manager
                            ON AR_Manager.MainCompany_c = 1
                               AND '100893' = AR_Manager.EmpID
                        LEFT JOIN Erp.Image
                            ON AR.EmpID = Image.ImageID
                        LEFT JOIN Erp.Image BS_Image
                            ON AR_Manager.EmpID = BS_Image.ImageID
                ) Contacts
                --AND Image.Company = AR.Company
                WHERE
                {(!fullAccess ? $" Contacts.OrderNum IN ( {string.Join(", ", parameterNames)} )  " : string.Empty)} 
                {(!fullAccess && isSearch ? " AND " : string.Empty)} 
                    {(isSearch ? @"(
                        Contacts.Name LIKE CONCAT('%', @searchText, '%')
                        OR Contacts.Email LIKE CONCAT('%', @searchText, '%')
                        OR Contacts.Title LIKE CONCAT('%', @searchText, '%')
                        OR ISNULL(NULLIF(Contacts.Phone, ''), '908-876-3001') LIKE CONCAT('%', @searchText, '%')
                    )" : "")}
                ORDER BY Contacts.ID;", parameters.ToArray());

            return data;
        }

        /// <summary>
        /// Gets the list of all (non-employee) Customer Portal users who have access to an order
        /// </summary>    
        /// <returns>List of Users</returns>
        public IActionResult GetUsersForOrder(int orderNum)
        {
            // get all users in the AccountOrders table for this order:
            List<User> users = (from accountOrders in CustomerPortal.AccountOrders
                                join accountUsers in CustomerPortal.Users
                                on accountOrders.UserID equals accountUsers.UserID
                                where accountOrders.OrderNum == orderNum
                                select accountUsers).ToList<User>();

            // get any child users of the found users in AccountOrders (unless they have LimitedAccess):
            List<int> parentUserIds = users.Select(x => x.UserID).ToList();
            List<User> childUsers = CustomerPortal.Users.Where(x => 
                x.ParentID != null && 
                x.LimitedAccess == false && 
                parentUserIds.Contains((int)x.ParentID)).ToList();
            
            users = users.Concat(childUsers).ToList();


            // get all users with LimitedAccess who have this order in their OrderList array:            
            List<User> limitedAccessUsers = CustomerPortal.Users.Where(user => user.LimitedAccess == true).ToList();                                        
            foreach (User user in limitedAccessUsers) {
                string[] limitedOrderList = JsonConvert.DeserializeObject<string[]>(user.OrderList);
                
                if (limitedOrderList.Contains(orderNum.ToString())) { 
                    users.Add(user);
                }
            } 
            
            users = users.Where(user => user.Verified == true).ToList();

            return Ok(new 
            {
                users
            });
        }
    }    

    public class OrderSurvey
    {
        public int SurveyID { get; set; }
        public int OrderNum { get; set; }
        public int? SurveyPortalUserID { get; set; }
        public SurveyAnswer[] Results { get; set; }
    }

    public class DeptData
    {
        public DataTable PMData { get; set; }
        public DataTable SalesData { get; set; }
        public DataTable ARData { get; set; }
        public DataTable CSData { get; set; }
    }

    public class CustomerPortalScheduleEvent
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string start => startDate.ToString("yyyy-MM-dd");
        public string end => endDate?.ToString("yyyy-MM-dd");
        public string color { get; set; }
        public string fontColor => (Utilities.PerceivedBrightness(System.Drawing.ColorTranslator.FromHtml(color)) > 130 ? ColorTranslator.ToHtml(Color.Black) : ColorTranslator.ToHtml(Color.White));
        public DateTime startDate { get; set; }
        public DateTime? endDate { get; set; }
        public bool editable { get; set; } = false;
        public string addedBy { get; set; }
        public Guid ganttId => Guid.NewGuid();

    }
}