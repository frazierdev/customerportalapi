﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerPortal.Data;
using CustomerPortal.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;
using System.Net;
using Newtonsoft.Json;

namespace CustomerPortal.Controllers
{
    //[ApiController]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    //public abstract class BaseController : Controller // where T: BaseController<T>
    public class BaseController : Controller
    {
        protected readonly IConfiguration Configuration;
        protected readonly CustomerPortalContext CustomerPortal;
        protected readonly SalesforceContext Salesforce;
        protected readonly Utilities Utilities;

        public BaseController(IConfiguration configuration, CustomerPortalContext cpContext, SalesforceContext sContext, Utilities utilities)
        {
            Configuration = configuration;
            CustomerPortal = cpContext;
            Salesforce = sContext;
            Utilities = utilities;
        }

        //public CustomerPortalContext CustomerPortal
        //{
        //    get
        //    {
        //        if (_customerPortal == null)
        //        {
        //            _customerPortal = new CustomerPortalContext();
        //        }
        //        return _customerPortal;
        //    }
        //}

        //public SalesforceContext Salesforce
        //{
        //    get
        //    {
        //        if (_salesforce == null)
        //        {
        //            _salesforce = new SalesforceContext();
        //        }
        //        return _salesforce;
        //    }
        //}


        /// <summary>
        /// Get User Credentials
        /// </summary>
        /// <returns>Current User record logged in</returns>
        protected UserCredentials VerifyCredentials()
        {
            //var e = (from emp in CustomerPortal.Users
            //         join token in MES.AuthenticationTokens on emp.EmployeeID equals token.EmployeeID
            //         where token.TokenID == Token
            //         select emp).FirstOrDefault();

            //if (e == null)
            //    throw new Exception("Token Invalid");

            //CurrentUser = e;

            //Logging.SetUser(e);

            string email = User.Identity.Name;
            string proxy = "";
            int empId = 0;
            bool hasMasterPassword = false;

            var identity = User.Identity as ClaimsIdentity;
            IEnumerable<Claim> claims = identity.Claims;

            if (claims.Any(p => p.Type == ClaimTypes.Actor))
            {
                proxy = claims.Single(p => p.Type == ClaimTypes.Actor).Value;
            }

            if (claims.Any(p => p.Type.ToUpper() == Constants.ClaimType.EMP_ID.ToUpper()))
            {
                string employeeId = claims.Single(p => p.Type.ToUpper() == Constants.ClaimType.EMP_ID.ToUpper()).Value;
                int.TryParse(employeeId, out empId);
            }

            if (claims.Any(p => p.Type.ToUpper() == Constants.ClaimType.HAS_MASTER_PASSWORD.ToUpper()))
            {
                string masterPassword = claims.Single(p => p.Type.ToUpper() == Constants.ClaimType.HAS_MASTER_PASSWORD.ToUpper()).Value;
                bool.TryParse(masterPassword, out hasMasterPassword);
            }

            //if (claims.Any(p => p.Type.ToUpper() == "USERID"))
            //{
            //    string id = claims.Single(p => p.Type.ToUpper() == "USERID").Value;
            //}

            string ip = GetIPAddress(out bool useDNS);

            //var test = HttpContext.Features.Get<IServerVariablesFeature>();
            //Context.Request.GetHttpContext().Request.ServerVariables["REMOTE_ADDR"];
            //Context.Request.Headers["X-Forwarded-For"];

            Logger.LogIt("Get IP Address", new { email, IPAddress = ip, useDNS });

            var user = new UserCredentials(CustomerPortal, email, proxy, empId, ip, hasMasterPassword);

            return user;
        }

        protected string GetIPAddress(out bool UseDNS)
        {
            UseDNS = false;
            string ip = "";

            if (Response != null)
                ip = Response.HttpContext.Connection.RemoteIpAddress.ToString();

            if (ip == "::1")
            {
                string hostName = Dns.GetHostName();
                var addressList = Dns.GetHostEntry(hostName).AddressList;
                ip = addressList.Length > 1 ? addressList[1].ToString() : (addressList.Length == 1 ? addressList[0].ToString() : "");
                UseDNS = true;
            }

            return ip;
        }

        //public IConfiguration Configuration
        //{
        //    get
        //    {
        //        return _configuration;
        //    }
        //}

        public void SaveField(object DatabaseObject, string FieldName, string Value) => Utilities.UpdateField(DatabaseObject, FieldName, Value, CustomerPortal);

        public void SaveFieldChanges<T>(T UserObject, string FieldName, string Value, int ChangedByUserID = 0)
        {
            object oldVal = null;
            string objectName = string.Empty;
            Type t = typeof(T);
            System.Reflection.PropertyInfo info = null;

            if (ChangedByUserID > 0)
            {
                info = t.GetProperty(FieldName);
                oldVal = info.GetValue(UserObject);
                objectName = t.Name;
            }

            SaveField(UserObject, FieldName, Value);

            if (ChangedByUserID > 0)
            {
                string logType = $"Update {objectName}";
                object newVal = info.GetValue(UserObject);

                Dictionary<string, object> fields = new Dictionary<string, object>();

                if (t == typeof(User))
                    fields.Add("UserID", ((User)Convert.ChangeType(UserObject, t)).UserID);

                fields.Add("Name", FieldName);
                fields.Add("oldVal", oldVal);
                fields.Add("newVal", newVal);


                CustomerPortal.Logs.Add(new Models.Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = logType,
                    UserID = ChangedByUserID,
                    LogText = JsonConvert.SerializeObject(fields)
                });

                Logger.LogIt(logType, fields);

                CustomerPortal.SaveChanges();
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (CustomerPortal != null)
            {
                //_CustomerPortal.Connection.Close();
                CustomerPortal.Dispose();
            }

            if (Salesforce != null)
            {
                Salesforce.Dispose();
            }
        }
    }
}
