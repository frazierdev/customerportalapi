﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using CustomerPortal.Data;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using System.Security.Claims;
using System.Net;
using System.Diagnostics;
using CustomerPortal.Models;
using System.Text;
using System.IO.Pipes;
using Spire.Xls;

namespace CustomerPortal.Controllers
{
    [Authorize]
    //[Produces("application/json")]
    //[Route("api/[controller]/[action]")]
    public class ReportController : BaseController
    {
        //private readonly IConfiguration _configuration;
        //private readonly CustomerPortalContext _customerPortal;
        //private readonly Utilities _utilities;

        public ReportController(IConfiguration configuration, CustomerPortalContext context, SalesforceContext sContext, Utilities utilities) : base(configuration, context, sContext, utilities)
        {
            //_configuration = configuration;
            //_customerPortal = context;
            //_utilities = utilities;
        }

        public IActionResult GetReportTypes()
        {
            List<object> list = new List<object>();

            foreach (ReportType type in Enum.GetValues(typeof(ReportType)))
            {
                switch (type)
                {
                    //case ReportType.UserSalesCS:
                    //    continue;
                    default:
                        list.Add(new
                        {
                            value = type.ToString(),
                            text = Reports.GetReportTitle(type),
                            props = new ReportParamsAccessibility(type)
                            // TODO: Add description that provides information about the type of report it is.
                        });
                        break;
                }
            }

            return Ok(list);
        }

        public IActionResult RunReport(ReportParams report)
        {
            var credentials = VerifyCredentials();
            Reports runner = new Reports(CustomerPortal, credentials);

            ReportQuery query = runner.GetReportQuery(report);

            DataExport export = new DataExport(Configuration);

            var result = export.QueryToTable(query.Query, Reports.GetReportTitle(report.Type), query.Parameters);

            return Ok(result);
        }

        public void ExportReport(ReportParams report)
        {
            var credentials = VerifyCredentials();
            Reports runner = new Reports(CustomerPortal, credentials);

            ReportQuery query = runner.GetReportQuery(report);

            DataExport export = new DataExport(Configuration);

            byte[] result = export.QueryToExcel(query.Query, Reports.GetReportTitle(report.Type), query.Parameters);
            
            Response.Body.Write(result, 0, result.Length);
            Response.Body.Flush();
            Response.Body.Close();

        }

        // TODO: Set up SSRS or IronPdf export method
        public void ExportSurveyPdf(int completionId, bool regenerate = false)
        {
            var credentials = VerifyCredentials();

            try
            {
                Reports reports = new Reports(CustomerPortal, credentials);

                // Provided regenerate symbol in case a survey regeneration was needed
                byte[] result = regenerate ? reports.GenerateSurveyPdf(new int[] { completionId }) : reports.RetrieveSurveyPdf(completionId);                

                //FileInfo file = new FileInfo($@"C:\Users\jalmonte\Desktop\survey_completion_{completionId}_{DateTime.Now:MMddyy_HHmmss}.pdf");
                //if (file.Exists)
                //{
                //    file.Delete();
                //    file.Refresh();
                //}

                //System.IO.File.WriteAllBytes(file.FullName, result);

                Response.Body.Write(result, 0, result.Length);
                Response.Body.Flush();
                Response.Body.Close();
            }
            catch (Exception ex)
            {
                CustomerPortal.Logs.Add(new Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = "ReportController.ExportSurveyPdf()",
                    UserID = credentials.UserID,
                    LogText = JsonConvert.SerializeObject(new { ex.Message })
                });

                CustomerPortal.SaveChanges();
            }
        }


        public void ExportSurveysPdf(string completionIdsJSON)
        {
            var credentials = VerifyCredentials();

            try
            {
                int[] completionIds = JsonConvert.DeserializeObject<int[]>(completionIdsJSON);

                Reports reports = new Reports(CustomerPortal, credentials);
                byte[] result = reports.GenerateSurveyPdf(completionIds);

                //FileInfo file = new FileInfo($@"C:\Users\jalmonte\Desktop\survey_completion_{completionId}_{DateTime.Now:MMddyy_HHmmss}.pdf");
                //if (file.Exists)
                //{
                //    file.Delete();
                //    file.Refresh();
                //}

                //System.IO.File.WriteAllBytes(file.FullName, result);

                Response.Body.Write(result, 0, result.Length);
                Response.Body.Flush();
                Response.Body.Close();
            }
            catch (Exception ex)
            {
                CustomerPortal.Logs.Add(new Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = "ReportController.ExportSurveysPdf()",
                    UserID = credentials.UserID,
                    LogText = JsonConvert.SerializeObject(new { ex.Message })
                });

                CustomerPortal.SaveChanges();
            }
        }

        public void ExportToExcel()
        {
            var credentials = VerifyCredentials();

            try
            {
                Stopwatch timer = new Stopwatch();
                timer.Start();

                // get current active survey (in the future we could loop through all survey responses adding a worksheet for each one)
                Surveys activeSurvey = CustomerPortal.Surveys.Where(x => x.Active == true).First();                

                List<SurveyQuestions> surveyQuestions = CustomerPortal.SurveyQuestions.Where(x => x.SurveyID == activeSurvey.SurveyID).OrderBy(x => x.QuestionID).ToList();
                List<SurveyComplete> surveyCompletions = CustomerPortal.SurveyComplete.Where(x => x.SurveyID == activeSurvey.SurveyID).OrderByDescending(x => x.CompleteDate).ToList();

                Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
                workbook.Worksheets.Clear();
                Spire.Xls.Worksheet worksheet = workbook.Worksheets.Add($"{activeSurvey.Name} Survey");
                var questionTypes = new Dictionary<int, string>();               

                // set column headers for static columns:
                int column = 1;                
                worksheet.SetValue(1, column++, "Company");
                worksheet.SetValue(1, column++, "Order");
                worksheet.SetValue(1, column++, "Completed On");
                worksheet.SetValue(1, column++, "Completed By");
                worksheet.SetValue(1, column++, "Admin User");                

                // set column headers for dynamic columns based on survey questions: 
                foreach (SurveyQuestions surveyQuestion in surveyQuestions)
                {
                    worksheet.SetValue(1, column++, surveyQuestion.ExcelHeader);
                    questionTypes.Add(surveyQuestion.QuestionID, surveyQuestion.QuestionType);
                }

                int row = 2;
                foreach (SurveyComplete surveyComplete in surveyCompletions)
                {
                    List<SurveyAnswer> surveyAnswers = CustomerPortal.SurveyAnswer.Where(p => p.CompletionID == surveyComplete.CompletionID).OrderBy(p => p.QuestionID).ThenBy(p => p.SubQuestionID).ToList();
                    User customerUser = CustomerPortal.Users.SingleOrDefault(p => p.EmailAddress == surveyComplete.CompletedBy);
                    User adminUser = CustomerPortal.Users.SingleOrDefault(p => p.UserID == surveyComplete.AdminUserID);
                    column = 1;

                    worksheet.SetValue(row, column++, customerUser.Company);
                    worksheet.SetNumber(row, column++, surveyComplete.OrderNum);
                    worksheet.SetValue(row, column++, surveyComplete.CompleteDate.ToString());
                    worksheet.SetValue(row, column++, $"{customerUser.FirstName} {customerUser.LastName} ({customerUser.EmailAddress})");
                    worksheet.SetValue(row, column++, surveyComplete.AdminUserID != null ? $"{adminUser.FirstName} {adminUser.LastName}" : "-");                    

                    // add answers as columns to the row (these are actually "rows" in the SurveyAnswer table):                    
                    foreach (SurveyAnswer surveyAnswer in surveyAnswers)
                    {                        
                        string answerText = surveyAnswer.Answer ?? string.Empty;
                        string questionType = questionTypes[surveyAnswer.QuestionID].ToLower();

                        switch (questionType) {
                            case "range":                                
                                worksheet.SetNumber(row, column++, Double.Parse(answerText));
                                break;
                            case "boolean":
                                worksheet.SetValue(row, column++, answerText == "true" ? "yes" : answerText == "false" ? "no" : answerText);
                                break;
                            default:
                                worksheet.SetValue(row, column++, answerText.Replace("\r", "").Replace("\n", "").Replace("\"", "").Trim());
                                break;
                        }                        
                    }

                    row++;
                }

                worksheet.AllocatedRange.AutoFitRows();
                worksheet.AllocatedRange.AutoFitColumns();

                // create file for download:
                string fileName = $"survey-responses.xlsx";
                string filePath = $"{Constants.Directories.TempAttachments}{fileName}";
                
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }

                //FileStream fileStream = new FileStream(fileName, FileMode.Create);    
                workbook.SaveToFile(filePath);

                byte[] result = System.IO.File.ReadAllBytes(filePath);

                Response.ContentType = "application/vnd.ms-excel";
                Response.Body.Write(result, 0, result.Length);
                Response.Body.Flush();
                Response.Body.Close();                                

                timer.Stop();

                string logType = "Excel Exported";
                var logObj = new { surveyCompletions.Count, timer.Elapsed, filePath, credentials.IPAddress };

                CustomerPortal.Logs.Add(new Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = logType,
                    UserID = credentials.UserID,
                    LogText = JsonConvert.SerializeObject(logObj)
                });

                Logger.LogIt(logType, logObj);

                CustomerPortal.SaveChanges();

            }
            catch (Exception e)
            {

                CustomerPortal.Logs.Add(new Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = "Error Exporting Excel",
                    UserID = credentials.UserID,
                    LogText = JsonConvert.SerializeObject(new { e.Message })
                });

                CustomerPortal.SaveChanges();
            }
        }


    }
}
