using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Drawing;
using System.IO;
using System;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using CustomerPortal.Data;
using CustomerPortal.Models;
using CustomerPortal.Images;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;

namespace CustomerPortal.Controllers
{
    //[Authorize]
    public class PhotoController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        public PhotoController(IConfiguration configuration, CustomerPortalContext cpContext, SalesforceContext sContext, Utilities utilities) : base(configuration, cpContext, sContext, utilities)
        {
            //_configuration = configuration;
            //_customerPortal = cpContext;
            //_salesforce = sContext;
            //_utilities = utilities;
        }

        public IActionResult GetGalleries()
        {
            DirectoryInfo @base = new DirectoryInfo(Constants.Directories.GalleryPhotos);

            if (@base.Exists)
            {
                var directories = @base.GetDirectories();

                List<FileInfo> files = new List<FileInfo>();

                //foreach (var directory in directories)
                //{
                //    files.AddRange(directory.GetFiles().Where(p => extensions.Contains(p.Extension.ToLower())));
                //}

                return Ok(directories.Select(p => p.Name).ToList());
            }

            return BadRequest();
        }

        public IActionResult GetGalleryPhotos(string gallery)
        {
            if (!string.IsNullOrWhiteSpace(gallery))
            {
                DirectoryInfo directory = new DirectoryInfo(Path.Combine(Constants.Directories.GalleryPhotos, gallery));

                if (directory.Exists)
                {
                    string[] extensions = new string[] { ".jpg", ".jpeg", ".png" };

                    var credentials = VerifyCredentials();
                    string logType = "Retrieve Gallery";
                    CustomerPortal.Logs.Add(new Models.Log
                    {
                        LogDateTime = DateTime.UtcNow,
                        LogType = logType,
                        UserID = credentials.UserID,
                        LogText = JsonConvert.SerializeObject(new { gallery })
                    });
                    CustomerPortal.SaveChanges();

                    List<FileInfo> files = new List<FileInfo>();

                    foreach (var file in directory.GetFiles().Where(p => extensions.Contains(p.Extension.ToLower())))
                    {
                        string filename = Path.Combine(directory.FullName, "_thumbnails", file.Name);

                        if (System.IO.File.Exists(filename))
                        {
                            files.Add(file);
                        }
                    }

                    return Ok(files.Select(p => new
                    {
                        p.Name,
                        DirectoryName = p.Directory.Name,
                        //FullDirectory = p.DirectoryName,
                        p.Length,
                        p.Extension
                    }).ToList());
                }
            }

            return BadRequest();
        }


        //[AllowAnonymous]
        public void GetAttachmentByFile(string directory, string file, bool thumbnail = false, int? userID = null)
        {
            if (thumbnail)
                directory = Path.Combine(directory, "_thumbnails");

            string filename = Path.Combine(Constants.Directories.GalleryPhotos, directory, file);
            if (!string.IsNullOrEmpty(filename))
            {
                FileInfo info = new FileInfo(filename);
                if (info.Exists)
                {
                    var credentials = VerifyCredentials();
                    string logType = "Retrieve Photo";
                    int userId = userID ?? credentials.UserID;
                    var attachment = new { info.FullName, info.Extension, info.Name, info.DirectoryName, info.Length, credentials.Email, UserID = userId, credentials.IPAddress };

                    if (!thumbnail)
                    {
                        CustomerPortal.Logs.Add(new Models.Log
                        {
                            LogDateTime = DateTime.UtcNow,
                            LogType = logType,
                            UserID = userId,
                            LogText = JsonConvert.SerializeObject(attachment)
                        });
                        CustomerPortal.SaveChanges();
                    }

                    Logger.LogIt(logType, attachment);

                    byte[] result = PhotoHandler.FormatImage(info.FullName, false);
                    Response.ContentType = "image/jpeg";
                    Response.Body.Write(result, 0, result.Length);
                    Response.Body.Flush();
                    Response.Body.Close();
                }
                //FileInfo file = new FileInfo($"{directory.Name}{photo}");
            }
        }

        //[AllowAnonymous]
        public void GetAttachmentById(int AttachmentID)
        {
            var file = CustomerPortal.Attachments.SingleOrDefault(p => p.AttachmentID == AttachmentID);
            string filename = file?.FileLocation ?? "";
            if (!string.IsNullOrEmpty(filename))
            {
                FileInfo info = new FileInfo(filename);
                if (info.Exists)
                {
                    var credentials = VerifyCredentials();
                    string logType = "Get Attachment";
                    var attachment = new { file.AttachmentID, file.Description, file.Category, file.OrderNum, info.FullName, file.AttachedBy, file.FileName, credentials.Email, credentials.UserID, credentials.IPAddress };
                    CustomerPortal.Logs.Add(new Models.Log
                    {
                        LogDateTime = DateTime.UtcNow,
                        LogType = logType,
                        UserID = credentials.UserID,
                        LogText = JsonConvert.SerializeObject(attachment)
                    });
                    CustomerPortal.SaveChanges();

                    Logger.LogIt(logType, attachment);

                    byte[] result = PhotoHandler.FormatImage(info.FullName, false);
                    Response.ContentType = "image/jpeg";
                    Response.Body.Write(result, 0, result.Length);
                    Response.Body.Flush();
                    Response.Body.Close();
                }
                //FileInfo file = new FileInfo($"{directory.Name}{photo}");
            }
        }

        public void DownloadAttachment(string directory, string file)
        {
            string filename = Path.Combine(Constants.Directories.GalleryPhotos, directory, file);
            FileInfo info = new FileInfo(filename);
            //return File(info.FullName, System.Net.Mime.MediaTypeNames.Application.Octet, info.Name);

            if (info.Exists)
            {
                var credentials = VerifyCredentials();

                string logType = "Download Attachment";
                var attachment = new { info.FullName, info.Extension, info.Name, info.DirectoryName, info.Length, credentials.Email, credentials.UserID, credentials.IPAddress };
                CustomerPortal.Logs.Add(new Models.Log
                {
                    LogDateTime = DateTime.UtcNow,
                    LogType = logType,
                    UserID = credentials.UserID,
                    LogText = JsonConvert.SerializeObject(attachment)
                });
                CustomerPortal.SaveChanges();
                Logger.LogIt(logType, attachment);

                byte[] result = System.IO.File.ReadAllBytes(info.FullName);
                Response.Body.Write(result, 0, result.Length);
                Response.Body.Flush();
                Response.Body.Close();

            }
        }


        [HttpPost]
        public IActionResult GenerateGalleryThumbnails()
        {
            string message = string.Empty;
            string error = string.Empty;
            var credentials = VerifyCredentials();

            Task.Run(async () =>
            {
                string url = $"{Constants.FrobotUrl}portal/GenGalleryThumbnails";
                HttpClient client = new HttpClient();

                var parameters = new Dictionary<string, string> { };

                //if (credentials.CurrentUser.Admin)
                //    parameters.Add("adminID", credentials.UserID.ToString());

                var encodedContent = new FormUrlEncodedContent(parameters);

                var response = await client.PostAsync(url, encodedContent); // .ConfigureAwait(false);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    // Do something with response. Example get content:
                    message = await response.Content.ReadAsStringAsync(); // .ConfigureAwait (false);
                }
                else
                {
                    error = response.ReasonPhrase;

                    if (string.IsNullOrEmpty(error))
                        error = "An unknown error has occurred. Please try again.";
                }

            }).Wait();

            //task.Start();
            //task.Wait();

            //Thread.Sleep(2000);
            if (!string.IsNullOrEmpty(error))
                return BadRequest(error);

            return Ok(message);
        }


        //        [ValidateInput(false), HttpPost]
        //        public void UploadPhotoBlock(int RecordID, string RecordType, Guid UploadIdentifier, bool LastBlock, int PhotoID)
        //        {
        //            string q = BaseFileStore + @"\{0}\{1}";
        //            DirectoryInfo directory = new DirectoryInfo(string.Format(q, RecordType + "s", RecordID));
        //            string filename = string.Format(directory.FullName + @"\{0}.jpg", UploadIdentifier.ToString());
        //            string x = string.Format(directory.FullName + @"\Photo {0}.jpg", PhotoID);
        //            string y = string.Format(directory.FullName + @"\Photo {0}-email.jpg", PhotoID);

        //            try
        //            {
        //                byte[] chunk = new byte[Request.InputStream.Length];

        //                Request.InputStream.Read(chunk, 0, Convert.ToInt32(chunk.Length));

        //                if (!directory.Exists)
        //                {
        //                    directory.Create();
        //                }

        //                using (var chunkStream = new FileStream(filename, FileMode.Append, FileAccess.Write))
        //                {
        //                    chunkStream.Write(chunk, 0, chunk.Length);
        //                }

        //                if (LastBlock)
        //                {
        //                    if (RecordType == "General")
        //                    {
        //                        (new FileInfo(filename)).MoveTo(string.Format(directory.FullName + @"\{0}.jpg", DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss")));
        //                    }
        //                    else
        //                    {
        //                        FileInfo x1 = new FileInfo(x);
        //                        if (x1.Exists)
        //                        {
        //                            Utilities.RetryOnException(() =>
        //                            {
        //                                x1.Delete();
        //                            });
        //                        }

        //                        FileInfo y1 = new FileInfo(y);
        //                        if (y1.Exists)
        //                        {
        //                            Utilities.RetryOnException(() =>
        //                            {
        //                                y1.Delete();
        //                            });
        //                        }

        //                        (new FileInfo(filename)).MoveTo(x);

        //                        FileInfo info = new FileInfo(x);

        //                        //using (Image src = Image.FromFile(info.FullName))
        //                        //{
        //                        using (FileStream fs = new FileStream(info.FullName, FileMode.Open, FileAccess.Read))
        //                        {
        //                            using (Image src = Image.FromStream(fs))
        //                            {
        //                                int w = 0, h = 0;
        //                                if (src.Width > src.Height)
        //                                {
        //                                    w = 1500;
        //                                    h = (int)((1500m / Convert.ToDecimal(src.Width)) * Convert.ToDecimal(src.Height));
        //                                }
        //                                else
        //                                {
        //                                    h = 1500;
        //                                    w = (int)((1500m / Convert.ToDecimal(src.Height)) * Convert.ToDecimal(src.Width));
        //                                }
        //                                Bitmap bitmap = Utilities.ResizeImage(src, w, h);


        //                                var encoder = ImageCodecInfo.GetImageEncoders().First(c => c.FormatID == ImageFormat.Jpeg.Guid);
        //                                var encParams = new EncoderParameters() { Param = new[] { new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 92L) } };
        //                                bitmap.Save(info.FullName.Replace(info.Extension, "-email" + info.Extension), encoder, encParams);

        //                                bitmap.Dispose();
        //                                //src.Dispose();
        //                            }
        //                        }
        //                    }
        //                }

        //#if DEBUG
        //                //System.Threading.Thread.Sleep(500); // Testing cancellation & timeout on client
        //#endif
        //            }
        //            catch (Exception ex)
        //            {
        //                LogException("UploadPhotoBlock", string.Empty, ex, new { RecordID, RecordType, UploadIdentifier, LastBlock, PhotoID });
        //                Logging.Instance.LogException(ex);

        //                #region Clean Up Files
        //                try
        //                {
        //                    if (System.IO.File.Exists(x))
        //                    {
        //                        Utilities.RetryOnException(() =>
        //                        {
        //                            System.IO.File.Delete(x);
        //                        }, times: 3, delay: TimeSpan.FromSeconds(1));
        //                    }
        //                }
        //                catch (Exception ex2)
        //                {
        //                    Logging.Instance.LogException(ex2, new List<string>() { $"Deleting {x}" });
        //                }
        //                try
        //                {
        //                    if (System.IO.File.Exists(y))
        //                    {
        //                        Utilities.RetryOnException(() =>
        //                        {
        //                            System.IO.File.Delete(y);
        //                        }, times: 3, delay: TimeSpan.FromSeconds(1));
        //                    }
        //                }
        //                catch (Exception ex2)
        //                {
        //                    Logging.Instance.LogException(ex2, new List<string>() { $"Deleting {y}" });
        //                }
        //                try
        //                {
        //                    if (System.IO.File.Exists(filename))
        //                    {
        //                        Utilities.RetryOnException(() =>
        //                        {
        //                            System.IO.File.Delete(filename);
        //                        }, times: 3, delay: TimeSpan.FromSeconds(1));
        //                    }
        //                }
        //                catch (Exception ex2)
        //                {
        //                    Logging.Instance.LogException(ex2, new List<string>() { $"Deleting {filename}" });
        //                }

        //                #endregion

        //                throw;
        //            }

        //        }
    }
}
