USE [CustomerPortal]

ALTER TABLE dbo.SurveyQuestions ADD ExcelHeader NVARCHAR(500)

UPDATE dbo.SurveyQuestions 
	SET ExcelHeader = 'Contact Number'
WHERE SurveyID = 2 AND QuestionID = 2

UPDATE dbo.SurveyQuestions 
	SET ExcelHeader = '1.Overall Communcation'
WHERE SurveyID = 2 AND QuestionID = 3

UPDATE dbo.SurveyQuestions 
	SET ExcelHeader = '2.Followed Timeline'
WHERE SurveyID = 2 AND QuestionID = 4

UPDATE dbo.SurveyQuestions 
	SET ExcelHeader = '3.Responsiveness'
WHERE SurveyID = 2 AND QuestionID = 5

UPDATE dbo.SurveyQuestions 
	SET ExcelHeader = '4.Overall Experience'
WHERE SurveyID = 2 AND QuestionID = 6

UPDATE dbo.SurveyQuestions 
	SET ExcelHeader = '5.Purchase Again'
WHERE SurveyID = 2 AND QuestionID = 7

UPDATE dbo.SurveyQuestions 
	SET ExcelHeader = 'Additional Comments'
WHERE SurveyID = 2 AND QuestionID = 8

UPDATE dbo.SurveyQuestions 
	SET ExcelHeader = 'Testimonial'
WHERE SurveyID = 2 AND QuestionID = 9

UPDATE dbo.SurveyQuestions 
	SET ExcelHeader = 'Reference'
WHERE SurveyID = 2 AND QuestionID = 10

UPDATE dbo.SurveyQuestions 
	SET ExcelHeader = 'Contact'
WHERE SurveyID = 2 AND QuestionID = 11